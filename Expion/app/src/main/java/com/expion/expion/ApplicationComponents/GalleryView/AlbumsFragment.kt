package com.expion.expion.ApplicationComponents.GalleryView

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.*
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.EImageAlbum
import com.expion.expion.Models.Location
import com.expion.expion.R
import com.expion.expion.Services.AlbumsArrayAdapter
import com.expion.expion.Services.LocationsArrayAdapter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.albums_view_layout.*
import okhttp3.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.support.v4.selector
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*


/**
 * Created by arowe on 5/16/2016.
 */
class AlbumsFragment : Fragment(), View.OnClickListener, AdapterView.OnItemClickListener {

    /* Bundled args */
    private lateinit var urlPath: String
    private var companyId: Int = 0

    /* Collections */
    private var locations: ArrayList<Location> = ArrayList()
    private var selectedLocations: ArrayList<Location> = ArrayList()
    private var albums: ArrayList<EImageAlbum> = ArrayList()

    /* Views and View Containers */
    private lateinit var locationsAdapter: LocationsArrayAdapter
    private lateinit var albumsArrayAdapter: AlbumsArrayAdapter

    /* Instance Variables */
    private lateinit var albumTypeValue: String
    private var busyLoading = false
    private var albumCount: Int = 0
    lateinit var albumTypeOptions: List<String>

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
         albumTypeOptions = listOf(getString(R.string.sysomos_albums), getString(R.string.facebook_albums))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.albums_view_layout, container, false) as LinearLayout

        progressBar = ProgressDialog(context)

        companyId = Integer.valueOf(arguments?.getString("company_id"))!!
        urlPath = arguments?.getString("baseUrl")!!

        albums = ArrayList()

        getLocationsFromServer()

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gallerylocationsspinner!!.setOnClickListener(this)
        btn_newalbum!!.setOnClickListener(this)
        album_type_btn!!.setOnClickListener(this)
        albums_list!!.onItemClickListener = this
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        val item = menu!!.findItem(R.id.refresh_button)
        if (item != null)
            item.isVisible = false
    }

    private fun getLocationsFromServer() {
        val connectUrlString = "$urlPath/api/location/FBPOSTV1/$companyId?selectAll=true"

        val request = Request.Builder()
                .url(connectUrlString)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                try {
                    response.body()!!.use { responseBody ->
                        if (!response.isSuccessful) {
                            longSnackbar(albums_list!!, "Failed GetLocations" + response.message())
                        } else {
                            val responseString = responseBody.string()
                            val jsonArray = JSONArray(responseString)
                            parseLocationsJsonArray(jsonArray)
                        }
                    }
                } catch (jsonExc: JSONException) {
                    jsonExc.printStackTrace()
                }

            }
        })
    }

    private fun parseLocationsJsonArray(jsonArray: JSONArray) {
        activity?.runOnUiThread {
            try {
                locations = Gson().fromJson<ArrayList<Location>>(jsonArray.toString(), object : TypeToken<ArrayList<Location>>() {

                }.type)
                locationsAdapter = LocationsArrayAdapter(context, locations)
                setUpSelectedLocations()
            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }
    }

    private fun setUpSelectedLocations() {
        selectedLocations.clear()
        for (location in locations) {
            if (location.isSelected) {
                selectedLocations.add(location)
            } else {
                if (selectedLocations.contains(location))
                    selectedLocations.remove(location)
            }
        }

        setLocationsButtonText(false)

        setAlbumTypeButtonText(0)
    }

    private fun setAlbumTypeButtonText(selectedIndex: Int) {

        val locationsBtnText = albumTypeOptions.get(selectedIndex)
        album_type_btn.text = locationsBtnText

        albumTypeValue = if (selectedIndex == 0) "e" else "fb"
        currentPage = 1
        getAlbumsByLoc(currentPage)
    }

    private fun setLocationsButtonText(resetDefaultText: Boolean) {
        var locationsBtnText = getString(R.string.select_location)

        if (!resetDefaultText) {
            locationsBtnText = if (selectedLocations.size == 1) selectedLocations[0].name else Integer.toString(selectedLocations.size) + " Locations Selected"
        }
        gallerylocationsspinner!!.text = locationsBtnText
    }

    private fun getAlbumsByLoc(page: Int) {
        if (!progressBar!!.isShowing) {
            progressBar = ProgressDialog.show(activity, getString(R.string.app_name), "Loading Albums Please Wait...")
        }

        val connectUrl = "$urlPath/api/eimages/albums/$companyId"

        val jsonObj = JSONObject()
        val locJsonArray = JSONArray()

        val selectedAlbumsSize = selectedLocations.size
        if (selectedAlbumsSize > 0) {
            for (i in 0 until selectedAlbumsSize) {
                locJsonArray.put(selectedLocations[i].id)
            }

            try {
                jsonObj.put("locs", locJsonArray)
                jsonObj.put("page", page)
                jsonObj.put("limit", albumPerPage)
                jsonObj.put("type", albumTypeValue)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val JSON = MediaType.parse("application/json; charset=utf-8")
            val body = RequestBody.create(JSON, jsonObj.toString())

            val request = Request.Builder()
                    .url(connectUrl)
                    .put(body)
                    .build()

            (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    if (progressBar!!.isShowing) {
                        progressBar!!.dismiss()
                    }
                    e.printStackTrace()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    try {
                        response.body()!!.use { responseBody ->
                            if (!response.isSuccessful) {
                                if (progressBar!!.isShowing) {
                                    progressBar!!.dismiss()
                                }
                                longSnackbar(albums_list!!, "Failed to get albums: " + response.message())
                            } else {
                                val responseString = responseBody.string()
                                val jsonObject = JSONObject(responseString)
                                parseAlbumsJson(jsonObject)
                            }
                        }
                    } catch (jsonExc: JSONException) {
                        if (progressBar!!.isShowing) {
                            progressBar!!.dismiss()
                        }
                        Log.e("JsonExc", "Parsing Albums Info")
                    }

                }
            })
        } else {
            if (progressBar!!.isShowing) {
                progressBar!!.dismiss()
            }
            setLocationsButtonText(true)
        }
    }

    private fun parseAlbumsJson(jsonObject: JSONObject?) {
        if (jsonObject != null) {
            try {
                albumCount = jsonObject.optInt("total_results")
                val albumsData = jsonObject.getJSONArray("data")

                val albumsFromServer = Gson().fromJson<ArrayList<EImageAlbum>>(albumsData.toString(), object : TypeToken<ArrayList<EImageAlbum>>() {

                }.type)
                setUpAlbumsListView(albumsFromServer)
            } catch (exc: JSONException) {
                albumCount = 0
                if (progressBar!!.isShowing) {
                    progressBar!!.dismiss()
                }
                Toast.makeText(context, "JSONExc - " + exc.message, Toast.LENGTH_LONG).show()
            }

        } else {
            if (progressBar!!.isShowing) {
                progressBar!!.dismiss()
            }
            Toast.makeText(context, "Error getting albums from server", Toast.LENGTH_LONG).show()
        }
    }

    private fun setUpAlbumsListView(albumsFromServer: ArrayList<EImageAlbum>) {
        activity?.runOnUiThread {
            if (albums.size > 0) {
                albums.addAll(albumsFromServer)
                albumsArrayAdapter.notifyDataSetChanged()
            } else {
                albums.addAll(albumsFromServer)
                albumsArrayAdapter = AlbumsArrayAdapter(context, albums)
                albums_list!!.adapter = albumsArrayAdapter
            }

            if (progressBar!!.isShowing) {
                progressBar!!.dismiss()
            }

            albums_list!!.setOnScrollListener(object : AbsListView.OnScrollListener {

                override fun onScroll(view: AbsListView, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
                    if (firstVisibleItem + visibleItemCount >= totalItemCount && !busyLoading && albumCount > currentPage * albumPerPage) {
                        busyLoading = true
                        getAlbumsByLoc(++currentPage)
                    }
                }

                override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {
                    busyLoading = scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                }
            })
        }
    }

    override fun onClick(view: View) {
        if (view === gallerylocationsspinner) {
            showLocationsSelectDialog()
        } else if (view === btn_newalbum) {
            createNewAlbum()
        } else if (view === album_type_btn) {

            selector("", albumTypeOptions, { _, i ->
                albums!!.clear()
                setAlbumTypeButtonText(i)
            })
        }

    }

    private fun createNewAlbum() {
        val createNewAlbumPopupFragment = CreateNewAlbumPopupFragment()
        val bundle = Bundle()
        bundle.putInt("companyId", companyId)
        bundle.putString("baseUrl", urlPath)
        bundle.putString("albumTypeValue", albumTypeValue)
        bundle.putParcelableArrayList("locations", locations)

        var selectedLocation: Location? = null
        if (selectedLocations.size == 1)
            selectedLocation = selectedLocations[0]

        bundle.putParcelable("selectedLocation", selectedLocation)

        createNewAlbumPopupFragment.arguments = bundle
        createNewAlbumPopupFragment.setTargetFragment(this, DIALOG_FRAGMENT)

        val tabletMode = resources.getBoolean(R.bool.isTablet)

        if (!tabletMode) {
            createNewAlbumPopupFragment.showsDialog = false
            createNewAlbumPopupFragment.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.PopupTheme)
        } else
            createNewAlbumPopupFragment.showsDialog = true

        createNewAlbumPopupFragment.show(fragmentManager, "CreateNewAlbumFragment")

    }

    private fun showLocationsSelectDialog() {
        val builder = AlertDialog.Builder(activity)
        val factory = LayoutInflater.from(activity)
        val content = factory.inflate(R.layout.locations_dialog, null)

        builder.setView(content)
        val locAlert = builder.create()
        locAlert.requestWindowFeature(Window.FEATURE_NO_TITLE)

        if (!resources.getBoolean(R.bool.isTablet))
            locAlert.window!!.attributes.windowAnimations = R.style.LocationDialogAnimation

        val locationsListView = content.findViewById<View>(R.id.locations_list) as ListView
        locationsListView.choiceMode = ListView.CHOICE_MODE_MULTIPLE
        locationsListView.adapter = locationsAdapter
        val locationsCount = locationsAdapter.count

        val btnLocDone = content.findViewById<View>(R.id.btn_listdone) as Button
        btnLocDone.setOnClickListener { _: View ->
            selectedLocations.clear()
            for (i in 0 until locationsCount) {
                if (locationsListView.isItemChecked(i)) {
                    val location = locationsAdapter.getItem(i)
                    selectedLocations.add(location!!)
                    location.setSelected("1")
                } else {
                    val location = locationsAdapter.getItem(i)
                    selectedLocations.remove(location)
                    location!!.setSelected("0")
                }
            }
            locAlert.dismiss()
            albums.clear()
            setLocationsButtonText(false)
            currentPage = 1
            getAlbumsByLoc(currentPage)
        }

        val btnSelectAll = content.findViewById<Button>(R.id.btn_selectall)
        btnSelectAll.setOnClickListener({ _: View ->
            (0 until locationsCount).forEach { i -> locationsListView.setItemChecked(i, true) }
        })
        val btnDeselectAll = content.findViewById<Button>(R.id.btn_deselect)
        btnDeselectAll.setOnClickListener({ _: View ->
            (0 until locationsCount).forEach { i -> locationsListView.setItemChecked(i, false) }
        })

        (0 until locationsCount).forEach({ i ->
            if (locations[i].isSelected) {
                locationsListView.setItemChecked(i, true)
            }
        })
        locAlert.show()

    }

    private fun launchPhotosFragment(position: Int) {
        val photosFragment = PhotosFragment()
        val bundle = Bundle()
        bundle.putParcelable("album", albums[position])
        bundle.putString("baseUrl", urlPath)
        bundle.putInt("companyId", companyId)
        bundle.putString("albumTypeValue", albumTypeValue)

        photosFragment.arguments = bundle

        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.container, photosFragment, "Photos")
        transaction?.addToBackStack("Photos")

        transaction?.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                DIALOG_FRAGMENT -> {
                    currentPage = 1
                    albums = ArrayList()
                    getAlbumsByLoc(currentPage)
                }
            }
        }

    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        if (parent === albums_list) {
            launchPhotosFragment(position)
        }
    }

    /*private fun toggleListViewSelection() {
        val containerActivity = activity as ContainerActivity

        val backEntry = fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount - 2)
        if (backEntry != null) {
            val backStackName = backEntry.name

            if (backStackName == "Workspaces") {
                containerActivity.backPressed = false
                if (containerActivity.lastGroupSelectedIndexStack.size > 0)
                    containerActivity.lastGroupSelectedIndexStack.removeLast()
                containerActivity.checkWorkspaceMLItem()
            } else {
                if (!containerActivity.backPressed)
                    containerActivity.lastGroupSelectedIndexStack.removeLast()
                containerActivity.mlListView.expandGroup(containerActivity.lastGroupSelectedIndexStack.pollLast())
                containerActivity.backPressed = true
            }
        }
    }*/

    companion object {
        private var progressBar: ProgressDialog? = null
        var albumPerPage = 12
        private var currentPage = 1
        val DIALOG_FRAGMENT = 1
    }
}
