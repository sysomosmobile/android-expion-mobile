package com.expion.expion.ApplicationComponents.GalleryView

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ListView
import android.widget.RelativeLayout
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.Location
import com.expion.expion.R
import com.expion.expion.Services.LocationsArrayAdapter
import com.expion.expion.Services.OnBackPressedListener
import com.expion.expion.Utilities.MathUtils
import kotlinx.android.synthetic.main.album_create_popup_layout.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import okhttp3.*
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

/**
 * Created by arowe on 5/18/2016.
 */
class CreateNewAlbumPopupFragment : DialogFragment(), View.OnClickListener, OnBackPressedListener {
    private lateinit var urlPath: String
    private lateinit var albumTypeValue: String
    private var companyId: Int = 0

    private lateinit var locations: ArrayList<Location>
    private var selectedLocation: Location? = null
    private var selectedLocationName: String? = ""
    private var selectedId: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.album_create_popup_layout, container, false) as RelativeLayout

        val tabletMode = resources.getBoolean(R.bool.isTablet)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)

        if (!tabletMode) {
            // creating the fullscreen dialog
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog.window!!.attributes.windowAnimations = R.style.CreateNewAlbumAnimation
        } else {
            val sixteenDp = MathUtils.convertDpToPixel(16f, context).toInt()
            val tenDp = MathUtils.convertDpToPixel(10f, context).toInt()

            newAlbumName!!.setPadding(sixteenDp, tenDp, sixteenDp, tenDp)
            newAlbumDescription!!.setPadding(sixteenDp, tenDp, sixteenDp, tenDp)
        }

        companyId = arguments?.getInt("companyId")!!
        urlPath = arguments?.getString("baseUrl", "")!!
        albumTypeValue = arguments?.getString("albumTypeValue", "")!!
        selectedLocation = arguments?.getParcelable("selectedLocation")
        if (selectedLocation != null) {
            selectedId = selectedLocation!!.id
            selectedLocationName = selectedLocation!!.name
        }

        locations = arguments?.getParcelableArrayList("locations")!!

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnCreateAlbum!!.setOnClickListener(this)
        createNewAlbumLocationsSpinner!!.setOnClickListener(this)
        btnCancel!!.setOnClickListener(this)

        setLocationsButtonText()
    }

    override fun onClick(view: View) {
        if (view === btnCreateAlbum) {
            if (selectedId == null) {
                alertbox("Please select a Location")
            } else if (newAlbumName!!.text != null && newAlbumName!!.text.isNotEmpty()) {
                val aTitle = newAlbumName!!.text.toString()
                val aDesc = if (newAlbumDescription!!.text != null) newAlbumDescription!!.text.toString() else ""
                createNewAlbum(selectedId!!, aTitle, aDesc)
                val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(newAlbumName!!.windowToken, 0)
            } else {
                alertbox("Name cannot be blank")
            }
        } else if (view === createNewAlbumLocationsSpinner) {
            showLocationsSelectDialog()
        } else if (view === btnCancel) {
            onBackPressed()
        }

    }

    private fun createNewAlbum(locationId: String, albumName: String, albumDescription: String) {
        val connectUrl = "${urlPath}/api/eimages/albums/$companyId"

        val jsonObj = JSONObject()
        val locJson = JSONObject()

        try {
            locJson.put("id", locationId)
            jsonObj.put("Location", locJson)
            jsonObj.put("title", albumName)
            jsonObj.put("description", albumDescription)
            jsonObj.put("type", albumTypeValue)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val JSON = MediaType.parse("application/json; charset=utf-8")
        val body = RequestBody.create(JSON, jsonObj.toString())

        val request = Request.Builder()
                .url(connectUrl)
                .post(body)
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                longSnackbar(createCancelToolBar!!, "Error creating album: " + e.message)
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                try {
                    response.body()!!.use { _ ->
                        if (!response.isSuccessful) {
                            longSnackbar(createCancelToolBar!!, "Error creating album: " + response.message())
                        } else {
                            confirmAlbumCreation()
                        }
                    }
                } catch (exc: Exception) {
                    longSnackbar(createCancelToolBar!!, "Error creating album: " + response.message())
                }

            }
        })
    }

    private fun confirmAlbumCreation() {
        launch(UI) {
            targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, activity?.intent)
            dismiss()
        }
    }


    private fun setLocationsButtonText() {
        val locationsBtnText = if (!selectedLocationName!!.isEmpty()) selectedLocationName else "Select a Location"

        createNewAlbumLocationsSpinner!!.text = locationsBtnText
    }

    private fun showLocationsSelectDialog() {
        val builder = AlertDialog.Builder(activity)
        val factory = LayoutInflater.from(activity)
        val content = factory.inflate(R.layout.locations_dialog, null)

        builder.setView(content)
        val locAlert = builder.create()
        locAlert.requestWindowFeature(Window.FEATURE_NO_TITLE)

        val locationsAdapter = LocationsArrayAdapter(context, locations)

        val locationsListView = content.findViewById<View>(R.id.locations_list) as ListView
        locationsListView.choiceMode = ListView.CHOICE_MODE_SINGLE
        locationsListView.adapter = locationsAdapter

        val btnSelectAll = content.findViewById<View>(R.id.btn_selectall) as Button
        btnSelectAll.visibility = View.GONE
        val btnDeselectAll = content.findViewById<View>(R.id.btn_deselect) as Button
        btnDeselectAll.visibility = View.GONE

        val btnLocDone = content.findViewById<View>(R.id.btn_listdone) as Button
        btnLocDone.setOnClickListener { v: View ->
            if (locationsListView.checkedItemPosition != -1) {
                selectedLocation = locationsListView.getItemAtPosition(locationsListView.checkedItemPosition) as Location
                selectedId = selectedLocation!!.id
                selectedLocationName = selectedLocation!!.name
            }
            locAlert.dismiss()
            setLocationsButtonText()
        }

        btnSelectAll.setOnClickListener { _: View ->
            val locationsCount = locationsAdapter.count
            for (i in 0 until locationsCount) {
                locationsListView.setItemChecked(i, true)
            }
        }

        btnDeselectAll.setOnClickListener { _: View ->
            val locationsCount = locationsAdapter.count
            for (i in 0 until locationsCount) {
                locationsListView.setItemChecked(i, false)
            }
        }

        locations.indices
                .filter { locations[it] == selectedLocation }
                .forEach { locationsListView.setItemChecked(it, true) }
        locAlert.show()

    }


    private fun alertbox(mymessage: String) {
        AlertDialog.Builder(activity)
                .setMessage(mymessage)
                .setTitle(alertBoxTitle)
                .setCancelable(true)
                .setNeutralButton(android.R.string.cancel) { _: DialogInterface, _: Int -> }
                .show()
    }

    override fun onBackPressed() {
        dialog.dismiss()
    }

    companion object {

        private val alertBoxTitle = "Album Creation Error"
    }
}
