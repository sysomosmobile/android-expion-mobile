package com.expion.expion.ApplicationComponents.GalleryView

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.EImageAlbumImage
import com.expion.expion.R
import com.expion.expion.Services.OnBackPressedListener
import com.expion.expion.Utilities.MathUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.photo_approval_detail.*
import okhttp3.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.support.v4.onUiThread
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arowe on 5/12/2016.
 */
class PhotoApprovalDetailFragment : DialogFragment(), View.OnClickListener, OnBackPressedListener {
    private var urlPath: String? = ""
    private var companyId: Int? = 0

    private var tabletMode: Boolean = false

    private var albumImages: ArrayList<EImageAlbumImage>? = null
    private var position: Int = 0
    private var imageRotate = 0
    private var imageCount: Int = 0
    private val imageUrlLargeSize = "?width=300&height=300"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.photo_approval_detail, container, false) as RelativeLayout

        progressBar = ProgressDialog(context)

        tabletMode = resources.getBoolean(R.bool.isTablet)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)

        if (!tabletMode) {
            // creating the fullscreen dialog
            //dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        }

        format = SimpleDateFormat("MM/dd/yyyy", activity?.resources?.configuration?.locale)
        inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", activity?.resources?.configuration?.locale)
        inputFormat!!.timeZone = TimeZone.getTimeZone("GMT")

        urlPath = arguments?.getString("baseUrl")
        companyId = arguments?.getInt("companyId")
        albumImages = arguments?.getParcelableArrayList("albumImages")
        position = arguments?.getInt("position")!!
        imageCount = arguments?.getInt("imageCount")!!

        return rootView

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btn_rotatecw!!.setOnClickListener(this)
        btn_rotateccw!!.setOnClickListener(this)
        btnApprovalImageDetail!!.setOnClickListener(this)
        btnApprovalSkipPhoto!!.setOnClickListener(this)
        btnApprovePhoto!!.setOnClickListener(this)
        btnRejectPhoto!!.setOnClickListener(this)


        val eImage = albumImages!![position]
        setDetailInfo(eImage)
        downloadImageBitmap(eImage)
    }

    override fun onResume() {
        super.onResume()
        if (resources.getBoolean(R.bool.isTablet)) {
            val sixHundred = MathUtils.convertDpToPixel(600f, context).toInt()
            dialog.window!!.setLayout(sixHundred, sixHundred)
        }
    }

    private fun downloadImageBitmap(eImage: EImageAlbumImage) {
        onUiThread {
            if (!progressBar!!.isShowing) {
                progressBar = ProgressDialog.show(activity, resources.getString(R.string.app_name), "Loading Image Please Wait...")
            } }
        val imageUrl = "$urlPath${eImage.imageUrl}$imageUrlLargeSize"

        val request = Request.Builder()
                .url(imageUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                onUiThread { btnApprovalSkipPhoto!!.isEnabled = true
                    if (progressBar!!.isShowing)
                        progressBar!!.dismiss() }
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->
                    if (!response.isSuccessful) {
                        longSnackbar(approvalImageView!!, "FailedImage Download" + response.message())
                        onUiThread { btnApprovalSkipPhoto!!.isEnabled = true
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss() }
                    } else {
                        val inputStream = response.body()!!.byteStream()
                        val bitmap = BitmapFactory.decodeStream(inputStream)
                        onUiThread {
                            populateImageView(bitmap)
                            btnApprovalSkipPhoto!!.isEnabled = true
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                        }
                    }
                }
            }
        })
    }

    private fun populateImageView(bitmap: Bitmap) {
        approvalImageView!!.setImageBitmap(bitmap)
        (activity as ContainerActivity).client.dispatcher().cancelAll()
    }

    private fun setDetailInfo(eImage: EImageAlbumImage) {
        onUiThread {
            btnApprovalImageDetail!!.text = eImage.name

            val locName = eImage.locName ?: ""

            val detailLocationInfoString = "For: $locName"
            var detailUserInfoString = "By: ${eImage.uploadedBy}"

            detailLocationInfo!!.text = detailLocationInfoString

            var imageDateString = eImage.uploadDate

            val convertedDate: Date
            try {
                convertedDate = inputFormat!!.parse(imageDateString)
                imageDateString = format!!.format(convertedDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            detailUserInfoString += ", " + imageDateString

            detailUserInfo!!.text = detailUserInfoString

            var albumName = eImage.albumName
            if (albumName != null && albumName.isNotEmpty()) {
                detailAlbumInfo!!.visibility = View.VISIBLE
                albumName = "Album: $albumName"
                detailAlbumInfo!!.text = albumName
            } else {
                detailAlbumInfo!!.visibility = View.INVISIBLE
            } }

    }

    private fun removeCurrentImage() {
        albumImages!!.removeAt(position)
        position = if (position < albumImages!!.size - 1) position else 0
        skipPhoto()
    }

    private fun skipPhoto() {
        onUiThread { btnApprovalSkipPhoto!!.isEnabled = false }
        position++

        if (position < albumImages!!.size) {
            val eImage = albumImages!![position]
            downloadImageBitmap(eImage)
            setDetailInfo(eImage)
        } else if (position >= albumImages!!.size && imageCount > currentPage * imagesPerPage) {
            downloadImagesJson(++currentPage, imagesPerPage)
        } else {
            position = 0

            val eImage = albumImages!![position]
            downloadImageBitmap(eImage)
            setDetailInfo(eImage)
        }
    }


    private fun downloadImagesJson(page: Int, limit: Int) {
        if (!progressBar!!.isShowing) {
            progressBar = ProgressDialog.show(activity, resources.getString(R.string.app_name), "Loading Images Please Wait...")
        }

        val connectUrl = "$urlPath/api/eimages/images/$companyId?page=$page&limit=$limit$excludeSubmitted"

        val request = Request.Builder()
                .url(connectUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(approvalImageView!!, "Failed getImages4Album" + response.message())
                        if (progressBar!!.isShowing)
                            progressBar!!.dismiss()
                        btnApprovalSkipPhoto!!.isEnabled = true
                    } else {
                        try {
                            val responseString = responseBody.string()
                            val jsonObject = JSONObject(responseString)
                            parseAlbumImages(jsonObject)
                        } catch (jsonExc: JSONException) {
                            jsonExc.printStackTrace()
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                            btnApprovalSkipPhoto!!.isEnabled = true
                        }

                    }
                }
            }
        })

    }

    private fun parseAlbumImages(imageJsonObject: JSONObject?) {
        if (imageJsonObject != null) {
            val jsonImagesArray = imageJsonObject.optJSONArray("data")
            if (jsonImagesArray != null) {
                val gson = Gson()

                val images = gson.fromJson<ArrayList<EImageAlbumImage>>(jsonImagesArray.toString(), object : TypeToken<ArrayList<EImageAlbumImage>>() {}.type)

                if (images.size > 0) {
                    albumImages!!.addAll(images)
                    val eImage = albumImages!![position]
                    setDetailInfo(eImage)

                    downloadImageBitmap(eImage)
                }
            }
        }

        if (progressBar!!.isShowing)
            progressBar!!.dismiss()
        onUiThread { btnApprovalSkipPhoto!!.isEnabled = true }
    }

    private fun rotateImage(clockwise: Boolean) {
        imageRotate += if (clockwise) -90 else 90
        imageRotate += if (imageRotate >= 360) -360 else if (imageRotate < 0) 360 else 0

        if (!tabletMode) {
            approvalImageView!!.rotation = imageRotate.toFloat()
        } else {
            val rotateDegrees = if (clockwise) -90 else 90
            val currentBitmap = (approvalImageView!!.drawable as BitmapDrawable).bitmap
            val matrix = Matrix()
            matrix.postRotate(rotateDegrees.toFloat())

            val bitmap = Bitmap.createBitmap(currentBitmap, 0, 0, currentBitmap.width, currentBitmap.height, matrix, true)
            approvalImageView!!.setImageBitmap(bitmap)
        }


    }

    private fun doRotateAction(eImage: EImageAlbumImage) {
        val rotateUrl = "$urlPath/eimages/RotateImageBy90DInterval?companyId=$companyId&imageId=${eImage.imageId}&degrees=$imageRotate"
        rotateActionConnection(rotateUrl)
    }

    private fun doApprovalAction() {
        val eImage = albumImages!![position]

        /*if (imageRotate != 0)
            doRotateAction(eImage)*/

        var approveUrl: String? = ""
        val actions = eImage.actions
        actions
                .filter { it.name == "approve" }
                .forEach { approveUrl = it.link }
        galleryActionConnection(urlPath + approveUrl!!)
    }

    private fun doRejectAction() {
        val eImage = albumImages!![position]

        /*if (imageRotate != 0)
            doRotateAction(eImage)*/

        val actions = eImage.actions
        val rejectUrl: String? = actions
                .firstOrNull { it.name == "reject" }
                ?.link
                ?: ""
        galleryActionConnection(urlPath + rejectUrl!!)
    }

    private fun galleryActionConnection(connectUrl: String) {

        val request = Request.Builder()
                .url(connectUrl)
                .patch(RequestBody.create(null, ""))
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                longSnackbar(approvalImageView!!, "Error getting gallery: " + e.message)
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->
                    if (!response.isSuccessful) {
                        longSnackbar(approvalImageView!!, response.message())
                    } else {
                        //onUiThread {
                            removeCurrentImage()
                            /*val actionType = if (connectUrl.contains("Approve")) "approved" else "rejected"
                            longSnackbar(approvalImageView!!, "Photo " + actionType)*/
                        //}
                    }
                }
            }
        })
    }

    private fun rotateActionConnection(connectUrl: String) {

        val request = Request.Builder()
                .url(connectUrl)
                .post(RequestBody.create(null, ""))
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                longSnackbar(approvalImageView!!, "Error rotating image: " + e.message)
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->
                    if (!response.isSuccessful) {
                        longSnackbar(approvalImageView!!, response.message())
                    }
                }
            }
        })
    }

    override fun onClick(view: View) {
        when {
            view === btn_rotatecw -> rotateImage(true)
            view === btn_rotateccw -> rotateImage(false)
            view === btnApprovalImageDetail -> onBackPressed()
            view === btnApprovalSkipPhoto -> skipPhoto()
            view === btnApprovePhoto -> doApprovalAction()
            view === btnRejectPhoto -> doRejectAction()
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("currentPage", currentPage)
        targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)

        dialog.dismiss()
    }

    companion object {
        private var progressBar: ProgressDialog? = null

        private var currentPage = 1
        var imagesPerPage = 12
        private val excludeSubmitted = "&exclude_submitted=False"

        private var format: SimpleDateFormat? = null
        private var inputFormat: SimpleDateFormat? = null
    }
}