package com.expion.expion.ApplicationComponents.GalleryView

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.EImageAlbumImage
import com.expion.expion.R
import com.expion.expion.Services.ImagesArrayAdapter
import com.expion.expion.Services.RecyclerViewClickListener
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.photo_approval_layout.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.run
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

/**
 * Created by arowe on 5/10/2016.
 */
class PhotoApprovalFragment : Fragment(), RecyclerViewClickListener //, OnBackPressedListener
{
    override fun onClick(view: View?, position: Int) {
        showApprovalDetailView(position)
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private val imageUrlSmallSize = "?width=75&height=75"
    private lateinit var urlPath: String

    private var companyId: Int = 0
    private var imageCount: Int = 0

    private var busyLoading = false

    private var albumImages: ArrayList<EImageAlbumImage> = ArrayList()
    private lateinit var imagesArrayAdapter: ImagesArrayAdapter

    private val lastVisibleItemPosition: Int
        get() = linearLayoutManager.findLastVisibleItemPosition()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.photo_approval_layout, container, false) as LinearLayout

        progressBar = ProgressDialog(context)

        companyId = Integer.valueOf(arguments?.getString("companyId"))!!
        urlPath = arguments?.getString("baseUrl")!!

        linearLayoutManager = LinearLayoutManager(context)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupPhotosListView()

        downloadImagesJson(currentPage, imagesPerPage)
        //approval_photos_list!!.onItemClickListener = this

        approval_photos_list!!.layoutManager = linearLayoutManager
        approval_photos_list!!.setHasFixedSize(true)
        imagesArrayAdapter = ImagesArrayAdapter(context, albumImages, this)
        approval_photos_list!!.adapter = imagesArrayAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                DIALOG_FRAGMENT -> {
                    currentPage = data!!.getIntExtra("currentPage", currentPage)
                    refreshImages()
                }
            }
        }

    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        val item = menu!!.findItem(R.id.refresh_button)
        if (item != null)
            item.isVisible = false
    }

    private fun setupPhotosListView() {
        approval_photos_list!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView!!.layoutManager.itemCount
                if (totalItemCount == lastVisibleItemPosition + 1 && !busyLoading
                        && imageCount > currentPage * imagesPerPage && currentPage > 0) {
                    busyLoading = true
                    Log.i("Images","Populating Images from OnScroll")
                    downloadImagesJson(++currentPage, imagesPerPage)
                }
            }
        })
    }

    fun downloadImagesJson(page: Int, limit: Int) {
        if (!progressBar!!.isShowing) {
            progressBar = ProgressDialog.show(activity, resources.getString(R.string.app_name), "Loading Images Please Wait...")
        }

        val connectUrl = "$urlPath/api/eimages/images/$companyId?page=$page&limit=$limit$excludeSubmitted"


        val request = Request.Builder()
                .url(connectUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(approval_photos_list!!, "FailedImages Download" + response.message())
                        if (progressBar!!.isShowing)
                            progressBar!!.dismiss()
                        else {}
                    } else {
                        try {
                            val responseString = responseBody.string()
                            val jsonObject = JSONObject(responseString)
                            parseAlbumImages(jsonObject, page)
                        } catch (jsonExc: JSONException) {
                            jsonExc.printStackTrace()
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                            else {}
                        }

                    }
                }
            }
        })

    }

    private fun parseAlbumImages(imageJsonObject: JSONObject?, page: Int) = launch(UI) {
        if (imageJsonObject != null) {
            imageCount = imageJsonObject.optInt("total_results")
            if (imageCount > 0) {
                approvalNoPhotosIndicator!!.visibility = View.GONE
                approval_photos_list!!.visibility = View.VISIBLE
            } else {
                approvalNoPhotosIndicator!!.visibility = View.VISIBLE
                approval_photos_list!!.visibility = View.GONE
                if (progressBar!!.isShowing)
                    progressBar!!.dismiss()
            }

            val jsonImagesArray = imageJsonObject.optJSONArray("data")
            if (jsonImagesArray != null) {
                val gson = Gson()
                try {
                    if (page == 1) {
                        albumImages = gson.fromJson<ArrayList<EImageAlbumImage>>(jsonImagesArray.toString(), object : TypeToken<ArrayList<EImageAlbumImage>>() {}.type)
                        imagesArrayAdapter.setAlbumImages(albumImages)
                        imagesArrayAdapter.notifyDataSetChanged()
                        run(CommonPool) {
                            downloadImages(albumImages)
                        }
                    } else {
                        val images = gson.fromJson<ArrayList<EImageAlbumImage>>(jsonImagesArray.toString(), object : TypeToken<ArrayList<EImageAlbumImage>>() {}.type)
                        albumImages.addAll(images)
                        imagesArrayAdapter.addAlbumImages(images)

                        run(CommonPool) {
                            downloadImages(images)
                        }
                    }
                } catch (exc: Exception) {
                    Log.e("GSON Exc", exc.message)
                    if (progressBar!!.isShowing)
                        progressBar!!.dismiss()
                }

            }
        }

        currentPage = page
    }

    private fun refreshImages() {
        downloadImages(albumImages)
    }

    private fun downloadImages(images: ArrayList<EImageAlbumImage>?) {
        if (progressBar!!.isShowing)
            progressBar!!.dismiss()

        for (image in images!!) {

            val downloadUrl = "$urlPath${image.imageUrl}$imageUrlSmallSize"

            val request = Request.Builder()
                    .url(downloadUrl)
                    .get()
                    .build()

            (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    longSnackbar(approval_photos_list!!, "Failed DownloadImgs " + e.message)
                    e.printStackTrace()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    response.body()!!.use { _ ->
                        if (!response.isSuccessful) {
                            longSnackbar(approval_photos_list!!, "Failed DownloadImgs " + response.message())
                        } else {
                            val inputStream = response.body()!!.byteStream()
                            val bitmap = BitmapFactory.decodeStream(inputStream)
                            image.image = bitmap

                            activity?.runOnUiThread { imagesArrayAdapter.notifyDataSetChanged() }
                        }
                    }
                }
            })
        }
    }

    private fun showApprovalDetailView(position: Int) {
        val photoApprovalDetailFragment = PhotoApprovalDetailFragment()

        val bundle = Bundle()
        bundle.putString("baseUrl", urlPath)
        bundle.putParcelableArrayList("albumImages", albumImages)
        bundle.putInt("position", position)
        bundle.putInt("companyId", companyId)
        bundle.putInt("imageCount", imageCount)

        photoApprovalDetailFragment.arguments = bundle
        photoApprovalDetailFragment.setTargetFragment(this, DIALOG_FRAGMENT)

        val tabletMode = resources.getBoolean(R.bool.isTablet)

        if (!tabletMode) {
            photoApprovalDetailFragment.showsDialog = false
            photoApprovalDetailFragment.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.PopupTheme)
        } else
            photoApprovalDetailFragment.showsDialog = true

        photoApprovalDetailFragment.show(fragmentManager, "PhotoApprovalDetail")
    }

    companion object {
        private var progressBar: ProgressDialog? = null
        private val excludeSubmitted = "&exclude_submitted=False"
        val DIALOG_FRAGMENT = 1
        private var currentPage = 1
        var imagesPerPage = 12
    }
}