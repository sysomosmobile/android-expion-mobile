package com.expion.expion.ApplicationComponents.GalleryView

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.EImageAlbumImage
import com.expion.expion.R
import com.expion.expion.Services.OnBackPressedListener
import com.expion.expion.Utilities.MathUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.photo_detail.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arowe on 5/12/2016.
 */
class PhotoDetailFragment : DialogFragment(), View.OnClickListener, OnBackPressedListener {
    private val imageUrlLargeSize = "?width=300&height=300"

    private var urlPath: String? = ""
    private var albumId: String? = ""
    private var albumTypeValue: String = ""
    private var albumCred: String? = ""

    private var companyId: Int = 0
    private var position: Int = 0
    private var imageCount: Int = 0

    private var tabletMode: Boolean = false

    private var albumImages: ArrayList<EImageAlbumImage>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.photo_detail, container, false) as RelativeLayout

        progressBar = ProgressDialog(context)

        tabletMode = resources.getBoolean(R.bool.isTablet)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)

        if (!tabletMode) {
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        }

        format = SimpleDateFormat("MM/dd/yyyy", activity?.resources?.configuration?.locale)
        inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", activity?.resources?.configuration?.locale)
        inputFormat!!.timeZone = TimeZone.getTimeZone("GMT")

        urlPath = arguments?.getString("baseUrl")
        companyId = arguments?.getInt("companyId")!!
        albumImages = arguments?.getParcelableArrayList("albumImages")
        position = arguments?.getInt("position")!!
        imageCount = arguments?.getInt("imageCount")!!
        albumId = arguments?.getString("albumId")
        albumTypeValue = arguments?.getString("albumTypeValue")!!
        albumCred = arguments?.getString("albumCred", "")

        return rootView

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnImageDetail!!.setOnClickListener(this)
        btnSkipPhoto!!.setOnClickListener(this)

        val eImage = albumImages!![position]
        setDetailInfo(eImage)
        downloadImageBitmap(eImage)
    }

    override fun onResume() {
        super.onResume()
        if (resources.getBoolean(R.bool.isTablet)) {
            val sixHundred = MathUtils.convertDpToPixel(600f, context).toInt()
            dialog.window!!.setLayout(sixHundred, sixHundred)
        }
    }

    private fun downloadImageBitmap(eImage: EImageAlbumImage) {
        val imageUrl = "$urlPath${eImage.imageUrl}$imageUrlLargeSize"

        val request = Request.Builder()
                .url(imageUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                btnSkipPhoto!!.isEnabled = true
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->
                    if (!response.isSuccessful) {
                        launch(UI) {
                            longSnackbar(detailImageView!!, "FailedImage Download" + response.message())
                            btnSkipPhoto!!.isEnabled = true
                        }
                    } else {
                        val inputStream = response.body()!!.byteStream()
                        val bitmap = BitmapFactory.decodeStream(inputStream)
                        launch(UI) {
                            populateImageView(bitmap)
                        }
                    }
                }
            }
        })
    }

    private fun populateImageView(bitmap: Bitmap) {
        detailImageView!!.setImageBitmap(bitmap)
        btnSkipPhoto!!.isEnabled = true
    }

    private fun setDetailInfo(eImage: EImageAlbumImage) {
        if (eImage.name != null && eImage.name!!.isNotEmpty())
            btnImageDetail!!.text = eImage.name
        else
            btnImageDetail!!.text = "Back to images"

        val locationName = eImage.locName
        if(locationName != null && locationName.isNotEmpty())
        {
            val detailLocationInfoString = "For: " + locationName

            detailLocationInfo!!.text = detailLocationInfoString
        }

        var imageDateString = eImage.uploadDate

        if (!imageDateString.isEmpty()) {
            val convertedDate: Date
            try {
                convertedDate = inputFormat!!.parse(imageDateString)
                imageDateString = format!!.format(convertedDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }

        val uploader = eImage.uploadedBy
        var detailUserInfoString = "By: " + uploader

        if (uploader.isNotEmpty()) {
            if (imageDateString.isNotEmpty()) {
                detailUserInfoString += ", " + imageDateString
            }

            detailUserInfo!!.text = detailUserInfoString
        }

        var albumName = eImage.albumName
        if (albumName != null && albumName.isNotEmpty()) {
            detailAlbumInfo!!.visibility = View.VISIBLE
            albumName = "Album: $albumName"
            detailAlbumInfo!!.text = albumName
        } else {
            detailAlbumInfo!!.visibility = View.INVISIBLE
        }

    }

    private fun skipPhoto() {
        btnSkipPhoto!!.isEnabled = false
        position++

        if (position < albumImages!!.size) {
            val eImage = albumImages!![position]
            setDetailInfo(eImage)
            downloadImageBitmap(eImage)
        } else if (position >= albumImages!!.size && imageCount > currentPage * imagesPerPage) {
            downloadImagesJson(++currentPage, imagesPerPage)
        } else {
            position = 0

            val eImage = albumImages!![position]
            setDetailInfo(eImage)
            downloadImageBitmap(eImage)
        }
    }


    private fun downloadImagesJson(page: Int, limit: Int) {
        if (!progressBar!!.isShowing) {
            progressBar = ProgressDialog.show(activity, resources.getString(R.string.app_name), "Loading Images Please Wait...")
        }

        var connectUrl = "$urlPath/api/eimages/images/$albumId/$companyId?page=$page&limit=$limit&type=$albumTypeValue$excludeSubmitted"
        if (albumTypeValue.contains("fb")) {
            connectUrl += "&cred=$albumCred"
        }

        val request = Request.Builder()
                .url(connectUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                btnSkipPhoto!!.isEnabled = true
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(detailImageView!!, "Failed getImages4Album" + response.message())
                        if (progressBar!!.isShowing)
                            progressBar!!.dismiss()
                        btnSkipPhoto!!.isEnabled = true
                    } else {
                        try {
                            val responseString = responseBody.string()
                            val jsonObject = JSONObject(responseString)
                            parseAlbumImages(jsonObject)
                            btnSkipPhoto!!.isEnabled = true
                        } catch (jsonExc: JSONException) {
                            jsonExc.printStackTrace()
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                            btnSkipPhoto!!.isEnabled = true
                        }

                    }
                }
            }
        })

    }

    private fun parseAlbumImages(imageJsonObject: JSONObject?) {
        if (imageJsonObject != null) {
            val jsonImagesArray = imageJsonObject.optJSONArray("data")
            if (jsonImagesArray != null) {
                val gson = Gson()

                val images = gson.fromJson<ArrayList<EImageAlbumImage>>(jsonImagesArray.toString(), object : TypeToken<ArrayList<EImageAlbumImage>>() {

                }.type)

                if (images.size > 0) {
                    albumImages!!.addAll(images)
                    val eImage = albumImages!![position]
                    setDetailInfo(eImage)
                    downloadImageBitmap(eImage)
                }
            }
        }

        if (progressBar!!.isShowing)
            progressBar!!.dismiss()
        btnSkipPhoto!!.isEnabled = true
    }

    override fun onClick(view: View) {
        if (view === btnImageDetail) {
            onBackPressed()
        } else if (view === btnSkipPhoto) {
            skipPhoto()
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("currentPage", currentPage)
        targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)

        dialog.dismiss()
    }

    companion object {
        private var progressBar: ProgressDialog? = null

        private var currentPage = 1
        var imagesPerPage = 12
        private val excludeSubmitted = "&exclude_submitted=False"

        private var format: SimpleDateFormat? = null
        private var inputFormat: SimpleDateFormat? = null
    }
}