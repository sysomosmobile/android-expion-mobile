package com.expion.expion.ApplicationComponents.GalleryView

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.LinearLayout
import android.widget.Toast
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.EImageAlbum
import com.expion.expion.Models.EImageAlbumImage
import com.expion.expion.R
import com.expion.expion.Services.ImagesArrayAdapter
import com.expion.expion.Services.OnBackPressedListener
import com.expion.expion.Services.RecyclerViewClickListener
import com.expion.expion.Utilities.CAMERA_PIC_REQUEST
import com.expion.expion.Utilities.CAMERA_ROLL_PIC_REQUEST
import com.expion.expion.Utilities.EIMAGES_PIC_REQUEST
import com.expion.expion.Utilities.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.photos_view_layout.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.run
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.support.v4.onUiThread
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.LinkedHashMap

/**
 * Created by arowe on 5/18/2016.
 */
class PhotosFragment : Fragment(), View.OnClickListener, OnBackPressedListener, RecyclerViewClickListener {


    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var imagesArrayAdapter: ImagesArrayAdapter
    private val imageUrlSmallSize = "?width=75&height=75"
    private var onActivityRequestCode: Int = 0

    private lateinit var album: EImageAlbum
    private lateinit var downloadUrl: String
    private var albumTypeValue: String = ""
    private var urlPath: String? = ""
    private lateinit var albumId: String
    private var albumCred: String = ""

    private var companyId = 0
    private var imageCount = 0
    private var albumImagePage = 1

    private var albumImages = ArrayList<EImageAlbumImage>()

    private var uploadImgData: Bitmap? = null
    private var uploadFileName: String? = null
    private lateinit var destination: File

    private var busyLoading = false

    private var clickableMap =  LinkedHashMap<Int,Boolean>()
    private val lastVisibleItemPosition: Int
        get() = linearLayoutManager.findLastVisibleItemPosition()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.photos_view_layout, container, false) as LinearLayout

        progressBar = ProgressDialog(context)

        album = arguments?.getParcelable("album")!!

        albumId = album.id
        albumCred = album.albumCred

        urlPath = arguments?.getString("baseUrl")
        companyId = arguments?.getInt("companyId")!!
        albumTypeValue = arguments?.getString("albumTypeValue", "")!!
        linearLayoutManager = LinearLayoutManager(context)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnAlbumTitle!!.setOnClickListener(this)
        btnTakePic!!.setOnClickListener(this)
        btnCameraRoll!!.setOnClickListener(this)

        albumimagesgridview!!.layoutManager = linearLayoutManager
        albumimagesgridview!!.setHasFixedSize(true)
        imagesArrayAdapter = ImagesArrayAdapter(activity, albumImages, this)
        albumimagesgridview!!.adapter = imagesArrayAdapter

        setUpImagesListView()

        val albumName = "${album.title} - ${album.location.name}"
        btnAlbumTitle!!.text = albumName

        populateImages(albumImagePage)
    }

    private fun populateImages(page: Int) {

        downloadUrl = "$urlPath/api/eimages/images/$albumId/$companyId?page=$page&limit=$albumImagePerPage&type=$albumTypeValue" //$excludeSubmitted"

        if (albumTypeValue.contains("fb")) {
            downloadUrl += "&cred=$albumCred"
        }

        if (!progressBar!!.isShowing) {
            progressBar = ProgressDialog.show(activity, resources.getString(R.string.app_name), "Loading Images Please Wait...")
        }

        val request = Request.Builder()
                .url(downloadUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                try {
                    response.body()!!.use { responseBody ->
                        if (!response.isSuccessful) {
                            longSnackbar(albumimagesgridview!!, "Failed GetLocations" + response.message())
                        } else {
                            val responseString = responseBody.string()
                            val jsonObject = JSONObject(responseString)
                            parseImagesJson(jsonObject)
                        }
                    }
                } catch (jsonExc: JSONException) {
                    jsonExc.printStackTrace()
                }

            }
        })
    }

    private fun parseImagesJson(jsonObject: JSONObject?) = launch(UI)  {
        if (jsonObject != null) {
            imageCount = jsonObject.optInt("total_results")
            if (imageCount > 0) {
                if(noPhotosIndicator != null) {
                    noPhotosIndicator!!.visibility = View.GONE
                }
                if(albumimagesgridview != null) {
                    albumimagesgridview!!.visibility = View.VISIBLE
                }
            } else {
                if(noPhotosIndicator != null) {
                    noPhotosIndicator!!.visibility = View.VISIBLE
                }
                if(albumimagesgridview != null) {
                    albumimagesgridview!!.visibility = View.GONE
                }
                if (progressBar!!.isShowing)
                    progressBar!!.dismiss()
            }

            val jsonImagesArray = jsonObject.optJSONArray("data")
            if (jsonImagesArray != null) {
                val gson = Gson()
                try {
                    if (albumImagePage == 1) {
                        albumImages.clear()
                        albumImages = gson.fromJson<ArrayList<EImageAlbumImage>>(jsonImagesArray.toString(), object : TypeToken<ArrayList<EImageAlbumImage>>() {}.type)
                        imagesArrayAdapter.setAlbumImages(albumImages)
                        imagesArrayAdapter.notifyDataSetChanged()
                        run(CommonPool) {
                            downloadImages(albumImages)
                        }
                    } else {
                        val images = gson.fromJson<ArrayList<EImageAlbumImage>>(jsonImagesArray.toString(), object : TypeToken<ArrayList<EImageAlbumImage>>() {}.type)
                        albumImages.addAll(images)
                        imagesArrayAdapter.addAlbumImages(images)

                        run(CommonPool) {
                            downloadImages(images)
                        }
                    }
                } catch (exc: Exception) {
                    if (progressBar!!.isShowing)
                        progressBar!!.dismiss()
                }

            } else {
                if (albumImagePage == 1) {
                    albumImages = ArrayList()
                    imagesArrayAdapter.notifyDataSetChanged()
                }
            }
        } else {
            if (progressBar!!.isShowing)
                progressBar!!.dismiss()
            longSnackbar(albumimagesgridview, "Error downloading images")
        }
    }

    private fun setUpImagesListView() {
        albumimagesgridview!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                busyLoading = newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                val totalItemCount = recyclerView!!.layoutManager.itemCount
                if (totalItemCount == lastVisibleItemPosition + 1 && !busyLoading
                        && imageCount > albumImagePage * albumImagePerPage && albumImagePage > 1) {
                    busyLoading = true
                    Log.i("Images","Populating Images from OnScroll")
                    populateImages(++albumImagePage)
                }
            }
        })
    }

    private fun downloadImages(images: ArrayList<EImageAlbumImage>?) {
        if (progressBar!!.isShowing)
            progressBar!!.dismiss()

        var counter = -1

        for (image in images!!) {
            val downloadUrl = urlPath + image.imageUrl + imageUrlSmallSize

            val request = Request.Builder()
                    .url(downloadUrl)
                    .get()
                    .build()

            (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    response.body()!!.use { _ ->
                            if (!response.isSuccessful) {
                                counter++
                                clickableMap.put(counter,false)
                            } else {
                                counter++
                                clickableMap.put(counter,true)
                                val inputStream = response.body()!!.byteStream()
                                val bitmap = BitmapFactory.decodeStream(inputStream)
                                image.image = bitmap
                                onUiThread {
                                    imagesArrayAdapter.notifyDataSetChanged()
                                }

                            }
                    }
                }
            })
        }
    }

    override fun onClick(view: View) {
        when {
            view === btnAlbumTitle -> fragmentManager?.popBackStack()
            view === btnTakePic -> {
                val df = SimpleDateFormat("yyyy-MM-dd-hh-mm-ss", Locale.US)
                uploadFileName = df.format(Date())
                destination = File(Environment.getExternalStorageDirectory(), uploadFileName!!)
                val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                cameraIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                val destinationUri = context?.let { FileProvider.getUriForFile(it, "com.expion.expion.fileProvider", destination) }
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, destinationUri)
                startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST)
            }
            view === btnCameraRoll -> {
                val intent = Intent()
                val availableMimeTypes = "image/*"
                intent.type = availableMimeTypes
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), CAMERA_ROLL_PIC_REQUEST)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            onActivityRequestCode = requestCode
            if (requestCode == CAMERA_PIC_REQUEST) {
                if (activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.READ_EXTERNAL_STORAGE) } != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
                } else {
                    onCaptureImageResult(destination)
                }
            } else if (requestCode == DIALOG_FRAGMENT) {
                populateImages(albumImagePage)
            } else if (requestCode == DETAIL_FRAGMENT) {
                albumImagePage = data!!.getIntExtra("currentPage", albumImagePage)
                downloadImages(albumImages)
            } else if (requestCode == CAMERA_ROLL_PIC_REQUEST) {
                try {
                    val uri = data!!.data
                    uploadImgData = MediaStore.Images.Media.getBitmap(activity?.contentResolver, uri)
                    if (activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.READ_EXTERNAL_STORAGE) } != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
                    } else {
                        uploadImageStuff(getTempFile(context))
                    }
                } catch (exc: IOException) {
                    Toast.makeText(context, exc.message, Toast.LENGTH_LONG).show()
                }

            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (requestCode == CAMERA_PIC_REQUEST) {
                        onCaptureImageResult(destination)
                    }
                    else if(requestCode == CAMERA_ROLL_PIC_REQUEST) {
                        uploadImageStuff(getTempFile(context))
                    }
                } else {
                    Toast.makeText(activity, "You can't upload a picture without this permission.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun onCaptureImageResult(file: File) {
        try {

            val uriFromFile = context?.let { FileProvider.getUriForFile(it, "com.expion.expion.fileProvider", file) }
            uploadImgData = MediaStore.Images.Media.getBitmap(activity?.contentResolver, uriFromFile)
            uploadImageStuff(file)
        } catch (exc: IOException) {
            exc.printStackTrace()
        }

    }

    private fun uploadImageStuff(file: File?) {
        if (uploadImgData != null) {

            val exif: ExifInterface
            var rotate = 0
            if (onActivityRequestCode != EIMAGES_PIC_REQUEST && onActivityRequestCode != 1 && file != null) {
                try {
                    exif = ExifInterface(file.absolutePath)

                    val exifOrientation = Integer.parseInt(exif.getAttribute(ExifInterface.TAG_ORIENTATION))

                    when (exifOrientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90

                        ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180

                        ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

            if (rotate != 0)
                uploadImgData = rotateImage(rotate, uploadImgData!!)

            try {
                val fo = context?.openFileOutput(file!!.name, Context.MODE_PRIVATE)
                uploadImgData!!.compress(Bitmap.CompressFormat.JPEG, 60, fo)
                fo?.close()
            } catch (exc: Exception) {
                exc.printStackTrace()
            }

            var showTextViewsInUploadFragment = false

            if (onActivityRequestCode == CAMERA_ROLL_PIC_REQUEST) {
                showTextViewsInUploadFragment = true
                uploadFileName = file!!.name
            }

            val uploadNewImagePopupFragment = UploadNewImagePopupFragment()
            val arguments = Bundle()
            arguments.putString("uploadFileName", uploadFileName)
            arguments.putInt("companyId", companyId)
            arguments.putString("baseUrl", urlPath)
            arguments.putString("albumTypeValue", albumTypeValue)
            arguments.putParcelable("selectedLocation", album.location)

            val locationId = album.location.id

            arguments.putString("locationId", locationId)

            arguments.putString("albumId", albumId)
            arguments.putBoolean("showTextViews", showTextViewsInUploadFragment)

            uploadNewImagePopupFragment.arguments = arguments
            uploadNewImagePopupFragment.setTargetFragment(this@PhotosFragment, DIALOG_FRAGMENT)

            val tabletMode = resources.getBoolean(R.bool.isTablet)

            if (!tabletMode) {
                uploadNewImagePopupFragment.showsDialog = false
                uploadNewImagePopupFragment.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.PopupTheme)
            } else
                uploadNewImagePopupFragment.showsDialog = true

            Handler().post { uploadNewImagePopupFragment.show(fragmentManager, "UploadNewImageFragment") }
        }
    }

    private fun getTempFile(context: Context?): File {
        val path = File(Environment.getExternalStorageDirectory(), context?.packageName)
        if (!path.exists()) {
            path.mkdir()
        }
        return File(path, "image.tmp")
    }

    private fun rotateImage(angle: Int, bitmapSrc: Bitmap): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        return Bitmap.createBitmap(bitmapSrc, 0, 0, bitmapSrc.width, bitmapSrc.height, matrix, true)
    }

    private fun showDetailView(position: Int) {
        val photoDetailFragment = PhotoDetailFragment()

        val bundle = Bundle()
        bundle.putString("baseUrl", urlPath)
        bundle.putParcelableArrayList("albumImages", albumImages)
        bundle.putInt("position", position)
        bundle.putInt("companyId", companyId)
        bundle.putInt("imageCount", imageCount)
        bundle.putString("albumId", albumId)
        bundle.putString("albumTypeValue", albumTypeValue)
        bundle.putString("albumCred", albumCred)

        photoDetailFragment.arguments = bundle
        photoDetailFragment.setTargetFragment(this, DETAIL_FRAGMENT)

        val tabletMode = resources.getBoolean(R.bool.isTablet)

        if (!tabletMode) {
            photoDetailFragment.showsDialog = false
            photoDetailFragment.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.PopupTheme)
        } else
            photoDetailFragment.showsDialog = true

        photoDetailFragment.show(fragmentManager, "PhotoDetail")
    }

    override fun onBackPressed() {
        activity?.supportFragmentManager?.popBackStack()
    }

    override fun onClick(view: View?, position: Int) {
        Log.d("Images","Clickable: ${clickableMap.getValue(position)}")
        if(clickableMap.getValue(position)) {
            showDetailView(position)
        }
    }

    companion object {
        private var progressBar: ProgressDialog? = null

        private val excludeSubmitted = "&exclude_submitted=false"

        val DIALOG_FRAGMENT = 1
        val DETAIL_FRAGMENT = 3
        private val albumImagePerPage = 12
    }
}
