package com.expion.expion.ApplicationComponents.GalleryView

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Build
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.Location
import com.expion.expion.R
import com.expion.expion.Services.MyCookieCache
import com.expion.expion.Utilities.MathUtils
import kotlinx.android.synthetic.main.image_upload_popup_layout.*
import okhttp3.*
import org.jetbrains.anko.design.longSnackbar
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.IOException

/**
 * Created by arowe on 5/18/2016.
 */
class UploadNewImagePopupFragment : DialogFragment(), View.OnClickListener {
    private var urlPath: String? = ""
    private lateinit var albumTypeValue: String
    private lateinit var androidUserAgent: String

    private var locationId: String? = ""
    private var uploadFileName: String? = ""
    private var albumId: String? = ""

    private var companyId: Int = 0

    private lateinit var uploadImgData: Bitmap
    private var showTextViews = false
    private var selectedLocation: Location? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.image_upload_popup_layout, container, false) as RelativeLayout

        val tabletMode = resources.getBoolean(R.bool.isTablet)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)

        if (!tabletMode) {
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
        } else {
            val sixteenDp = MathUtils.convertDpToPixel(16f, context).toInt()
            val tenDp = MathUtils.convertDpToPixel(10f, context).toInt()

            newImageName!!.setPadding(sixteenDp, tenDp, sixteenDp, tenDp)
            newImageDescription!!.setPadding(sixteenDp, tenDp, sixteenDp, tenDp)
        }

        progressBar = ProgressDialog(context)

        companyId = arguments?.getInt("companyId")!!
        urlPath = arguments?.getString("baseUrl")
        albumTypeValue = arguments?.getString("albumTypeValue")!!
        locationId = arguments?.getString("locationId")
        uploadFileName = arguments?.getString("uploadFileName")
        albumId = arguments?.getString("albumId")
        showTextViews = arguments?.getBoolean("showTextViews")!!
        selectedLocation = arguments?.getParcelable("selectedLocation")

        val expionPrefSettings = context?.getSharedPreferences(getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)
        androidUserAgent = expionPrefSettings?.getString(getString(R.string.android_user_agent), "")!!

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnUploadImage!!.setOnClickListener(this)
        btnCancel!!.setOnClickListener(this)
        setLocationsButtonText()

        if (!showTextViews) {
            newImageName!!.visibility = View.GONE
            newImageDescription!!.visibility = View.GONE
            val params = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.5f)
            btnUploadImage!!.layoutParams = params
            btnUploadImage!!.text = "Use This Image"
        }

        populateImageView()
    }


    private fun setLocationsButtonText() {
        val locationsBtnText = if (selectedLocation != null) selectedLocation!!.name else "Select a Location"

        imageUploadLocationsSpinner!!.text = locationsBtnText
    }


    private fun populateImageView() {
        try {
            uploadImgData = BitmapFactory.decodeStream(context?.openFileInput(uploadFileName))
            val width = uploadImgData.width
            val height = uploadImgData.height
            var newWidth = 300
            var newHeight = 300

            val widthDivNewHeight = width / newHeight
            val heightDivNewWidth = height / newWidth

            if (width > height && widthDivNewHeight > 0) {
                newHeight = height / widthDivNewHeight
            } else if (height > width && heightDivNewWidth > 0) {
                newWidth = width / heightDivNewWidth
            }

            val scaleWidth = newWidth.toFloat() / width
            val scaleHeight = newHeight.toFloat() / height
            val matrix = Matrix()
            matrix.postScale(scaleWidth, scaleHeight)
            val imgPrevData = Bitmap.createBitmap(uploadImgData, 0, 0, width, height, matrix, true)
            imageUpload!!.setImageBitmap(imgPrevData)
        } catch (exc: FileNotFoundException) {
            exc.printStackTrace()
        }

    }

    override fun onClick(view: View) {
        if (view === btnUploadImage) {
            if (showTextViews) {
                if (newImageName!!.text != null && newImageName!!.text.isNotEmpty()) {
                    uploadNewImage(locationId, newImageName!!.text.toString(), newImageDescription!!.text.toString())
                } else {
                    alertbox("Post Error", "Name cannot be blank", true)
                }
            } else {
                uploadNewImage(locationId, uploadFileName, "")
            }
        } else if (view === btnCancel) {
            dialog.dismiss()
        }

    }

    private fun uploadNewImage(locationId: String?, imageName: String?, imageDescription: String) {
        if (!progressBar!!.isShowing) {
            progressBar = ProgressDialog.show(activity, resources.getString(R.string.app_name), "Uploading Image Please Wait...")
        }

        var uploadImageUrl = urlPath
        var appType = "?app="

        if (albumTypeValue.contains("e")) {
            appType += "EIMAGES"
            uploadImageUrl += "/Upload/ExpionFileMobile"
        } else if (albumTypeValue.contains("fb")) {
            appType += "FBIMAGEV2"
            uploadImageUrl += "/Upload/FacebookFileMobile"
        }

        var cookie: Cookie? = null

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            cookie = MyCookieCache.getCookies()!!.stream().filter { c -> c.domain().contains("sysomos.com") && c.name().contains("ASPXAUTH") }.findFirst().orElse(null)
        else {
            val cookies = MyCookieCache.getCookies()
            for (cookie1 in cookies!!) {
                if (cookie1.domain().contains("sysomos.com") && cookie1.name().contains("ASPXAUTH")) {
                    cookie = cookie1
                    break
                }
            }
        }

        if (cookie != null) {

            val tokenInfo = "&token=" + cookie.value()
            //uploadImageUrl += tokenInfo

            val urlParams = "$appType&location_id=$locationId&album_id=$albumId&company_id=$companyId${tokenInfo}"

            uploadImageUrl += urlParams

            val bos = ByteArrayOutputStream()
            uploadImgData!!.compress(Bitmap.CompressFormat.JPEG, 60, bos)

            val IMAGE_TYPE = MediaType.parse("image/*")
            val imgName = imageName!! + ".jpg"

            val requestBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file", imgName,
                            RequestBody.create(IMAGE_TYPE, bos.toByteArray()))
                    .build()

            val request = Request.Builder()
                    .url(uploadImageUrl)
                    .post(requestBody)
                    .build()

            (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                    if (progressBar!!.isShowing)
                        progressBar!!.dismiss()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    response.body()!!.use { _ ->
                        if (!response.isSuccessful) {
                            longSnackbar(imageUpload!!, "Error uploading $imageName.jpg")
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                        } else {
                            confirmImageUpload()
                        }
                    }
                }
            })
        } else {
            (activity as ContainerActivity).returnToLogin()
            dismiss()
        }
    }

    private fun confirmImageUpload() {
        activity?.runOnUiThread {
            if (progressBar!!.isShowing)
                progressBar!!.dismiss()
            Toast.makeText(activity, newImageName!!.text.toString() + " uploaded successfully", Toast.LENGTH_LONG).show()
            targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, activity?.intent)
            dismiss()
        }
    }

    protected fun alertbox(title: String, myMessage: String, cancelable: Boolean) {
        AlertDialog.Builder(activity)
                .setMessage(myMessage)
                .setTitle(title)
                .setCancelable(cancelable)
                .setNeutralButton(android.R.string.cancel) { _: DialogInterface, _: Int -> }
                .show()
    }

    companion object {
        private var progressBar: ProgressDialog? = null
    }
}
