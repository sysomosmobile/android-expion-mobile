package com.expion.expion.ApplicationComponents.RootComponents

import android.app.Activity
import android.app.ProgressDialog
import android.content.*
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.MenuItemCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SwitchCompat
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import android.webkit.WebView
import android.widget.*
import com.expion.expion.ApplicationComponents.ViewPortComponents.LoginFragment
import com.expion.expion.ApplicationComponents.ViewPortComponents.MLExpandableListAdapter
import com.expion.expion.ApplicationComponents.ViewPortComponents.StacksFragment
import com.expion.expion.ApplicationComponents.ViewPortComponents.WebViewFragment
import com.expion.expion.BuildConfig
import com.expion.expion.DynamicClasses.ExpionMLBaseClassSettings
import com.expion.expion.Models.MLDirective
import com.expion.expion.Models.MLItem
import com.expion.expion.Models.StackDetail
import com.expion.expion.R
import com.expion.expion.Services.*
import com.expion.expion.Utilities.*
import com.franmontiel.persistentcookiejar.ClearableCookieJar
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.badge_layout.*
import kotlinx.android.synthetic.main.container_activity.*
import kotlinx.android.synthetic.main.log_view_action_bar_switch_layout.view.*
import okhttp3.*
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit

//import com.google.firebase.iid.FirebaseInstanceId;
//import com.joanzapata.iconify.widget.IconButton;

//import com.testfairy.TestFairy;

/**
 * Created by arowe on 12/4/2015.
 */
class ContainerActivity : AppCompatActivity(), OnTaskCompleted, StackLoadedListener, SwipeRefreshLayout.OnRefreshListener, LoggingUtils.LoggingCallback //, OnApprovalImagesDownloaded //,
{
    private lateinit var logsSwitchItem: MenuItem

    private val observer = createObserver()

    //NavigationDrawer
    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle

    //LeftPanel
    private var newMlAdapter: MLExpandableListAdapter? = null
    internal lateinit var progressBar: ProgressDialog

    //Bundle extras
    internal lateinit var myUrl: String
    internal lateinit var company_id: String
    private var userName: String = ""

    private var isLoggedIn = false
    internal var debugMode = false
    internal var logToTextView = false

    //OkHttp
    lateinit var client: OkHttpClient

    //ML Stuff
    internal lateinit var currentMLItem: MLItem
    private lateinit var userHomeItemsArray: ArrayList<MLItem>
    private lateinit var mlWorkspacesItem: MLItem
    private var homeWorkspacePresent = false
    internal var mlAdapterPopulated = false
    private var homeItemsCount = -1
    internal var lastChildSelectedIndex = -1 //Last Workspace selected
    private lateinit var lastGroupSelectedIndexStack: Deque<Int>
    private lateinit var getMLJsonConnectURL: String

    internal var tabletMode = false

    private var dialogStackLevel = 0

    private var resumingFromStacksFragment = false
    private lateinit var stackLayoutConnectionUrl: String
    private lateinit var cloneUrl: String
    private lateinit var loadUrl: String

    var gson = Gson()

    private lateinit var baseClassSettings: ExpionMLBaseClassSettings
    private var notificationsEnabled: Boolean = false

    //private static final String YOUR_APP_TOKEN = "817793e9098fba3c9b57167b0ce9cd5498fd4a7c";

    private var ssoEnabled = true
    private lateinit var cookieJar: ClearableCookieJar
    private lateinit var cookieCache: MyCookieCache
    private var broadcastRegistered: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.container_activity)
        adjustFontScale(this, resources.configuration)
        //TestFairy.begin(this, YOUR_APP_TOKEN);

        //if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            deleteCache()
        //}

        tabletMode = resources.getBoolean(R.bool.isTablet)

        progressBar = ProgressDialog(this)

        swipeRefreshLayout!!.setOnRefreshListener(this)

        mlListView!!.choiceMode = ExpandableListView.CHOICE_MODE_SINGLE

        lastGroupSelectedIndexStack = ArrayDeque()

        if (!tabletMode) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            //drawerLayout = findViewById(R.id.drawerLayout)
            actionBarDrawerToggle = object : ActionBarDrawerToggle(
                    this, /* host Activity */
                    drawerLayout, /* DrawerLayout object */
                    R.string.navigation_drawer_open, /* "open drawer" description for accessibility */
                    R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
            ) {

                //Called when a drawer has settled in a completely open state.
                override fun onDrawerOpened(drawerView: View) {
                    super.onDrawerOpened(drawerView)
                    if (!mlAdapterPopulated) {
                        setUpMLListView()
                    }
                }
            }

            drawerLayout!!.post { actionBarDrawerToggle.syncState() }
            drawerLayout!!.addDrawerListener(actionBarDrawerToggle)
        } else
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        restoreActionBar(false)

        StackLayoutUtils.initializeJsonObject()
    }

    override fun onStart() {
        super.onStart()
        setUpUserAgent()
        setUpOkHttpClient()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (!tabletMode)
            actionBarDrawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == INSTAGRAM_UPLOAD_REQUEST) {
            if(data != null) {
                Log.i("IG Upload",data.toString())
            }
        }
    }

    public override fun onResume() {
        super.onResume()

        if (isLoggedIn) {
            val currentFragment = supportFragmentManager.findFragmentById(R.id.container)
            //If we're in stacks, refresh them. If not, stay in whatever fragment we're in
            if (currentFragment is StacksFragment && !resumingFromStacksFragment)
            {
                if (!tabletMode) {
                    actionProgressBar!!.visibility = View.VISIBLE
                    actionProgressBar!!.isIndeterminate = true
                }

                mlItemSelected(currentMLItem, false, false)
            } else if (currentFragment is StacksFragment)
                resumingFromStacksFragment = false

            broadcastRegistered = true
            registerReceiver(notificationReceiver, IntentFilter("update_notifications"))
        } else {
            returnToLogin()
        }
    }

    private fun setUpUserAgent() {

        val versionNumber = BuildConfig.VERSION_NAME.replace(".", "")

        val expionPrefSettings = getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)
        val prefEditor = expionPrefSettings.edit()

        val userAgent = WebView(this).settings.userAgentString

        if (tabletMode) {
            val androidUserAgent = "Expion-AndroidTablet-$versionNumber-$userAgent"
            prefEditor.putString(resources.getString(R.string.android_user_agent), androidUserAgent)
        } else {
            val androidUserAgent = "Expion-AndroidPhone-$versionNumber-$userAgent"
            prefEditor.putString(resources.getString(R.string.android_user_agent), androidUserAgent)
        }

        prefEditor.apply()
    }

    fun setUpOkHttpClient() {
        val cookieManager = CookieManager.getInstance()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            cookieManager.flush()

        cookieCache = MyCookieCache()
        cookieJar = PersistentCookieJar(cookieCache, SharedPrefsCookiePersistor(applicationContext))

        val expionPrefSettings = getSharedPreferences(getString(R.string.stored_prefs_name), Context.MODE_PRIVATE)
        val androidUserAgent = expionPrefSettings.getString(getString(R.string.android_user_agent), "")

        client = OkHttpClient().newBuilder()
                .followRedirects(true)
                .followSslRedirects(true)
                .cookieJar(cookieJar)
                .addNetworkInterceptor(UserAgentInterceptor(androidUserAgent!!))
                .connectTimeout(45, TimeUnit.SECONDS) // connect timeout
                .readTimeout(45, TimeUnit.SECONDS)
                .build()


        val observable = createObservable()
        observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer)
    }

    /*
        Setting up ML ListView Section
     */

    private fun setUpAdapter(parsedMLData: LinkedHashMap<MLItem, ArrayList<MLItem>>) {
        newMlAdapter = MLExpandableListAdapter(this, parsedMLData, true)

        setUpMLListViewListeners()
    }

    private fun setUpMLListView() {
        if(newMlAdapter != null) {
            mlListView!!.setAdapter(newMlAdapter)
            mlAdapterPopulated = true

            try {
                if(!lastGroupSelectedIndexStack.isEmpty()) {
                    mlListView!!.expandGroup(lastGroupSelectedIndexStack.peekLast())

                    if (lastChildSelectedIndex != -1) {
                        try {
                            lastChildSelectedIndex = mlListView!!.getFlatListPosition(ExpandableListView.getPackedPositionForChild(homeItemsCount, lastChildSelectedIndex))
                            mlListView!!.setItemChecked(lastChildSelectedIndex, true)
                        } catch (exc: NullPointerException) {
                            exc.printStackTrace()
                        }

                    }
                }
            } catch (exc: NoSuchElementException) {
                exc.printStackTrace()
            }
        }
        else {
            longSnackbar(mainContainerView!!, "Error setting up ML")
            returnToLogin()
        }

    }

    private fun setUpMLListViewListeners() {
        mlListView!!.setOnChildClickListener { parent: ExpandableListView, v: View, groupPosition: Int, childPosition: Int, id: Long ->
            selectItem(false, groupPosition, childPosition)
            lastChildSelectedIndex = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition))
            mlListView!!.setItemChecked(lastChildSelectedIndex, true)
            true
        }

        mlListView!!.setOnGroupClickListener { _: ExpandableListView, _: View, groupPosition: Int, _: Long ->
            selectItem(true, groupPosition, 0)
            false
        }

        mlListView!!.setOnGroupExpandListener(object : ExpandableListView.OnGroupExpandListener {
            internal var previousItem = -1

            override fun onGroupExpand(groupPosition: Int) {
                if (groupPosition != previousItem) {
                    mlListView!!.collapseGroup(previousItem)
                } else {
                    if (lastChildSelectedIndex != -1) {
                        mlListView!!.setItemChecked(lastChildSelectedIndex, true)
                    }
                }

                previousItem = groupPosition
            }
        })

        runOnUiThread {

            mlListView!!.setOnScrollListener(object : AbsListView.OnScrollListener {

                override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {}

                override fun onScroll(view: AbsListView, firstVisibleItem: Int,
                                      visibleItemCount: Int, totalItemCount: Int) {
                    var enable = false
                    if (mlListView != null && mlListView!!.childCount > 0) {
                        // check if the first item of the list is visible
                        val firstItemVisible = mlListView!!.firstVisiblePosition == 0
                        // check if the top of the first item is visible
                        val topOfFirstItemVisible = mlListView!!.getChildAt(0).top == 0
                        // enabling or disabling the refresh_icon layout
                        enable = firstItemVisible && topOfFirstItemVisible
                    }
                    swipeRefreshLayout!!.isEnabled = enable
                }
            })
        }
    }

    /*
        MLItem Selection Section
     */
    private fun mlItemSelected(mlItem: MLItem, setLastSelected: Boolean, delayedInjection: Boolean) {
        Log.d("Stacks","MlItemSelected")
        if (debugMode)
            LoggingUtils.logTimestamp(this, "MLItemSelected", logToTextView)

        client.dispatcher().cancelAll()

        currentMLItem = mlItem
        //setLayoutId(currentMLItem)

        val classString = "com.expion.expion.DynamicClasses.${StringParsingUtilities.convertCodeClassNameForDCI(mlItem.code_class_name)}Class"

        when {
            classString.toLowerCase().contains("mlstackclass") -> {
                if(delayedInjection) {
                    initDCI("com.expion.expion.DynamicClasses.MLStackClass", arrayListOf(), arrayListOf(), delayedInjection)
                }
                checkDeployedStatusLayout(mlItem, classString, setLastSelected, delayedInjection)
                //checkDeployedStatusLayout(currentMLItem, classString, setLastSelected, false)
            }
            classString.toLowerCase().contains(getString(R.string.home_workspace)) -> {
                val connectUrl = "$myUrl/api/ml/GetUserHomeWorkspace/$company_id"
                populateHomeWorkspace(connectUrl, classString)
            }
            else -> initDCI(classString, null, arrayListOf(), false)
        }
    }

    private fun populateHomeWorkspace(connectUrl: String, classString: String) {

        val request = Request.Builder()
                .url(connectUrl)
                .get()
                .build()

        Log.d("Stacks","Requesting: $connectUrl")

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(mainContainerView!!, "Error Populating Home Workspace: " + response.message())
                    } else {
                        try {
                            val responseString = responseBody.string()
                            currentMLItem.layout_id = getLayoutId(JSONObject(responseString))
                            checkDeployedStatusLayout(currentMLItem, classString, false, false)
                        } catch (jsonExc: JSONException) {
                            jsonExc.printStackTrace()
                        }

                    }
                }
            }
        })
    }

    @Throws(JSONException::class)
    private fun getLayoutId(jsonObject: JSONObject): Int {
        return jsonObject.getJSONObject("directive").getJSONObject("attributes").getInt(getString(R.string.layout_id))
    }

    private fun selectItem(selectAsGroup: Boolean, groupPosition: Int, childPosition: Int) {
        mlListView!!.clearChoices()

        val selectedML: MLItem?
        if (selectAsGroup)
            selectedML = newMlAdapter?.getGroup(groupPosition)
        else
            selectedML = newMlAdapter?.getChild(groupPosition, childPosition)

        if (selectedML != null) {
            if (!tabletMode && !selectedML.folder_ind) {
                drawerLayout!!.closeDrawer(GravityCompat.START)
            } else {
                toggleLeftSectionExpanded(true)
            }

            if (!selectedML.folder_ind) {
                lastGroupSelectedIndexStack.addLast(groupPosition)
                mlItemSelected(selectedML, true, false)
            }
        }
    }

    /*
        End MLItem Selection Section
     */
    private fun toggleLeftSectionExpanded(showText: Boolean) {
        if (tabletMode) {
            newMlAdapter?.setShowText(showText)
            newMlAdapter?.notifyDataSetChanged()
        }
    }

    fun checkDeployedStatusLayout(mlItem: MLItem?, classString: String, setLastSelected: Boolean, delayedInjection: Boolean) {
        if (mlItem!!.deployed_status != null && mlItem.deployed_status == "DEPLOYED") {
            val connectUrl = "${cloneUrl}${mlItem.id}/$company_id?clone="

            val request = Request.Builder()
                    .cacheControl(CacheControl.Builder()
                            .maxAge(0, TimeUnit.SECONDS)
                            .build())
                    .url(connectUrl)
                    .get()
                    .build()

            Log.d("Stacks","Requesting: $connectUrl")

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    response.body()!!.use { responseBody ->
                        if (!response.isSuccessful) {
                            longSnackbar(mainContainerView!!, "Error Checking Deployed Status: " + response.message())
                        } else {
                            try {
                                mlItem.layout_id = JSONObject(responseBody.string()).getInt("ClonedStackID")
                                if (setLastSelected) {
                                    getStackLayoutJson(classString, delayedInjection)
                                    setLastSelectedWorkspace(mlItem)
                                }
                                else
                                    getStackLayoutJson(classString, delayedInjection)

                            } catch (jsonExc: JSONException) {
                                jsonExc.printStackTrace()
                            }

                        }
                    }
                }
            })
        } else {
            getStackLayoutJson(classString, delayedInjection)
            if (setLastSelected)
                setLastSelectedWorkspace(mlItem)
        }
    }

    private fun getStackLayoutJson(classString: String, delayedInjection: Boolean) {
        Log.d("Stacks", "Getting StacksLayout")
        val connectUrl = "${stackLayoutConnectionUrl}${currentMLItem.layout_id}"

        val request = Request.Builder()
                .cacheControl(CacheControl.Builder()
                        .maxAge(0, TimeUnit.SECONDS)
                        .build())
                .url(connectUrl)
                .get()
                .build()

        Log.d("Stacks","Requesting: $connectUrl")

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(mainContainerView!!, "Error Getting Stack Layout: " + response.message())
                    } else {
                        Log.d("Stacks","Response: Getting StacksLayout")
                        try {
                            Log.d("Stacks", "Response from StacksLayout")
                            val responseString = responseBody.string()
                            val jsonStackLayoutArray = JSONArray(responseString)
                            val responseUrl = response.request().url().toString()
                            if (responseUrl.toLowerCase().contains("api/stacklayout")) {
                                Log.d("Stacks", "$jsonStackLayoutArray")
                                parseStackLayoutJSON(jsonStackLayoutArray, classString, delayedInjection)
                            }
                            else
                                longSnackbar(mainContainerView!!, "Invalid Server Response Getting Stack Layout: " + response.message())
                        } catch (jsonExc: JSONException) {
                            longSnackbar(mainContainerView!!, "JSON Parse Error Getting Stack Layout: " + jsonExc.message)
                            jsonExc.printStackTrace()
                        }

                    }
                }
            }
        })
    }

    private fun parseStackLayoutJSON(jsonStackLayoutArray: JSONArray?, stackClass: String, delayedInjection: Boolean) {
        Log.d("Stacks", "Parsing StacksLayout")
        if (debugMode)
            runOnUiThread { LoggingUtils.logTimestamp(this, "ParsingStackLayout", logToTextView) }

            try {

                if (jsonStackLayoutArray != null) {
                    val layoutData = jsonStackLayoutArray.getJSONObject(0)
                    if(layoutData != null) {
                        val stackDetailsArray = gson.fromJson<ArrayList<StackDetail>>(layoutData.getJSONArray("stack_details").toString(), object : TypeToken<ArrayList<StackDetail>>() {}.type)
                        val stackIdArray = ArrayList<String>()
                        val columnCounts = ArrayList<Int>()
                        for (stackDetail in stackDetailsArray) {

                            Log.d("Stacks", "Stack ID: ${stackDetail.id.toString()}")
                            stackIdArray.add(stackDetail.id.toString())
                            columnCounts.add(stackDetail.columnCount!!)
                        }
                        if(!delayedInjection) {
                            initDCI(stackClass, stackIdArray, columnCounts, delayedInjection)
                        }
                        else {
                            injectStackInfo(stackIdArray,columnCounts)
                        }

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        //}

        if (debugMode)
            runOnUiThread { LoggingUtils.logTimestamp(this, "DoneParsingStackLayout", logToTextView) }

    }

    private fun injectStackInfo(stackIdArray: ArrayList<String>, columnCounts: ArrayList<Int>) {
        Log.d("Stacks", "Injecting Stack Info")
        baseClassSettings.setStacksSettings(stackIdArray, columnCounts, resumingFromStacksFragment, true, "Workspaces", true)

        val currentFragment = currentFragment
        if (currentFragment != null) {
            Log.d("Stacks","Building Injection Strings")
            val injectionStrings = StackLayoutUtils.buildInjectionStrings(currentMLItem.id, currentMLItem.layout_id, stackIdArray)
            (currentFragment as StacksFragment).injectStackData(injectionStrings, columnCounts,stackIdArray)
            Log.d("Stacks","Calling Populate ViewPager from injectStackInfo")
            if(!tabletMode)
                currentFragment.populateViewPager(false, true)
            else
                currentFragment.populateAdapter(false, true)
        }
        else  {
            Log.d("Stacks","Current Fragment is Null")
        }
    }

    private fun initDCI(classString: String?, stackIdArray: ArrayList<String>?, columnCounts: ArrayList<Int>, delayedInjection: Boolean) {
        var absoluteClassPath = classString
        if (classString != null) {
            baseClassSettings.setMlItem(currentMLItem)

            if(stackIdArray != null)
            {
                when (classString) {
                    "com.expion.expion.DynamicClasses.MLStackClass" ->
                        baseClassSettings.setStacksSettings(stackIdArray, columnCounts, resumingFromStacksFragment, true, "Workspaces", delayedInjection)
                    "com.expion.expion.DynamicClasses.HomeWorkspaceClass" -> {
                        baseClassSettings.setStacksSettings(stackIdArray, columnCounts, resumingFromStacksFragment, true, "HomeWorkspace", delayedInjection)
                        absoluteClassPath = "com.expion.expion.DynamicClasses.MLStackClass"
                    }
                }
            }
            else {
                when (classString) {
                    "com.expion.expion.DynamicClasses.MobileApprovalClass" -> baseClassSettings.setPhotosSettings("approval")
                    "com.expion.expion.DynamicClasses.MobileUploaderClass" -> baseClassSettings.setPhotosSettings("uploader")
                    }
                }
            }

            try {
                val myClass = Class.forName(absoluteClassPath)
                val methodToInvoke = "Execute"
                val m = myClass.getMethod(methodToInvoke)
                val ctor = myClass.getConstructor(ExpionMLBaseClassSettings::class.java)
                val obj = ctor.newInstance(baseClassSettings)
                m.invoke(obj)
            } catch (exc: Exception) {
                if (exc.message != null) {
                    Log.e("DCI Exception", exc.message)
                }
            }

    }

    /*
        Overridden interface methods
     */

    private fun parseCompleted(parsedMLData: LinkedHashMap<MLItem, ArrayList<MLItem>>, refreshing: Boolean) {
        if (!tabletMode) {
            runOnUiThread {
                hideActionProgressBar()
                actionBarDrawerToggle.isDrawerIndicatorEnabled = true
                actionBarDrawerToggle.syncState()
            }
        }
        setUpAdapter(parsedMLData)
        if(refreshing || tabletMode) {
            setUpMLListView()
            swipeRefreshLayout?.isRefreshing = false
        }
    }

    override fun updateResumingStacksFromFragment(resumingStacksFromFragment: Boolean) {
        resumingFromStacksFragment = resumingStacksFromFragment
    }

    override fun loginCompleted(isLoggedIn: Boolean, debugMode: Boolean, overrideCookies: Boolean) {
        this.debugMode = debugMode
        this.isLoggedIn = isLoggedIn
        logToTextView = debugMode
        if (isLoggedIn) {

            if (debugMode) {

                runOnUiThread {
                    setupLoggingView()
                }

                LoggingUtils.logTimestamp(this, "LoginCompleted", logToTextView)
            }

            this.myUrl = getSharedPreferences(getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE).getString(getString(R.string.base_url), "")
            this.company_id = getSharedPreferences(getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE).getString(getString(R.string.company_id), "")
            this.userName = getSharedPreferences(getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE).getString(getString(R.string.UserName), "")

            Log.d("Stacks","LoginCompleted")

            /*val mlItem = retrieveCurrentMLItem()

            if(mlItem != null) {
                currentMLItem = mlItem
            }*/

            /*getMLJsonConnectURL = "$myUrl/API/ML/User?company_id=$company_id"
            okHttpGetML(false)*/

            runOnUiThread {

                if (!tabletMode) {
                    actionProgressBar!!.visibility = View.VISIBLE
                    actionProgressBar!!.isIndeterminate = true
                }
            }

            val accessTokenString = getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE).getString("access_token", "")
            var accessTokenCookie = ""

            if (ssoEnabled) {
                try {
                    val `object` = JSONObject()
                    `object`.put("access_token", accessTokenString)
                    accessTokenCookie = `object`.toString()
                } catch (exc: JSONException) {
                    exc.printStackTrace()
                }

            }

            val loginDialog = supportFragmentManager.findFragmentByTag("loginDialog")
            try {
                if (loginDialog != null) {
                    val df = loginDialog as DialogFragment
                    df.dismiss()
                }
            } catch (exc: IllegalStateException) {
                exc.printStackTrace()
            }

            val expionPrefSettings = getSharedPreferences(resources.getString(R.string.stored_prefs_name), Context.MODE_PRIVATE)
            //String firebaseToken = FirebaseInstanceId.getInstance().getToken();
            //registerDeviceForNotifications(true);

            val prefEditor = expionPrefSettings.edit()
            prefEditor.putString(resources.getString(R.string.firebase_token_needs_updating), "false")
            prefEditor.apply()

            var host = "sysomos.com"
            if(overrideCookies)
                host = URL(myUrl).host

            stackLayoutConnectionUrl = "$myUrl/API/stacklayout/$company_id?layout_id="
            cloneUrl = "$myUrl/api/ml/deployws/clonewsforuser/"
            loadUrl = "$myUrl/ML/ComponentLoader?company_id=$company_id"

            val cookieManager = CookieManager.getInstance()
            cookieManager.setAcceptCookie(true)
            cookieManager.setCookie("ls.access", accessTokenCookie)

            val cookies = MyCookieCache.getCookies()
            var cookieValue = ""
            var aspxAuthCookie: Cookie? = null
            var expicoCookie: Cookie? = null

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                expicoCookie = cookies!!.stream().filter { c -> c.domain().contains(host) && c.name().contains("expion.co") }.findFirst().orElse(null)
                aspxAuthCookie = cookies.stream().filter { c -> c.domain().contains(host) && c.name().contains("ASPXAUTH") && c.value().isNotEmpty() }.findAny().orElse(null)
            }
            else {

                for(it in cookies!!) {
                    if(it.domain().contains(host)) {
                        if(it.name().contains("ASPXAUTH"))
                            aspxAuthCookie = it
                        else if(it.name().contains("expion.co"))
                            expicoCookie = it
                    }
                    if(it.name().contains("ASPXAUTH"))
                        aspxAuthCookie = it
                    else if(it.name().contains("expion.co"))
                        expicoCookie = it
                }
            }
            if (aspxAuthCookie != null) {
                cookieValue = if(overrideCookies)
                    aspxAuthCookie.toString()
                else
                    "${aspxAuthCookie.name()}=${aspxAuthCookie.value()}"

                getMLJsonConnectURL = "$myUrl/API/ML/User?company_id=$company_id"
                okHttpGetML(false)
            }
            else
            {
                longSnackbar(mainContainerView!!, "Error retrieving cookies from server")
                returnToLogin()
            }

            if(expicoCookie != null) {
                Log.d("CookieInContainer",cookieValue)
                cookieValue = if(overrideCookies)
                    "$cookieValue; $expicoCookie"
                else
                    "$cookieValue; ${expicoCookie.name()}=${expicoCookie.value()}"
                baseClassSettings = ExpionMLBaseClassSettings(supportFragmentManager, myUrl, company_id, loadUrl, tabletMode, cookieValue, debugMode)
            }
            else
            {
                longSnackbar(mainContainerView!!, "Error retrieving cookies from server")
                returnToLogin()
            }

        } else {
            longSnackbar(mainContainerView!!, "Logged out")
            returnToLogin()
        }
    }

    fun setupLoggingView() {
        if(debugMode)
            logging_layout.visibility = View.VISIBLE
        else
            logging_layout.visibility = View.GONE

        var expanded = false

        loggingTextView!!.movementMethod = ScrollingMovementMethod()

        if (logToTextView) {
            logging_layout!!.visibility = View.VISIBLE
        } else
            logging_layout!!.visibility = View.GONE

        copy_to_clipboard!!.setOnClickListener  {
            copyToClipboard()
        }

        expand_logging_view!!.setOnClickListener {
            if(!expanded) {
                expand_logging_view.setImageResource(R.drawable.collapse_icon)
                loggingTextView.maxLines = 125
                expanded = true
            }
            else {
                expand_logging_view.setImageResource(R.drawable.expand_icon)
                loggingTextView.maxLines = 8
                expanded = false
            }
        }

        delete_logging_view!!.setOnClickListener {
            deleteLoggingViewText()
        }
    }

    private fun okHttpGetML(refreshing: Boolean) {
        runOnUiThread { progressBar = ProgressDialog.show(this, getString(R.string.app_name), "Retrieving Workspaces") }

        if (debugMode)
            runOnUiThread { LoggingUtils.logTimestamp(this, "GettingMLJSON", logToTextView) }

        val request = Request.Builder()
                .cacheControl(CacheControl.Builder()
                        .maxAge(0, TimeUnit.SECONDS)
                        .build())
                .url(getMLJsonConnectURL)
                .get()
                .build()

        Log.d("Stacks","Requesting: Getting MLJSON")

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                if (progressBar.isShowing)
                    progressBar.dismiss()
                longSnackbar(mainContainerView!!, getString(R.string.ml_error) + e.message)
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        if (progressBar.isShowing)
                            progressBar.dismiss()
                        longSnackbar(mainContainerView!!, getString(R.string.ml_error) + response.message())
                    } else {

                        Log.d("Stacks","Response: Getting MLJSON")
                        try {
                            if (debugMode)
                                runOnUiThread { LoggingUtils.logTimestamp(this@ContainerActivity, "MLJSonResponse", logToTextView) }
                            parseMLJson(JSONObject(responseBody.string()), refreshing)
                        } catch (exc: NullPointerException) {
                            if (progressBar.isShowing)
                                progressBar.dismiss()
                            Log.e("GettingML", exc.message)
                        } catch (exc: JSONException) {
                            if (progressBar.isShowing)
                                progressBar.dismiss()
                            Log.e("GettingML", exc.message)
                        }

                    }
                }
            }
        })
    }

    private fun createObservable(): Observable<MLItem> {

        return Observable.never()
    }

    private fun createObserver(): Observer<MLItem> {

        return object : Observer<MLItem> {
            override fun onSubscribe(@NonNull d: Disposable) {}

            override fun onNext(@NonNull mlItem: MLItem) {
                //mlItemSelected(mlItem, false)
                val classString = if(mlItem.code_class_name == "homeworkspace") "com.expion.expion.DynamicClasses.HomeWorkspaceClass" else "com.expion.expion.DynamicClasses.MLStackClass"
                currentMLItem = mlItem
                if(mlItem.code_class_name != "homeworkspace") {
                    initDCI(classString, arrayListOf(), arrayListOf(), true)
                    checkDeployedStatusLayout(mlItem, classString, false, true)
                }
                else {
                    val connectUrl = "$myUrl/api/ml/GetUserHomeWorkspace/$company_id"
                    populateHomeWorkspace(connectUrl, classString)
                }
                if (progressBar.isShowing)
                    progressBar.dismiss()
            }

            override fun onError(@NonNull e: Throwable) {}

            override fun onComplete() {}
        }
    }

    private fun parseMLJson(mlJson: JSONObject, refreshing: Boolean) {


        Log.d("Stacks","Parsing MLJson")
        homeItemsCount = 0

        if (debugMode)
            LoggingUtils.logTimestamp(this, "ParsingMLJSON", logToTextView)

        val mLItemsMap = LinkedHashMap<MLItem, ArrayList<MLItem>>()
        lastGroupSelectedIndexStack.clear()

        /*val headerHomeItems = mlJson.optJSONArray("header_home_items")
        if (headerHomeItems != null) {
            val headerHomeItemsArray = gson.fromJson<ArrayList<MLItem>>(headerHomeItems.toString(), object : TypeToken<ArrayList<MLItem>>() {
            }.type)

            for(mlItem in headerHomeItemsArray)
                if(mlItem.name == "Insights") {
                    notificationsEnabled = true
                    break
                }
        }*/

        val homeItems = mlJson.optJSONArray(getString(R.string.home_items))
        if (homeItems != null) {
            val homeItemsArray = gson.fromJson<ArrayList<MLItem>>(homeItems.toString(), object : TypeToken<ArrayList<MLItem>>() {

            }.type)
            for (mlItem in homeItemsArray) {
                val children = mlItem.children
                mLItemsMap.put(mlItem, children!!)
                if(mlItem.name == "Insights") {
                    notificationsEnabled = true
                }
            }
            homeItemsCount = homeItems.length()
        }

        invalidateOptionsMenu()

        val handler = Handler(Looper.getMainLooper())
        handler.postAtFrontOfQueue {

            val userHomeItems = mlJson.optJSONArray(getString(R.string.user_home_items))
            if (userHomeItems != null) {
                val id = -1
                val company_id = 0
                val name = "My Workspaces"
                val order_ind = 0
                val settings = ""
                val code_class_name = "workspaces"
                val css_class_name = "workspaces"
                val folder_ind = true

                Log.d("Stacks","Parsing UserHomeItems")

                userHomeItemsArray = parseUserHomeItems(mlJson, mlJson.optInt("last_selected"), handler)

                mlWorkspacesItem = MLItem.Builder(id, company_id, name, settings, code_class_name, css_class_name)
                        .children(userHomeItemsArray).order_ind(order_ind).folder_ind(folder_ind).build()

                mLItemsMap.put(mlWorkspacesItem, userHomeItemsArray)

                parseCompleted(mLItemsMap, refreshing)
            }
            else {
                if (!checkForHomeworkspaceMLItem(observer, mlJson)) {
                    runOnUiThread {
                        if(progressBar.isShowing) {
                            progressBar.hide()
                        }
                    }
                }
                parseCompleted(mLItemsMap, refreshing)
            }
        }
    }

    private fun parseUserHomeItems(mlJson: JSONObject, last_selected_id: Int, handler: Handler)
            : ArrayList<MLItem> {
        if (debugMode)
            LoggingUtils.logTimestamp(this, "ParsingHomeItems", logToTextView)

        val userHomeItems = mlJson.optJSONArray(resources.getString(R.string.user_home_items))

        return if (userHomeItems != null && userHomeItems.length() > 0) {
            val returnItems = gson.fromJson<ArrayList<MLItem>>(userHomeItems.toString(), object : TypeToken<ArrayList<MLItem>>() {}.type)

            Log.d("Stacks","Determining ML Selection")
            determineMLSelection(mlJson, last_selected_id, returnItems, handler)

            returnItems
        } else {
            ArrayList(emptyList<MLItem>())
        }
    }

    private fun determineMLSelection(mlJson: JSONObject, last_selected_id: Int, returnItems: ArrayList<MLItem>, handler: Handler)
    {
        /*val handler = Handler(Looper.getMainLooper())
        handler.postAtFrontOfQueue {*/
            for (i in returnItems.indices) {
                val mlItem = returnItems[i]
                setLayoutId(mlItem)
                if (mlItem.id == last_selected_id) {
                    lastChildSelectedIndex = i
                    lastGroupSelectedIndexStack.addLast(homeItemsCount) //Add "My Workspaces" index
                    handler.post({
                        observer.onNext(mlItem)

                        Log.d("Stacks","Selecting ML Item #$i")
                    })

                    if (debugMode) {
                        runOnUiThread { LoggingUtils.logTimestamp(this, "Selecting MLItem - " + mlItem.name, logToTextView) }
                    }
                    //break
                }/*
                else {
                    Log.d("Stacks","Setting Layout Id #$i")
                    setLayoutId(mlItem)
                }*/
            }

            if (lastChildSelectedIndex == -1) {
                if (!checkForHomeworkspaceMLItem(observer, mlJson)) {
                    lastGroupSelectedIndexStack.addLast(homeItemsCount)
                    lastChildSelectedIndex = 0

                    Log.d("Stacks","Selecting ML Item #0")
                    observer.onNext(returnItems[0])
                    if (debugMode) {
                        runOnUiThread { LoggingUtils.logTimestamp(this, "Selecting MLItem - " + returnItems[0].name, logToTextView) }
                    }
                }
            }
            if (debugMode)
                LoggingUtils.logTimestamp(this, "DoneParsingHomeItems", logToTextView)
        //}
    }

    private fun setLayoutId(mlItem: MLItem)
    {
        //launch(CommonPool) {
            if (!mlItem.settings.isEmpty()) {
                try {
                    val settingsJSONObject = JSONObject(mlItem.settings)
                    val directiveJsonObject = settingsJSONObject.optJSONObject("directive")
                    if(directiveJsonObject != null) {
                        val directiveJSON = gson.fromJson<MLDirective>(directiveJsonObject.toString(), object : TypeToken<MLDirective>() {}.type)
                        //val directiveJSON = gson.fromJson<MLDirective>(JSONObject(mlItem.settings).optJSONObject("directive").toString()), object : TypeToken<MLDirective>() {}.type)
                        //if (directiveJSON != null) {
                        if(directiveJSON?.attributes != null && directiveJSON.attributes!!.layoutId != null) {
                            mlItem.layout_id = directiveJSON.attributes!!.layoutId!!
                        }
                        /*val attributesJSON = directiveJSON.getJSONObject("attributes")
                        if (attributesJSON != null) {
                            val layout_idInt = attributesJSON.optInt(getString(R.string.layout_id))
                            if (!attributesJSON.isNull(getString(R.string.layout_id))) {
                                mlItem.layout_id = layout_idInt
                            }
                        }*/
                        //}
                    }
                } catch (exc: JSONException) {
                    Log.e("JSONParsing", exc.message)
                }

            }
        //}
    }

    private fun checkForHomeworkspaceMLItem(observer: Observer<MLItem>, mlJson: JSONObject): Boolean {
        val homeItems = mlJson.optJSONArray(getString(R.string.home_items))
        if (homeItems != null && homeItems.length() > 0) {
                try {
                    val mlJsonObject = homeItems.get(0) as JSONObject
                    if (mlJsonObject.optString("name") == "Home Workspace") {
                        Log.d("Stacks","Selecting HomeWorkspace ML Item")
                        observer.onNext(gson.fromJson(mlJsonObject.toString(), object : TypeToken<MLItem>() {}.type))
                        homeWorkspacePresent = true
                        lastGroupSelectedIndexStack.addLast(0)
                    }
                } catch (exc: JSONException) {
                    exc.printStackTrace()
                }
        }
        return homeWorkspacePresent
    }


    private fun registerDeviceForNotifications(registerToggle: Boolean) {

        val preferences = getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)

        val connectUrl = myUrl + notificationsProxy
        val notificationEndpoint = "/api/user_devices"

        val company_id = preferences.getString(resources.getString(R.string.company_id), "")
        val username = preferences.getString(resources.getString(R.string.UserName), "")

        val jsonParams = JSONObject()

        try {
            val dataJson = JSONObject()
            dataJson.put("company_id", company_id)
            dataJson.put("sso_user_nm", username)
            dataJson.put("server_nm", "t1")
            dataJson.put("device_type", "android")
            dataJson.put("device_token", FirebaseInstanceId.getInstance().token)
            dataJson.put("active_ind", registerToggle)

            jsonParams.put("data", dataJson)
            jsonParams.put("method", "post")
            jsonParams.put("endpoint", notificationEndpoint)
        } catch (exception: Exception) {

        }

        val body = RequestBody.create(JSON, jsonParams.toString())

        val request = Request.Builder()
                .url(connectUrl)
                .post(body)
                .build()

        Log.d("Stacks","Requesting: $connectUrl")

        Log.d("NotificationsJSON", jsonParams.toString())


        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(mainContainerView!!, "Error Registering Device for Notifications: " + response.message())
                    } else {
                        println(responseBody.string())
                    }
                }
            }
        })
    }

    override fun dialogClosed() {
        restoreActionBar(true)
    }

    /*
        RefreshListeners
     */
    override fun onRefresh() {
        if (!tabletMode) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
            actionProgressBar!!.visibility = View.VISIBLE
        }
        okHttpGetML(true)
    }

    /*
        End overridden interface methods
     */
    fun returnToLogin() {
        runOnUiThread {
            dialogStackLevel++

            removeFragments()

            val loginFragment = supportFragmentManager.findFragmentByTag("loginDialog")
            if ((loginFragment != null && !loginFragment.isAdded) || loginFragment == null) {
                try {
                    val dialogFragment = LoginFragment.newInstance(dialogStackLevel)
                    dialogFragment.isCancelable = false
                    dialogFragment.showsDialog = false
                    dialogFragment.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.PopupTheme)
                    dialogFragment.show(supportFragmentManager, "loginDialog")

                    supportFragmentManager.executePendingTransactions()
                } catch (exc: IllegalStateException) {
                    if (loginFragment != null)
                        (loginFragment as DialogFragment).show(supportFragmentManager, "loginDialog")

                    supportFragmentManager.executePendingTransactions()

                }

            } else {

                try {
                    (loginFragment as DialogFragment).show(supportFragmentManager, "loginDialog")

                    supportFragmentManager.executePendingTransactions()
                } catch (exc: IllegalStateException) {
                    Log.e("AddingFragment", exc.message)
                }

            }
        }
    }


    private fun removeFragments() {
        try {
            if (supportFragmentManager != null && supportFragmentManager.backStackEntryCount > 0) {
                for (i in 0 until supportFragmentManager.backStackEntryCount) {
                    val backEntry = supportFragmentManager.getBackStackEntryAt(i)
                    val str = backEntry.name
                    val fragment = supportFragmentManager.findFragmentByTag(str)
                    if (fragment != null)
                        supportFragmentManager.beginTransaction().remove(fragment).commit()
                }
            }
        } catch (npe: NullPointerException) {
            npe.printStackTrace()
        }

    }

    fun updateNotificationBadgeCount() {

        val preferences = getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)

        val connectUrl = myUrl + notificationsProxy
        val notificationEndpoint = "/api/notifications"


        val jsonParams = JSONObject()

        try {
            val dataJson = JSONObject()
            dataJson.put("company_id", preferences.getString(resources.getString(R.string.company_id), ""))
            dataJson.put("sso_user_nm", preferences.getString(resources.getString(R.string.UserName), ""))
            dataJson.put("page_id", 1)

            jsonParams.put("data", dataJson)
            jsonParams.put("method", "get")
            jsonParams.put("endpoint", notificationEndpoint)
        } catch (exception: Exception) {

        }

        val body = RequestBody.create(JSON, jsonParams.toString())

        val request = Request.Builder()
                .url(connectUrl)
                .post(body)
                .build()

        Log.d("Stacks","Requesting: $connectUrl")

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(mainContainerView!!, "Error Updating Notifications Badge Count: " + response.message())
                    } else {
                        val jsonObject = JSONObject(responseBody.string())
                        val unreadCount = jsonObject.optString("unread_count")
                        if (unreadCount == "0")
                            itemMessagesBadgeTextView!!.visibility = View.GONE
                        else {
                            itemMessagesBadgeTextView!!.visibility = View.VISIBLE
                            itemMessagesBadgeTextView!!.text = unreadCount
                        }

                        val expionPrefSettings = getSharedPreferences(resources.getString(R.string.stored_prefs_name), Context.MODE_PRIVATE)
                        expionPrefSettings.edit()
                                .putString("unread_count", unreadCount)
                                .apply()
                    }
                }
            }
        })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        menu.findItem(R.id.url_switch).isVisible = false

        val refreshDrawable = menu.getItem(0).icon
        refreshDrawable.mutate()
        refreshDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_IN)

        val userNameItem = menu.findItem(R.id.userPrettyName)
        userNameItem.title = userName

        if (debugMode) {
            logsSwitchItem = menu.findItem(R.id.logs_switch)
            logsSwitchItem.setActionView(R.layout.log_view_action_bar_switch_layout)

            val logsSwitchActionView = logsSwitchItem.actionView
            val logViewSwitch = logsSwitchActionView.log_view_switch

            logViewSwitch.showText = true
            logViewSwitch.setOnClickListener { v: View ->
                val checked = (v as SwitchCompat).isChecked
                logToTextView = !checked
                if (checked) {
                    logging_layout!!.visibility = View.GONE
                } else {
                    logging_layout!!.visibility = View.VISIBLE
                }
            }

            //logsSwitchItem.actionView = null

        } else {
            //if(logsSwitchItem == null) {
                logsSwitchItem = menu.getItem(3)
            //}
            logsSwitchItem.isVisible = false
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {

        val itemMessages = menu!!.findItem(R.id.action_show_notifications)
        if(notificationsEnabled) {
            MenuItemCompat.setActionView(itemMessages, R.layout.badge_layout)
            val badgeLayout = itemMessages.actionView as RelativeLayout
            val itemMessagesBadgeTextView = badgeLayout.findViewById<TextView>(R.id.itemMessagesBadgeTextView)

            val expionPrefSettings = getSharedPreferences(resources.getString(R.string.stored_prefs_name), Context.MODE_PRIVATE)
            val unreadCount = expionPrefSettings.getString("unread_count", "0")
            if (unreadCount == "0")
                itemMessagesBadgeTextView.visibility = View.GONE
            else {
                itemMessagesBadgeTextView.visibility = View.VISIBLE
                itemMessagesBadgeTextView.text = unreadCount
            }

            badgeLayout.setOnClickListener { _: View ->
                launchMobileNotifications()
                //updateNotificationBadgeCount()
            }
        }
        else {
            itemMessages.isVisible = false
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> {
                if (tabletMode) {
                    val isTextShowing = !newMlAdapter!!.isTextShowing
                    toggleLeftSectionExpanded(isTextShowing)
                    adjustListViewWidth(isTextShowing)
                } else {
                    if (actionBarDrawerToggle.onOptionsItemSelected(item)) {

                        return true
                    }
                }
                return true
            }
            R.id.logout -> {
                logout()
                return true
            }
            R.id.refresh_button -> {
                refreshWorkspace(0, StackUtils.STACK_REFRESHED)
                return true
            }
            R.id.action_show_notifications -> {
                launchMobileNotifications()
                return true
            }
            /*R.id.copy_to_clipboard -> {
                copyToClipboard()
                return true
            }*/
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun launchMobileNotifications() {
        var groupPosition = -1
        var notificationsMLItem: MLItem? = null
        val mlItemIterator = newMlAdapter?.mlData?.keys?.iterator()
        while (mlItemIterator!!.hasNext()) {
            groupPosition++
            notificationsMLItem = mlItemIterator.next()
            if (notificationsMLItem.name == "Insights")
                break
            else
                notificationsMLItem = null
        }
        if (notificationsMLItem != null) {
            setLayoutId(notificationsMLItem)
            //checkDeployedStatusLayout(mlItem, classString, setLastSelected, true)
            lastGroupSelectedIndexStack.addLast(groupPosition)
            if(mlListView != null && mlListView.adapter != null)
                mlListView.expandGroup(lastGroupSelectedIndexStack.peekLast())
            mlItemSelected(notificationsMLItem, true, false)
        }
    }

    private fun logout() {
        if (actionProgressBar != null)
            hideActionProgressBar()

        debugMode = false
        logToTextView = false
        logsSwitchItem.actionView = null

        invalidateOptionsMenu()

        setupLoggingView()
        //persistCurrentMLItem()

        lastGroupSelectedIndexStack.clear()

        lastChildSelectedIndex = -1

        isLoggedIn = false
        logoutFromServer()

        actionBarDrawerToggle.isDrawerIndicatorEnabled = true
        actionBarDrawerToggle.syncState()

        mlAdapterPopulated = false
        if (drawerLayout != null)
            drawerLayout!!.closeDrawers()
        returnToLogin()

    }

    private fun persistCurrentMLItem() {

        val mlJson = convertMLItemToJson()

        val expionPrefSettings = getSharedPreferences(getString(R.string.stored_prefs_name), Context.MODE_PRIVATE)
        expionPrefSettings.edit()
                .putString("CurrentMLItem",mlJson)
                .putInt("LastGroupMLIndex",lastGroupSelectedIndexStack.peekLast())
                .putInt("LastChildMLIndex",lastChildSelectedIndex)
                .apply()
    }

    private fun retrieveCurrentMLItem(): MLItem? {
        val expionPrefSettings = getSharedPreferences(getString(R.string.stored_prefs_name), Context.MODE_PRIVATE)
        val mlItemString = expionPrefSettings.getString("CurrentMLItem", "")
        if(mlItemString != null && mlItemString.isNotEmpty()) {
            return gson.fromJson(mlItemString, object : TypeToken<MLItem>() {}.type)
            lastGroupSelectedIndexStack.push(expionPrefSettings.getInt("LastGroupMLIndex",0))
            lastChildSelectedIndex = expionPrefSettings.getInt("LastChildMLIndex",0)
        }
        return null
    }

    private fun convertMLItemToJson(): String {
        return gson.toJson(currentMLItem)
    }

    private fun logoutFromServer() {
        val connectUrl = "$myUrl/Account/SSOLogOff/?ReturnUrl=$myUrl&token=${getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE).getString("access_token", "")}"

        val request = Request.Builder()
                .url(connectUrl)
                .get()
                .build()

        Log.d("Stacks","Requesting: $connectUrl")

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->
                    if (!response.isSuccessful) {
                        longSnackbar(mainContainerView!!, "Error Logging Out: " + response.message())
                    }
                }
            }
        })

        //deleteCache();
        cookieCache.clear()
        //cookieJar.clear();

        clearWebViewCaches()
        //registerDeviceForNotifications(false);
    }

    private fun deleteCache() {
        try {
            val dir = cacheDir
            deleteDir(dir)
        } catch (e: Exception) {
            Log.e("Cache","Error deleting")
        }

    }

    private fun adjustListViewWidth(expanding: Boolean) {
        val loparams = swipeRefreshLayout!!.layoutParams as LinearLayout.LayoutParams
        loparams.height = ViewGroup.LayoutParams.MATCH_PARENT

        val r = resources
        val expandedPixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 210f, r.displayMetrics)
        val contractedPixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50f, r.displayMetrics)

        if (!expanding)
            loparams.width = contractedPixels.toInt()
        else
            loparams.width = expandedPixels.toInt()

        swipeRefreshLayout!!.layoutParams = loparams
        swipeRefreshLayout!!.requestLayout()
    }

    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager
        val backStackEntryCount = fragmentManager.backStackEntryCount
        if (backStackEntryCount > 1) {
            val backEntry = fragmentManager.getBackStackEntryAt(backStackEntryCount - 1)
            val str = backEntry.name
            val fragment = fragmentManager.findFragmentByTag(str)

            if (fragment is OnBackPressedListener) {
                (fragment as OnBackPressedListener).onBackPressed()
            } else {
                lastGroupSelectedIndexStack.pollLast()

                val lastFragName = fragmentManager.getBackStackEntryAt(backStackEntryCount - 2).name

                fragmentManager.popBackStack()

                if (lastFragName == "Workspaces")
                    checkWorkspaceMLItem()
                else
                    mlListView!!.expandGroup(lastGroupSelectedIndexStack.peekLast()) //, true);
            }
        } else {
            //Log.d("BackStackCount",": " + backStackEntryCount);
        }
    }

    private fun restoreActionBar(setHomeButtonEnabled: Boolean) {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(setHomeButtonEnabled)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.menu_hamburger)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.expion_blue)))
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        supportActionBar!!.setDisplayUseLogoEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setIcon(R.drawable.action_bar_30)

        invalidateOptionsMenu()
    }

    private fun hideActionProgressBar() {
        runOnUiThread { actionProgressBar!!.visibility = View.GONE }
    }

    val isScreenLarge: Boolean
        get() {
            val screenSize = resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
            return screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE || screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE
        }

    fun setLastSelectedWorkspace(mlItem: MLItem?) {
        val connectUrl = "$myUrl/api/ML/$company_id"

        val jsonMLItem = gson.toJson(mlItem)
        val body = RequestBody.create(JSON, jsonMLItem)

        val request = Request.Builder()
                .url(connectUrl)
                .patch(body)
                .build()

        Log.d("Stacks","Requesting: $connectUrl")

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()

                longSnackbar(mainContainerView!!, resources.getString(R.string.session_timeout))
                returnToLogin()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->
                    if (!response.isSuccessful) {
                        longSnackbar(mainContainerView!!, "Error Updating Last Selected Workspace: " + response.message())
                    } else {
                        val url = response.request().url().toString()
                        if (url.contains("LogOn") || url.contains("LogOff")) {
                            longSnackbar(mainContainerView!!, resources.getString(R.string.session_timeout))
                            returnToLogin()
                        }
                        else {}
                    }
                }
            }
        })
    }

    private fun checkWorkspaceMLItem() {
        try {
            mlListView!!.expandGroup(lastGroupSelectedIndexStack.peekLast())
            mlListView!!.setItemChecked(lastChildSelectedIndex, true)
        } catch (NPE: NullPointerException) {
            Log.e("CheckWorkspaceMLItem", "Null Pointer")
        }

    }

    override fun logOutput(outputText: String) {

        runOnUiThread { loggingTextView!!.append(outputText) }
    }

    private fun copyToClipboard() {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        clipboard.primaryClip = ClipData.newPlainText("logs",loggingTextView!!.text)
    }

    private fun deleteLoggingViewText() {
        loggingTextView.text = ""
    }

    override fun stackLoaded(stackPositionLoaded: Int) {
        val currentFragment = currentFragment
        if (currentFragment != null) {
            //) {
                (currentFragment as StacksFragment).loadStack(stackPositionLoaded)
            //}
        }
    }

    override fun refreshWorkspace(position: Int, refreshType: Int) {
        val currentFragment = currentFragment
        if (currentFragment != null) {
            if(currentFragment is StacksFragment)
                currentFragment.refreshWorkspace(position, refreshType)
            else (currentFragment as? WebViewFragment)?.onResume()

        }
    }

    override fun deleteStack(position: Int) {
        val currentFragment = currentFragment
        if (currentFragment != null) {
            (currentFragment as StacksFragment).deleteStack(position)
        }
    }

    override fun insertStack(position: Int, stackId: Int) {
        val currentFragment = currentFragment
        if (currentFragment != null) {
            (currentFragment as StacksFragment).insertStack(position, stackId)
        }
    }

    private val currentFragment: Fragment?
        get() {
            var fragment: Fragment? = null
            try {
                val index = supportFragmentManager.backStackEntryCount - 1
                val backEntry = supportFragmentManager.getBackStackEntryAt(index)
                val tag = backEntry.name
                //Log.i("Stacks", "Name of Current Fragment: $tag")
                //TODO: Add MLWebView fragments in here too
                if (tag != null && tag.contains("Workspaces")) {
                    fragment = supportFragmentManager.findFragmentByTag(tag)
                }
            } catch (exc: ArrayIndexOutOfBoundsException) {
                Log.e("GetCurrentFragment", exc.message)
            }

            return fragment
        }

    public override fun onStop() {
        super.onStop()
        clearWebViewCaches()
    }

    public override fun onDestroy() {
        super.onDestroy()
        if(broadcastRegistered) {
            broadcastRegistered = false
            unregisterReceiver(notificationReceiver)
        }
    }

    private fun clearWebViewCaches() {
        //if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            try {
                val index = supportFragmentManager.backStackEntryCount - 1
                if (index >= 0) {
                    val backEntry = supportFragmentManager.getBackStackEntryAt(index)
                    val tag = backEntry.name
                    if (tag != null && tag.contains("Workspace")) {
                        val stacksFragment = supportFragmentManager.findFragmentByTag(tag) as StacksFragment
                        stacksFragment.getFragments()!!
                                .mapNotNull { it.getWebView() }
                                .forEach {
                                    try {
                                        it.clearCache(true)
                                    } catch (exc: Exception) {
                                        Log.e("Exception", exc.message)
                                    }
                                }
                    }
                }
            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        //}
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode)
        {
            *//*case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
            {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //TODO: Implement this callback to the StacksFragment and StackViewAdapter for tablet asset upload
                    //uploadImageStuff(uploadFileIntent);
                }
                else
                {
                    Toast.makeText(this,"You can't upload a picture without this permission.",Toast.LENGTH_LONG).show();
                }
            }*//*
        }
    }*/

    override fun onNewIntent(intent : Intent){
        setIntent(intent)
        val message = intent.getStringExtra("message")
        val stack_type = intent.getStringExtra("stack_type")

        if(stack_type != null) {
            updateNotificationBadgeCount()
            launchMobileNotifications()
        }
    }


    private val notificationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            // Extract data included in the Intent
            val message = intent.getStringExtra("message")
            //longSnackbar(mainContainerView!!, message)

            updateNotificationBadgeCount()

            //do other stuff here
        }
    }

    companion object {
        val JSON = MediaType.parse("application/json; charset=utf-8")
        private val notificationsProxy = "/api/notificationproxy"

        fun deleteDir(dir: File?): Boolean {
            if (dir != null && dir.isDirectory) {
                val children = dir.list()
                for (i in children.indices) {
                    val success = deleteDir(File(dir, children[i]))
                    if (!success) {
                        return false
                    }
                }
                return dir.delete()
            } else return if (dir != null && dir.isFile) {
                dir.delete()
            } else {
                false
            }
        }
    }
}
