package com.expion.expion.ApplicationComponents.ViewPortComponents

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.webkit.WebView
import com.expion.expion.Services.WebViewPager


/**
 * Created by arowe on 1/20/17.
 */

class HorizontalScrollWebView(context: Context, attrs: AttributeSet) : WebView(context, attrs) {

    private val deltaXThreshhold = 25
    private val perimeterThreshhold = .15
    private var programmaticScroll = false
    var position = -1



    override fun overScrollBy(deltaX: Int, deltaY: Int, scrollX: Int, scrollY: Int, scrollRangeX: Int, scrollRangeY: Int, maxOverScrollX: Int, maxOverScrollY: Int, isTouchEvent: Boolean): Boolean {
        if(parent.parent is WebViewPager) {
            val viewPager: WebViewPager = (parent.parent as WebViewPager)

            if(viewPager.scrollState == ViewPager.SCROLL_STATE_IDLE && !programmaticScroll) {
                if(deltaX > deltaXThreshhold) {
                    programmaticScroll = true
                    if(checkAdapterLimits(1, viewPager.currentItem)) //right
                        (parent.parent as ViewPager).setCurrentItem(viewPager.currentItem + 1,true)
                }
                else if(deltaX < -deltaXThreshhold) {
                    programmaticScroll = true
                    if(checkAdapterLimits(-1, viewPager.currentItem)) //left
                        (parent.parent as ViewPager).setCurrentItem(viewPager.currentItem - 1,true)
                }
                else {
                    programmaticScroll = false
                }
            }
        }
        return false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if(parent.parent is WebViewPager) {
            parent.requestDisallowInterceptTouchEvent(true)

            programmaticScroll = event.action != MotionEvent.ACTION_DOWN

            if(event.action == MotionEvent.ACTION_DOWN) {
                val x1 = event.x
                if (x1 >= width - width * perimeterThreshhold || x1 <= width * perimeterThreshhold) {
                    (parent.parent as WebViewPager).setDurationScroll(500)
                }
                else {
                    (parent.parent as WebViewPager).setDurationScroll(400)
                }
            }
            else {
                (parent.parent as WebViewPager).setDurationScroll(400)
            }
        }

        return super.onInterceptTouchEvent(event)
    }

    private fun checkAdapterLimits(direction: Int, position: Int) : Boolean {
        return if(direction < 0) //left
            position >= 1
        else //right
            position < (parent.parent as ViewPager).adapter!!.count - 1

    }
}
