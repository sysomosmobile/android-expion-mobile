package com.expion.expion.ApplicationComponents.ViewPortComponents

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ListView
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.Location
import com.expion.expion.R
import com.expion.expion.Services.LocationsArrayAdapter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.locations_dialog.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Request
import okhttp3.Response
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException
import java.util.*

/**
 * Created by arowe on 4/18/2016.
 */
class LocationsPickerFragment : DialogFragment(), View.OnClickListener {

    private var locationsAdapter: LocationsArrayAdapter? = null
    private var locations = ArrayList<Location>()

    private var urlPath: String? = null
    private var company_id: String? = null
    private var source: String? = null
    private var guid: String? = null

    private var selectedLocationIds: ArrayList<String>? = ArrayList()
    private var selectedLocationNames: ArrayList<String>? = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.locations_dialog, container)

        isCancelable = true

        urlPath = arguments?.getString("baseUrl")
        company_id = arguments?.getString("company_id")
        source = arguments?.getString("source")
        guid = arguments?.getString("guid")
        selectedLocationIds = arguments?.getStringArrayList("selected_locs")

        getLocationsFromServer()

        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(true)


        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btn_listdone!!.setOnClickListener(this)
        btn_selectall!!.setOnClickListener(this)
        btn_deselect!!.setOnClickListener(this)
        locations_list!!.choiceMode = ListView.CHOICE_MODE_MULTIPLE
    }


    private fun getLocationsFromServer() {
        val connectUrl = "$urlPath/api/location/$source/$company_id?selectAll=true"

        val request = Request.Builder()
                .url(connectUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        longSnackbar(locations_list!!, "Error Getting Locations: " + response.message())
                    } else {
                        try {
                            val responseString = responseBody.string()
                            val jsonArray = JSONArray(responseString)
                            async(UI) {
                                parseLocationsJsonArray(jsonArray)
                            }
                        } catch (jsonExc: JSONException) {
                            jsonExc.printStackTrace()
                        }

                    }
                }
            }
        })
    }

    private fun parseLocationsJsonArray(jsonArray: JSONArray) {
        locations = Gson().fromJson(jsonArray.toString(), object : TypeToken<ArrayList<Location>>() {

        }.type)
        locationsAdapter = LocationsArrayAdapter(context, locations)

        locations_list!!.adapter = locationsAdapter
        setSelectedLocations()
    }

    private fun setSelectedLocations() {
        for (i in locations.indices) {
            locations_list!!.setItemChecked(i, selectedLocationIds!!.contains(locations[i].id))
        }
    }
    override fun onClick(view: View) {
        if (view === btn_listdone) {
            selectedLocationIds!!.clear()
            for (i in locations.indices)
            {
                if (locations_list!!.isItemChecked(i)) {
                    val location = locationsAdapter!!.getItem(i)
                    selectedLocationIds!!.add(location!!.id)
                    selectedLocationNames!!.add(location.name!!)
                    location.setSelected("1")
                } else {
                    val location = locationsAdapter!!.getItem(i)
                    selectedLocationIds!!.remove(location!!.id)
                    selectedLocationNames!!.remove(location.name)
                    location.setSelected("0")
                }
            }
            finishDialog()
        } else if (view === btn_selectall) {
            for (i in locations.indices) {
                locations_list!!.setItemChecked(i, true)
            }
        } else if (view === btn_deselect) {
            for (i in locations.indices) {
                locations_list!!.setItemChecked(i, false)
            }
        }
    }

    private fun finishDialog() {
        val data = Intent()
        data.putExtra("guid", guid)
        data.putStringArrayListExtra("selectedLocationIds", selectedLocationIds)

        val locationsText = if (selectedLocationNames!!.size == 1) selectedLocationNames!![0] else Integer.toString(selectedLocationNames!!.size) + " Locations Selected"
        data.putExtra("locationsText", locationsText)

        targetFragment?.onActivityResult(targetRequestCode, Activity.RESULT_OK, data)
        dismiss()
    }
}
