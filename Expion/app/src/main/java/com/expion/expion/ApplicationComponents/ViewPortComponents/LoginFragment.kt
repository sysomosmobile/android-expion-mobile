package com.expion.expion.ApplicationComponents.ViewPortComponents

/**
 * Created by arowe on 10/01/15.
 */

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.*
import android.view.View.OnClickListener
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.OAuthInformation
import com.expion.expion.R
import com.expion.expion.Services.MyCookieCache
import com.expion.expion.Utilities.LoggingUtils
import kotlinx.android.synthetic.main.login_main.*
import kotlinx.android.synthetic.main.login_main.view.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.run
import okhttp3.*
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*
import java.util.concurrent.Executor

class LoginFragment : DialogFragment(), OnClickListener {
    private var expionPrefSettings: SharedPreferences? = null
    private var progressBar: ProgressDialog? = null

    private var oAuthInformation: OAuthInformation? = null

    var company_id: String? = null

    var isLoggedIn: Boolean = false
    private var loginUrl: String? = null

    private var debugMode = false
    private var clickCounter = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootLoginView = inflater.inflate(R.layout.login_main, container)

        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        progressBar = ProgressDialog(context)
        progressBar!!.window!!.setDimAmount(.9f)

        expionPrefSettings = activity?.getSharedPreferences(getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)

        setUpOkHttpOauthInfo()

        return rootLoginView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        username!!.inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        btn_save_password!!.setOnClickListener(this)
        btn_login!!.setOnClickListener(this)
        loginlogoicon!!.setOnClickListener(this)

        view.btn_login!!.isClickable = true
        setupSpinners()
        getPrefs()

    }

    private fun setupSpinners() {

        val debugOptions = ArrayList<String>()
        debugOptions.add(getString(R.string.run))
        debugOptions.add(getString(R.string.debug))
        val debugAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, debugOptions)

        view?.debug_spinner!!.adapter = debugAdapter

        view?.debug_spinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                debugMode = position == 1
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    private fun getPrefs() {

        isLoggedIn = expionPrefSettings!!.getBoolean(resources!!.getString(R.string.is_logged_in), false)

        val storedUsername = expionPrefSettings!!.getString(getString(R.string.UserName), "")
        val storedPassword = expionPrefSettings!!.getString(getString(R.string.password), "")
        val storedBranchCode = expionPrefSettings!!.getString(getString(R.string.branch_code), "")
        val storedServerCode = expionPrefSettings!!.getString(getString(R.string.dev_server_code), "")

        if (storedUsername.isNotEmpty()) {
            view?.username!!.setText(storedUsername)
        }
        if (storedPassword.isNotEmpty()) {
            view?.password!!.setText(storedPassword)
        }
        if(storedBranchCode.isNotEmpty()) {
            view?.dev_box!!.visibility = View.VISIBLE
            view?.debug_spinner!!.visibility = View.VISIBLE
            view?.branch!!.setText(storedBranchCode)
        }
        if(storedServerCode.isNotEmpty()) {
            view?.dev_box!!.visibility = View.VISIBLE
            view?.debug_spinner!!.visibility = View.VISIBLE
            view?.server!!.setText(storedServerCode)
        }
    }


    private fun showDebugToggle() {
        if (view?.debug_spinner!!.visibility == View.VISIBLE) {
            view?.debug_spinner!!.visibility = View.GONE
            //debugSpinnerVisible = false
        } else if (view?.debug_spinner!!.visibility == View.GONE) {
            view?.debug_spinner!!.visibility = View.VISIBLE
            //debugSpinnerVisible = true
        }

        if (view?.dev_box!!.visibility == View.GONE) {
            view?.dev_box!!.visibility = View.VISIBLE
        } else if (view?.dev_box!!.visibility == View.VISIBLE) {
            //branch!!.setText("")
            debugMode = false
            view?.dev_box!!.visibility = View.GONE
        }
    }

    private fun setUpOkHttpOauthInfo() {

        oAuthInformation = OAuthInformation()

        val interceptor = { chain:Interceptor.Chain ->
            val response = chain.proceed(chain.request())

            val locationHeader = response.header("Location")
            if (locationHeader != null)
            {
                parseLocationHeader(locationHeader.replace(" ".toRegex(), "%20"))
            }

            response
        }

        try {
            (activity as ContainerActivity).client = (activity as ContainerActivity).client.newBuilder()
                    .addNetworkInterceptor(interceptor)
                    .build()
        }
        catch(exc: UninitializedPropertyAccessException) {
            (activity as ContainerActivity).setUpOkHttpClient()
            (activity as ContainerActivity).client = (activity as ContainerActivity).client.newBuilder()
                    .addNetworkInterceptor(interceptor)
                    .build()
        }
    }

    private fun parseLocationHeader(locationHeader: String) {
        val uri = Uri.parse(locationHeader)
        val fragment = uri.fragment
        if (fragment != null && fragment.contains("access_token")) {
            val fragmentParams = fragment.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (fragmentParam in fragmentParams) {
                if (fragmentParam.contains("access_token")) {
                    val accessToken = fragmentParam.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                    oAuthInformation!!.accessToken = accessToken
                }
                if (fragmentParam.contains("token_secret")) {
                    val accessSecret = fragmentParam.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                    oAuthInformation!!.accessSecret = accessSecret
                }
            }
        } else {
            for (paramName in uri.queryParameterNames) {
                if (paramName == "authorization_error") {
                    oAuthInformation!!.authorizationError = paramName
                } else if (paramName == "locked") {
                    oAuthInformation!!.errorReason = getString(R.string.locked_out_user)
                } else if (paramName == "authentication_error") {
                    oAuthInformation!!.authenticationError = "Authentication Error"
                } else if (paramName == "company_id")
                    company_id = uri.getQueryParameter(paramName)

            }
        }
    }

    override fun onClick(clickedView: View) {
        if (clickedView === view?.btn_login!!) {
            view?.btn_login!!.isClickable = false
            oAuthInformation = OAuthInformation()
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(username!!.windowToken, 0)
            if (view?.username!!.text.isNotEmpty() && view?.password!!.text.isNotEmpty()) {
                var oauthUrl = "https://"
                if (view?.branch!!.text.isEmpty() && view?.server!!.text.isEmpty()) {
                    loginUrl = "${oauthUrl}menu.sysomos.com/"
                    oauthUrl = "${oauthUrl}oauth.sysomos.com/oauth/login.do"
                } else {
                    loginUrl = "${oauthUrl}${view?.branch!!.text}-menu.sysomos.com/"
                    oauthUrl = "${oauthUrl}${view?.branch!!.text}-oauth.sysomos.com/oauth/login.do"
                }

                storePrefs(view?.username!!.text.toString(), view?.password!!.text.toString(), view?.branch!!.text.toString(), view?.server!!.text.toString(), loginUrl.toString(), company_id)

                postLoginOkHttp(oauthUrl)

                progressBar = ProgressDialog.show(activity, getString(R.string.app_name), getString(R.string.user_auth))
                progressBar!!.setCancelable(true)
            } else {
                if (view?.username!!.text.isEmpty()) {
                    longSnackbar(dev_box, getString(R.string.username_exc))
                }
                if (view?.password!!.text.isEmpty()) {
                    longSnackbar(dev_box, getString(R.string.password_exc))
                }
                view?.btn_login!!.isClickable = true
            }
        } else if (clickedView === loginlogoicon) {
            if (++clickCounter == 1) {
                object : CountDownTimer(500, 500) {

                    override fun onTick(millisUntilFinished: Long) {}

                    override fun onFinish() {
                        clickCounter = 0
                    }
                }.start()
            } else if (clickCounter >= 3)
                showDebugToggle()
        }
    }

    private fun postLoginOkHttp(connectUrl: String) {
        if (debugMode)
            LoggingUtils.logTimestamp(activity as AppCompatActivity?, "PostingLogin", true)

        val enteredPassword = view?.password!!.text.toString()

        val formBody: FormBody = FormBody.Builder()
                .add("j_username", view?.username!!.text.toString())
                .add("j_password", enteredPassword)
                .add("login", "Submit")
                .build()

        val request = Request.Builder()
                .url(connectUrl)
                .post(formBody)
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

                if (progressBar!!.isShowing)
                    progressBar!!.dismiss()
                e.printStackTrace()
                longSnackbar(view?.login_scroll!!, "${getString(R.string.login_failed)}${e.message}")
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->

                    if (!response.isSuccessful) {

                        if (progressBar!!.isShowing)
                            progressBar!!.dismiss()
                        longSnackbar(view?.login_scroll!!, "${getString(R.string.login_failed)} Incorrect username or password. Please try again.")
                    } else {
                        if (oAuthInformation!!.authenticationError != null && oAuthInformation!!.authenticationError!!.isNotEmpty()) {

                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                            longSnackbar(view?.login_scroll!!, "Login Failed: Incorrect username or password. Please try again.")
                        } else if (oAuthInformation!!.authorizationError == null || oAuthInformation!!.authorizationError!!.isEmpty()) {
                            val accessTokenString = oAuthInformation!!.accessToken
                            if (accessTokenString != null && accessTokenString.isNotEmpty()) {
                                setOkHttpCookies()
                                val oauthUrl = "https://"
                                var menuUrl = "${oauthUrl}menu.sysomos.com/api/ml/apiurl?token=$accessTokenString"

                                if (view?.branch!!.text.isNotEmpty()) {
                                    menuUrl = "$oauthUrl${branch!!.text}-menu.sysomos.com/api/ml/apiurl?token=$accessTokenString"
                                }
                                getOkHttpMLUserJson(menuUrl)
                            } else {
                                if (progressBar!!.isShowing)
                                    progressBar!!.dismiss()
                                longSnackbar(view?.login_scroll!!, "Login Failed: Incorrect username or password. Please try again.")
                            }
                        } else {
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                            val snackbar = Snackbar.make(view?.login_scroll!!, "Login Failed: " + oAuthInformation!!.errorReason, Snackbar.LENGTH_LONG)
                            val snackbarView = snackbar.view
                            val textView = snackbarView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
                            textView.maxLines = 5  // show multiple line
                            snackbar.show()
                        }
                    }
                }
            }
        })
        view?.btn_login!!.isClickable = true
    }

    private fun setOkHttpCookies() {
        var accessTokenCookieString = "{\"access_token\":\"${oAuthInformation!!.accessToken}\"}"

        val accessTokenString = oAuthInformation!!.accessToken

        try {
            val `object` = JSONObject()
            `object`.put("access_token", accessTokenString)
            accessTokenCookieString = `object`.toString()
        } catch (exc: JSONException) {
            exc.printStackTrace()
        }

        val accessTokenCookie = MyCookieCache.createNonPersistentCookie("ls.access", accessTokenCookieString, "sysomos.com")
        MyCookieCache.cookies!!.add(accessTokenCookie)

        accessTokenCookieString = "ls.access=$accessTokenCookieString"

        /*(activity as ContainerActivity).client = (activity as ContainerActivity).client.newBuilder()
                .addNetworkInterceptor(AddLSAccessCookieInterceptor(accessTokenCookieString))
                .build()*/

        val prefEditor = expionPrefSettings!!.edit()
        prefEditor.putString("access_token", oAuthInformation!!.accessToken)
        prefEditor.putString("ls_access_cookie",accessTokenCookieString)
        prefEditor.apply()
    }

    private fun getOkHttpMLUserJson(mlJsonUrl: String) {
        if (debugMode)
            LoggingUtils.logTimestamp(activity as AppCompatActivity?, "Getting User ML", true)

        val request = Request.Builder()
                .url(mlJsonUrl)
                .get()
                .build()

        Log.d("CookieRequest","Requesting: $mlJsonUrl")

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

                if (progressBar!!.isShowing)
                    progressBar!!.dismiss()
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful)
                    {
                        if (progressBar!!.isShowing)
                            progressBar!!.dismiss()
                        longSnackbar(view?.login_scroll!!, "Error Logging In: " + response.message())
                    }
                    else {
                        val responseHeaders = response.headers("Set-Cookie")
                        for(responseHeader in responseHeaders) {
                            Log.d("CookieInResponse",responseHeader)
                        }
                        var responseMessage = ""
                        try {

                            val jsonObject = JSONObject(responseBody.string())

                            responseMessage = jsonObject.optString("ResponseMessage")
                            val responseDataJson = jsonObject.getJSONObject("ResponseData")

                            val mobileIsValid = responseDataJson.optString("MobileIsValid")
                            if (mobileIsValid != null && mobileIsValid == "false") {

                                if (progressBar!!.isShowing)
                                    progressBar!!.dismiss()
                                updateApp()
                            } else {
                                val companyId = responseDataJson.getInt("company_id")

                                if (responseMessage != null && responseMessage.isNotEmpty())
                                {
                                    longSnackbar(view?.login_scroll!!, responseMessage)

                                    var overrideCookies = false
                                    var ssoBaseUrl = responseDataJson.getString("SSOBaseUrl")
                                    if (view?.server!!.text.isNotEmpty()) {
                                        ssoBaseUrl = "https://${server!!.text}-social.sysomos.com"
                                        setSubdomainCookies("${server!!.text}-social.sysomos.com")
                                        overrideCookies = true
                                    }
                                    setUserData(companyId, ssoBaseUrl, false, overrideCookies)
                                } else {

                                    var overrideCookies = false
                                    var ssoBaseUrl = "https:${responseDataJson.getString("SSOBaseUrl")}"
                                    if (view?.server!!.text.isNotEmpty()) {
                                        ssoBaseUrl = "https://${server!!.text}-social.sysomos.com"
                                        setSubdomainCookies("${server!!.text}-social.sysomos.com")
                                        overrideCookies = true
                                    }
                                    setUserData(companyId, ssoBaseUrl, true, overrideCookies)
                                }

                            }
                        } catch (jsonExc: JSONException) {
                            if (progressBar!!.isShowing)
                                progressBar!!.dismiss()
                            longSnackbar(view?.login_scroll!!, responseMessage)
                        }

                    }
                }
            }
        })
    }

    private fun setSubdomainCookies(cookieDomain: String) {
        val addingCookies = mutableListOf<Cookie>()
        val cookiesIterator = MyCookieCache.cookies!!.iterator()
        while(cookiesIterator.hasNext()) {
            val cookie = cookiesIterator.next()
            if(cookie.name().toUpperCase().contains("ASPXAUTH") || cookie.name().toLowerCase().contains("expion.co")
                    || cookie.name().toLowerCase().contains("ls.access")) {
                val subDomainCookie = MyCookieCache.createNonPersistentCookie(cookie.name(), cookie.value(), cookieDomain)
                val cookieSplit = subDomainCookie.value().split("=")
                if(cookieSplit.isNotEmpty() && cookieSplit[0].isNotEmpty())
                    addingCookies.add(subDomainCookie)
            }
        }
        for(cookie in addingCookies) {
            if(MyCookieCache.cookies != null) {
                MyCookieCache.cookies!!.add(cookie)
            }
        }
    }

    private fun setUserData(companyId: Int, ssoBaseUrl: String, launchML: Boolean, overrideCookies: Boolean) {
        if (debugMode)
            LoggingUtils.logTimestamp(activity as AppCompatActivity?, "Setting User Data", true)

        this.company_id = companyId.toString()
        storePrefs(view?.username!!.text.toString(), view?.password!!.text.toString(), view?.branch!!.text.toString(), view?.server!!.text.toString(), ssoBaseUrl, company_id)

        if (progressBar!!.isShowing)
            progressBar!!.dismiss()

        if (launchML)
            launchML(overrideCookies)
    }

    private fun launchML(overrideCookies: Boolean) {
        if (debugMode)
            LoggingUtils.logTimestamp(activity as AppCompatActivity?, "Launching ML", true)
        (activity as ContainerActivity).loginCompleted(true, debugMode, overrideCookies)

        dismiss()
    }

    private fun storePrefs(username: String, password: String, branchCode: String, serverCode: String, baseUrl: String, company_id: String?) {
        val prefEditor = expionPrefSettings!!.edit()
        prefEditor.putString(getString(R.string.UserName), username)
        prefEditor.putString(getString(R.string.company_id), company_id)
        prefEditor.putString(resources!!.getString(R.string.branch_code), branchCode)
        prefEditor.putString(getString(R.string.base_url), baseUrl)
        prefEditor.putString(getString(R.string.dev_server_code), serverCode)

        if (view?.btn_save_password!!.isChecked)
            prefEditor.putString(resources!!.getString(R.string.password), password)
        else
            prefEditor.putString(resources!!.getString(R.string.password), "")

        prefEditor.apply()
    }

    fun updateApp() {

        val MainThread = object : Executor {
            private val mHandler = Handler(Looper.getMainLooper())

            override fun execute(command: Runnable) {
                mHandler.post(command)
            }
        }.asCoroutineDispatcher()

        async(CommonPool) {
            run(MainThread) {
                AlertDialog.Builder(activity)
                        .setMessage("An update is required in order to use this application.")
                        .setCancelable(false)
                        .setPositiveButton("Update") { _: DialogInterface, _: Int ->
                            val appPackageName = activity?.packageName // getPackageName() from Context or Activity object
                            try {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                            } catch (anfe: ActivityNotFoundException) {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                            }
                        }
                        .show()
            }
        }
    }

    companion object {

        fun newInstance(num: Int): LoginFragment {
            val frag = LoginFragment()

            // Supply num input as an argument.
            val args = Bundle()
            args.putInt("num", num)
            frag.arguments = args

            return frag
        }
    }
}

