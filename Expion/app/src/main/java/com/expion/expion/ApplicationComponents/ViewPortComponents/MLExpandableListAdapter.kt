package com.expion.expion.ApplicationComponents.ViewPortComponents

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.Models.MLItem
import com.expion.expion.R
import java.util.*

/**
 * Created by mbagwell on 10/08/15.
 */
class MLExpandableListAdapter(private val containerActivity: ContainerActivity, mlData: LinkedHashMap<MLItem, ArrayList<MLItem>>, showText: Boolean) : BaseExpandableListAdapter() {
    var mlData = LinkedHashMap<MLItem, ArrayList<MLItem>>()
    private val mlDataKeys = ArrayList<MLItem>()
    var isTextShowing = true
        private set

    init {
        this.mlData = mlData
        this.isTextShowing = showText

        populateMlDataKeys()
    }

    private fun populateMlDataKeys() {
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            mlDataKeys = mlData.entrySet().stream()
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
        }
        else
        {*/
        for ((key) in mlData) {
            mlDataKeys.add(key)
        }
        //}
    }

    private class ViewHolder {
        internal var text: TextView? = null
    }

    override fun getGroup(groupPosition: Int): MLItem? {

        return mlDataKeys[groupPosition]
    }

    override fun getGroupCount(): Int {
        return mlData.size
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        var retView = convertView

        val mlItem = getGroup(groupPosition)

        val wsName = mlItem?.name ?: ""
        val holder: ViewHolder

        if (retView == null) {
            holder = ViewHolder()
            val infalInflater = containerActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            retView = infalInflater.inflate(R.layout.frag_ndrawer_group_item, parent, false)
        } else {
            holder = retView.tag as ViewHolder
            retView = convertView
        }

        holder.text = retView!!.findViewById(R.id.nav_drawer_group_text)

        if (isTextShowing)
            holder.text!!.text = wsName
        else
            holder.text!!.text = ""

        holder.text!!.isSelected = isExpanded

        var drawableId = if (isExpanded) R.drawable.open_folder_blue else R.drawable.closed_folder_grey

        if (mlItem != null) {
            when (mlItem.name) {
                "Home Workspace" //Home Workspace
                -> drawableId = if (isExpanded) R.drawable.home_blue else R.drawable.home_grey
                "Insights" //Insights
                -> drawableId = if (isExpanded) R.drawable.bell_blue else R.drawable.bell_black
                "Photo Approval" //Photo Approval
                -> drawableId = if (isExpanded) R.drawable.photo_approval_blue else R.drawable.photo_approval_grey
                "Photos" //Photo Uploader
                -> drawableId = if (isExpanded) R.drawable.camera_icon_blue else R.drawable.camera_icon_grey
                "Create Content" //New Post
                    , "" //Create Content
                -> drawableId = if (isExpanded) R.drawable.quick_post_blue else R.drawable.quick_post_grey
            }
        }

        val leftDrawable = ContextCompat.getDrawable(containerActivity, drawableId)
        val bitmap = (leftDrawable as BitmapDrawable).bitmap
        val pixelSize = if (containerActivity.tabletMode) 100 else 275
        val scaledBitmap = BitmapDrawable(containerActivity.resources, Bitmap.createScaledBitmap(bitmap, convertPixelsToDp(pixelSize), convertPixelsToDp(pixelSize), true))
        holder.text!!.setCompoundDrawablesWithIntrinsicBounds(scaledBitmap, null, null, null)

        retView.tag = holder

        return retView
    }

    override fun getChild(groupPosition: Int, childPosition: Int): MLItem? {

        val mlGroupItem = getGroup(groupPosition)
        var mlChildItem: MLItem? = null

        val mlChildren = mlData[mlGroupItem]

        if (mlChildren != null && childPosition < mlChildren.size) {
            mlChildItem = mlChildren[childPosition]
        }

        return mlChildItem
    }


    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var retView = convertView

        val holder: ViewHolder
        val mlItem = getChild(groupPosition, childPosition)
        val homeItemName = mlItem?.name ?: ""
        val inflater = containerActivity.layoutInflater

        if (retView == null) {
            holder = ViewHolder()
            retView = inflater.inflate(R.layout.frag_ndrawer_exlist_child_item, parent, false)
        } else {
            holder = retView.tag as ViewHolder
            retView = convertView
        }

        holder.text = retView!!.findViewById(R.id.nav_drawer_child_text)

        if (isTextShowing)
            holder.text!!.text = homeItemName
        else
            holder.text!!.text = ""


        retView.tag = holder
        return retView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        val mlGroupItem = getGroup(groupPosition)
        val mlChildren = mlData[mlGroupItem]
        var childrenCount = 0
        if (mlChildren != null)
            childrenCount = mlChildren.size
        return childrenCount
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    fun setShowText(showText: Boolean) {
        this.isTextShowing = showText
        this.notifyDataSetChanged()
    }

    private fun convertPixelsToDp(px: Int): Int {
        val resources = containerActivity.resources
        val metrics = resources.displayMetrics
        return (px / (metrics.densityDpi / 160f)).toInt()
    }
}
