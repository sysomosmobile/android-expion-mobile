package com.expion.expion.ApplicationComponents.ViewPortComponents

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.ViewGroup

/**
 * Created by mbagwell on 09/29/15.
 */
internal class MLPagedScrollAdapter(fm: FragmentManager, private val fragments: MutableList<WebViewFragment>) : FragmentStatePagerAdapter(fm) {
    private var deleting = false

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): WebViewFragment {
        return fragments[position]
    }

    override fun getItemPosition(`object`: Any): Int {

        if (deleting) {
            return PagerAdapter.POSITION_NONE
        }

        val fragment = `object` as WebViewFragment?
        return if (fragments.contains(fragment)) {
            fragment!!.position
        } else {
            PagerAdapter.POSITION_NONE
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        try {
            super.destroyItem(container, position, `object`)
        } catch (exc: IndexOutOfBoundsException) {
            exc.printStackTrace()
        }

    }

    fun addItem(fragment: WebViewFragment) {
        fragments.add(fragment)
        notifyDataSetChanged()
    }

    fun addItemAtPosition(position: Int, fragment: WebViewFragment) {
        fragments.add(position, fragment)
    }

    fun removeItem(position: Int) {
        fragments.removeAt(position)

        deleting = true
        notifyDataSetChanged()
        deleting = false
    }
}

