package com.expion.expion.ApplicationComponents.ViewPortComponents

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.expion.expion.R

/**
 * Created by arowe on 5/26/17.
 */

internal class NotificationsAdapter// Provide a suitable constructor (depends on the kind of dataset)
(private val dataset: Array<String>, private val itemLayout: Int, private val listener: NotificationsInbox.OnItemClickedListener?) : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {
    private var mRecyclerView: RecyclerView? = null

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    internal class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // each data item is just a string in this case
        var mTextView: TextView

        init {
            mTextView = view.findViewById<View>(R.id.notification_text_view) as TextView
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationsAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context).inflate(itemLayout, parent, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.text = dataset[position]

        holder.mTextView.setOnClickListener { view: View ->
            val pos = mRecyclerView!!.getChildAdapterPosition(view)
            listener?.itemClicked(view, dataset[pos])
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return dataset.size
    }

    /*override fun onAttachedToRecyclerView(recyclerView: RecyclerView?) {
        super.onAttachedToRecyclerView(recyclerView)

        mRecyclerView = recyclerView
    }*/
}
