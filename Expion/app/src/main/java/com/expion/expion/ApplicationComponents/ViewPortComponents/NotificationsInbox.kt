package com.expion.expion.ApplicationComponents.ViewPortComponents


import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.expion.expion.R
import com.expion.expion.Services.OnTaskCompleted
import org.jetbrains.anko.design.longSnackbar

class NotificationsInbox : DialogFragment() {
    //private static final String WEBVIEW_URL = "webViewUrl";

    //private String webViewUrl;

    private var notificationsRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*if (getArguments() != null) {
            webViewUrl = getArguments().getString(WEBVIEW_URL);
        }*/
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_notifications_inbox, container, false)
        notificationsRecyclerView = rootView.findViewById<View>(R.id.notifications_recycler_view) as RecyclerView
        notificationsRecyclerView!!.setHasFixedSize(true)

        // use a linear layout manager
        mLayoutManager = LinearLayoutManager(activity)
        notificationsRecyclerView!!.layoutManager = mLayoutManager

        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        //getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        /*Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setTitle("Notifications");

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);*/

        val actionBar = (activity as AppCompatActivity).supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel)
            actionBar.setDisplayShowTitleEnabled(true)
            actionBar.setDisplayUseLogoEnabled(false)
            actionBar.setIcon(null)
            actionBar.title = "Notifications"
        }
        //setHasOptionsMenu(true);

        populateRecyclerView()

        return rootView
    }

    private fun populateRecyclerView() {

        mAdapter = NotificationsAdapter(arrayOf("1", "2", "3"), R.layout.notification_item, object : OnItemClickedListener {
            override fun itemClicked(view: View, yadda: String) {
                onItemClicked(view, yadda)
            }
        })
        notificationsRecyclerView!!.adapter = mAdapter
    }

    private fun onItemClicked(view: View, yadda: String) {
        //TODO: Implement notification action handling on clicks here
        longSnackbar(view, yadda)
    }

    /** The system calls this only when creating the layout in a dialog.  */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface?) {
        setHasOptionsMenu(false)
        if (activity is OnTaskCompleted)
            (activity as OnTaskCompleted).dialogClosed()
    }

    /*override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        activity.menuInflater.inflate(R.menu.notifications_menu, menu)
    }*/

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.itemId

        /*if (id == R.id.action_mark_all_as_read) {
            //TODO: Mark All As Read
            return true
        } else if (id == android.R.id.home) {
            dismiss()
            return true
        }*/

        return super.onOptionsItemSelected(item)
    }


    interface OnItemClickedListener {
        //TODO: Add Notification Model in here once it's created
        fun itemClicked(view: View, blah: String)
    }

    companion object {
        fun newInstance(webViewUrl: String): NotificationsInbox {
/*Bundle args = new Bundle();
        args.putString(WEBVIEW_URL, webViewUrl);
        fragment.setArguments(args);*/
            return NotificationsInbox()
        }
    }

}
