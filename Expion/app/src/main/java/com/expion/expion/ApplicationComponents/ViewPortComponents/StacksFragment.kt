package com.expion.expion.ApplicationComponents.ViewPortComponents


import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.support.v17.leanback.widget.HorizontalGridView
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.R
import com.expion.expion.Services.OnTaskCompleted
import com.expion.expion.Services.StackViewAdapter
import com.expion.expion.Services.WebViewPager
import com.expion.expion.Utilities.StackLayoutUtils
import com.expion.expion.Utilities.StackUtils
import kotlinx.android.synthetic.main.container_activity.*
import kotlinx.android.synthetic.main.fragment_stacks_layout.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import okhttp3.*
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class StacksFragment : Fragment() {
    val stackClass = DYNAMIC_CLASSES_DIRECTORY + "mlstackClass"
    private var refreshType: Int = 0

    private val KEY_POSITION = "position"
    private val STACK_URL = "loadUrl"
    private val WV_INJECTIONSTR = "wvInjectionStr"
    private val COLUMN_COUNT = "columnCount"
    private val WV_URL = "webViewUrl"
    private val INITIAL_COLUMN_COUNT = 1

    private var layout_id: Int = 0
    private var mlItemId: Int = 0
    private var stackPosition: Int = 0

    private var loadExpion = false
    internal var tabletMode = false
    private var resumingFromStacksFragment = false

    private lateinit var myUrl: String
    private lateinit var loadUrl: String
    private lateinit var cookieValue: String
    internal lateinit var company_id: String
    private var delayedInjection: Boolean = false


    private var injectionStrings = ArrayList<String>()
    private var columnCounts = ArrayList<Int>()
    private var stackIdArray = ArrayList<String>()

    private lateinit var fragments: MutableList<WebViewFragment>

    private lateinit var stackViewAdapter: StackViewAdapter
    private lateinit var viewPagerAdapter: MLPagedScrollAdapter

    private lateinit var fragmentViewPager: WebViewPager

    private var debugMode: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //adjustFontScale(context, resources.configuration)
        fragments = ArrayList()

        val extras = arguments
        if (extras != null) {
            injectionStrings = extras.getStringArrayList("injectionStrings")
            stackIdArray = extras.getStringArrayList("stackIdArray")
            columnCounts = extras.getIntegerArrayList("columnCounts")
            myUrl = extras.getString("myUrl")
            company_id = extras.getString("company_id")
            loadExpion = extras.getBoolean("loadExpion")
            tabletMode = extras.getBoolean("tabletMode")
            delayedInjection = extras.getBoolean("delayedInjection")
            resumingFromStacksFragment = extras.getBoolean("resumingFromStacksFragment")
            loadUrl = extras.getString("loadUrl")
            cookieValue = extras.getString("cookieValue")
            layout_id = extras.getInt("layout_id")
            mlItemId = extras.getInt("mlItemId")
            debugMode = extras.getBoolean("debugMode")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_stacks_layout, container, false)
    }

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if(!tabletMode) {
            viewPagerAdapter = MLPagedScrollAdapter(childFragmentManager, fragments)
            fragmentViewPager = viewPager!!
            fragmentViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageScrollStateChanged(state: Int) {
                    viewPager.scrollState = state
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
                override fun onPageSelected(position: Int) { }

            })
            fragmentViewPager.adapter = viewPagerAdapter
            fragmentViewPager.setDurationScroll(325)
        }
        else {
            gridView!!.setHasFixedSize(true)
            gridView!!.setItemViewCacheSize(3)
            gridView!!.setScrollingTouchSlop(RecyclerView.TOUCH_SLOP_PAGING)

            if (gridView!!.focusScrollStrategy != HorizontalGridView.FOCUS_SCROLL_ITEM)
                gridView!!.focusScrollStrategy = HorizontalGridView.FOCUS_SCROLL_ITEM

            stackViewAdapter = StackViewAdapter(activity as ContainerActivity, injectionStrings, myUrl, loadUrl, columnCounts, cookieValue)
            gridView!!.adapter = stackViewAdapter
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onResume() {
        super.onResume()

        if (!resumingFromStacksFragment) {
            Log.d("Stacks","TabletMode: $tabletMode")
            if (tabletMode) {
                if(!delayedInjection) {
                    toggleVisibility()
                    Log.d("Stacks","Calling Populate Adapter from OnResume")
                    populateAdapter(false, false)
                }
                else {
                    populateAdapterFirstLoad()
                }
            } else {
                fragmentViewPager.offscreenPageLimit = 1

                if(!delayedInjection) {
                    toggleVisibility()
                    Log.d("Stacks","Calling Populate ViewPager from OnResume")
                    populateViewPager(false, false)
                }
                else {
                    populateViewPagerFirstLoad()
                }
            }
        } else {
            if (activity is OnTaskCompleted)
                (activity as OnTaskCompleted).updateResumingStacksFromFragment(false)
        }
    }

    private fun toggleVisibility() {
        if (injectionStrings.size == 0) {
            fragments.clear()
            if(tabletMode) {
                Log.d("Stacks","Hiding GridView")
                gridView!!.visibility = View.GONE
            }
            else {
                fragmentViewPager.visibility = View.GONE
                viewPagerAdapter.notifyDataSetChanged()
            }
            noStacksWarningView!!.visibility = View.VISIBLE
        } else {
            if(noStacksWarningView != null) {
                noStacksWarningView!!.visibility = View.GONE
            }

            if (tabletMode) {
                Log.d("Stacks","Showing GridView")
                gridView!!.visibility = View.VISIBLE
            } else {
                fragmentViewPager.visibility = View.VISIBLE
            }
        }
    }

    fun populateAdapter(stackRefreshing: Boolean, injectFirstFragment: Boolean) {
        Log.d("Stacks","Populating Adapter")

        activity?.runOnUiThread( {
            var moreThanTwoStacks = false
            for (i in 0 until injectionStrings.size) {
                Log.d("Stacks","Injection String #$i, ${injectionStrings[i]}")
                if(i == 0) {
                    if(injectFirstFragment) {
                        Log.d("Stacks","Injecting Stack #$i, Inject First Fragment: $injectFirstFragment")
                        if(fragments.isNotEmpty()) {
                            val frag = fragments.get(i)
                            frag.injectionStr = injectionStrings.get(i)
                            //Log.d("Stacks","InjectionString #$i, ${frag.injectionStr}")
                            Log.d("Stacks", "Script Injected #$i - ${frag.initialScriptInjected}")
                            if(frag.initialScriptInjected)
                                frag.injectScript(frag.injectionStr)
                        }
                        else {
                            Log.e("Stackss","Fragments are empty, can't populate")
                        }
                    }
                    else {
                        Log.d("Stacks","Populating Stack #$i, Inject First Fragment: $injectFirstFragment")
                        fragments.add(buildFragment(i, stackRefreshing))
                        stackViewAdapter.notifyDataSetChanged()
                    }
                }
                else {
                    Log.d("Stacks","Populating Stack #$i, Inject First Fragment: $injectFirstFragment")
                    fragments.add(buildFragment(i, stackRefreshing))
                    if(i == 1)
                        stackViewAdapter.notifyDataSetChanged()
                }
                if(i == 2)
                    moreThanTwoStacks = true
            }
            if(moreThanTwoStacks) {
                stackViewAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun populateAdapterFirstLoad() {
        Log.d("Stacks","Populating Adapter on First Load")
        injectionStrings.add("")
        columnCounts.add(1)
        activity?.runOnUiThread( {
            fragments.add(buildFragment(0, false))
            stackViewAdapter.notifyDataSetChanged()
        })
    }

    fun populateViewPager(stackRefreshing: Boolean, injectFirstFragment: Boolean) {

        activity?.runOnUiThread( {
            var moreThanTwoStacks = false
            for (i in 0 until injectionStrings.size) {
                Log.d("Stacks","Injection String #$i, ${injectionStrings[i]}")
                if(i == 0) {
                    if(injectFirstFragment) {
                        Log.d("Stacks","Injecting Stack #$i, Inject First Fragment: $injectFirstFragment")
                        if(fragments.isNotEmpty()) {
                            val frag = fragments.get(i)
                            frag.injectionStr = injectionStrings.get(i)
                            //Log.d("Stacks","InjectionString #$i, ${frag.injectionStr}")
                            Log.d("Stacks", "Script Injected #$i - ${frag.initialScriptInjected}")
                            if(frag.initialScriptInjected)
                                frag.injectScript(frag.injectionStr)
                        }
                        else {
                            Log.e("Stackss","Fragments are empty, can't populate")
                        }
                    }
                    else {
                        Log.d("Stacks","Populating Stack #$i, Inject First Fragment: $injectFirstFragment")
                        fragments.add(buildFragment(i, stackRefreshing))
                        viewPagerAdapter.notifyDataSetChanged()
                    }
                }
                else {
                    Log.d("Stacks","Populating Stack #$i, Inject First Fragment: $injectFirstFragment")
                    fragments.add(buildFragment(i, stackRefreshing))
                    if(i == 1)
                        viewPagerAdapter.notifyDataSetChanged()
                }
                if(i == 2)
                    moreThanTwoStacks = true
            }
            if(moreThanTwoStacks) {
                viewPagerAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun populateViewPagerFirstLoad() {
        Log.d("Stacks","Populating View Pager on First Load")
        injectionStrings.add("")
        columnCounts.add(1)
        activity?.runOnUiThread( {
            fragments.add(buildFragment(0, false))
            viewPagerAdapter.notifyDataSetChanged()
        })
    }

    fun injectStackData(injectionStrings: ArrayList<String>, columnCounts: ArrayList<Int>, stackIdArray: ArrayList<String>) {
        Log.d("Stacks","Injecting Stack Data")

        this.injectionStrings = injectionStrings
        this.columnCounts = columnCounts
        this.stackIdArray = stackIdArray

        activity?.runOnUiThread( {
            toggleVisibility()
            delayedInjection = false
        })
    }

    private fun buildFragment(position: Int, stackRefreshing: Boolean): WebViewFragment {
        val frag = WebViewFragment()
        //Log.d("Stacks","Building Stack #$position - Injection String: ${injectionStrings[position]}")
        val args = Bundle()
        args.putString(STACK_URL, loadUrl)
        args.putString(WV_URL, myUrl)
        args.putInt(KEY_POSITION, position)
        args.putString(WV_INJECTIONSTR, if(injectionStrings.isNotEmpty()) injectionStrings[position] else "")
        args.putInt(COLUMN_COUNT, if(columnCounts.isNotEmpty()) columnCounts[position] else 0)
        args.putString("cookieValue", cookieValue)
        args.putBoolean("loadExpion", loadExpion)
        args.putBoolean("stackRefreshing", stackRefreshing)
        args.putString("company_id",company_id)
        args.putBoolean("debugMode",debugMode)
        frag.arguments = args

        return frag
    }


    fun loadStack(stackPositionLoaded: Int) {
            if (stackPositionLoaded < fragments.size) {
                    fragments[stackPositionLoaded].populateWebView()
            }
    }

    fun refreshWorkspace(stackPosition: Int, refreshType: Int) {
        this.refreshType = refreshType
        this.stackPosition = stackPosition
        injectionStrings.clear()
        stackIdArray.clear()
        val stackLayoutConnectionUrl = "$myUrl/API/stacklayout/$company_id?layout_id=$layout_id"

        getStackLayout(stackLayoutConnectionUrl)
    }

    fun deleteStack(stackPosition: Int) {
        viewPagerAdapter.removeItem(stackPosition)
        updateFragmentPositions(StackUtils.DECREMENT_STACK_POSITION, stackPosition)
        viewPagerAdapter.notifyDataSetChanged()
        stackIdArray.removeAt(stackPosition)
        injectionStrings.removeAt(stackPosition)
        columnCounts.removeAt(stackPosition)
        if (injectionStrings.size - 1 >= 1) {
            fragmentViewPager.currentItem = if (stackPosition - 1 >= 0) stackPosition - 1 else 0
            viewPagerAdapter.notifyDataSetChanged()
        } else if(injectionStrings.isEmpty())
            noStacksWarningView!!.visibility = View.VISIBLE

        launch(CommonPool) {
            updateStackOrderOnServer()
        }
    }

    fun insertStack(position: Int, stackId: Int) {
        this.stackPosition = position
        if(position > stackIdArray.size - 1) {
            stackIdArray.add(stackId.toString())
            columnCounts.add(INITIAL_COLUMN_COUNT)
        }
        else {
            stackIdArray.add(position,stackId.toString())
            columnCounts.add(position,INITIAL_COLUMN_COUNT)
        }
        injectionStrings = StackLayoutUtils.buildInjectionStrings(mlItemId, layout_id, stackIdArray)
        addToAdapter()

        launch(CommonPool) {
            updateStackOrderOnServer()
        }
    }

    private fun updateStackOrderOnServer() {

        val jsonStackArray = JSONArray()
        var counter = -1
        for(stackId in stackIdArray) {
            try {
                val jsonStackData = JSONObject()
                jsonStackData.put("stack_id", stackId)
                jsonStackData.put("sort_index", ++counter)

                jsonStackArray.put(jsonStackData)
            } catch (exception: Exception) {
                Log.e("UpdateStackOrder",exception.message)
            }
        }

        val body = RequestBody.create(ContainerActivity.JSON, jsonStackArray.toString())

        val connectUrl = "$myUrl/api/stack/stackactions/UpdateStackOrder/$layout_id/$company_id"
        val request = Request.Builder()
                .url(connectUrl)
                .post(body)
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                if(tabletMode) {
                    longSnackbar(gridView!!, "Error Updating Stack Layout: " + e.message)
                }
                else {
                    longSnackbar(viewPager!!, "Error Updating Stack Layout: " + e.message)
                }
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { _ ->
                    if (!response.isSuccessful) {
                        if(tabletMode) {
                            longSnackbar(gridView!!, "Error Updating Stack Layout: " + response.message())
                        }
                        else {
                            longSnackbar(viewPager!!, "Error Updating Stack Layout: " + response.message())
                        }
                    }
                }
            }
        })
    }

    private fun updateFragmentPositions(direction: Int, positionMarker: Int) {
        for (i in positionMarker until fragments.size) {
            val newPosition = fragments[i].position + direction
            fragments[i].position = newPosition
        }
    }


    private fun getStackLayout(connectUrl: String) {

        val request = Request.Builder()
                .cacheControl(CacheControl.Builder().noCache().build())
                .url(connectUrl)
                .get()
                .build()

        (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                response.body()!!.use { responseBody ->
                    if (!response.isSuccessful) {
                        if(tabletMode) {
                            longSnackbar(gridView!!, "Error Getting Stack Layout: " + response.message())
                        }
                        else {
                            longSnackbar(viewPager!!, "Error Getting Stack Layout: " + response.message())
                        }
                    } else {
                        try {
                            val responseString = responseBody.string()
                            val jsonStackLayoutArray = JSONArray(responseString)
                            val responseUrl = response.request().url().toString()
                            if (responseUrl.toLowerCase().contains("api/stacklayout")) {
                                parseStackLayoutJSON(jsonStackLayoutArray, stackClass)
                            }
                            else
                                longSnackbar(mainContainerView!!, "Error Getting Stack Layout: " + response.message())
                        } catch (jsonExc: JSONException) {
                            jsonExc.printStackTrace()
                        }

                    }
                }
            }
        })
    }

    private fun parseStackLayoutJSON(jsonStackLayoutArray: JSONArray?, stackClass: String) {
        try {

            if (jsonStackLayoutArray != null) {
                val layoutData = jsonStackLayoutArray.getJSONObject(0)
                val stack_detailsData = layoutData.getJSONArray("stack_details")
                val stackDetailsDataLength = stack_detailsData.length()
                var stack_detailsDict: JSONObject?
                columnCounts = ArrayList()
                for (sc in 0..stackDetailsDataLength - 1) {
                    stack_detailsDict = stack_detailsData.getJSONObject(sc)
                    if (stack_detailsDict != null) {
                        val stack_detail_id = stack_detailsDict.optInt("id")
                        if (!stack_detailsDict.isNull("id")) {
                            stackIdArray.add(Integer.toString(stack_detail_id))
                        }
                        val columnCount = stack_detailsDict.optInt("column_count")
                        if (!stack_detailsDict.isNull("column_count")) {
                            columnCounts.add(columnCount)
                        }
                    }
                }

                injectionStrings = StackLayoutUtils.buildInjectionStrings(mlItemId, layout_id, stackIdArray)
                addToAdapter()

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun addToAdapter() {
        activity?.runOnUiThread {

            try {
                for (i in fragments.indices) {
                    childFragmentManager.beginTransaction().remove(fragments[i]).commitNowAllowingStateLoss()
                }

                fragments.clear()

                viewPagerAdapter = MLPagedScrollAdapter(childFragmentManager, fragments)

                Log.d("Stacks","Calling Populate ViewPager from addToAdapter")
                populateViewPager(true, false)

                fragmentViewPager.adapter = viewPagerAdapter

                fragmentViewPager.setCurrentItem(stackPosition, true)

            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }

    }

    fun getFragments(): List<WebViewFragment>? {
        return fragments
    }

    private fun clearWebViewCache(fragment: WebViewFragment?, versionCheck: Boolean) {
        if (fragment != null) {
            val webView = fragment.getWebView()
            if (webView != null) {
                if (versionCheck) {
                    webView.clearCache(true)
                }
                webView.destroy()
            }
        }
    }

    override fun onDestroyView() {
        var counter = 0
        val fragmentTransaction = childFragmentManager.beginTransaction()
        for (fragment in fragments) {
            counter++
            if (counter <= 1) {
                clearWebViewCache(fragment, Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1)
            }
            fragmentTransaction.remove(fragment)
        }
        childFragmentManager.executePendingTransactions()
        super.onDestroyView()
    }

    companion object {
        private val DYNAMIC_CLASSES_DIRECTORY = "com.expion.expion.DynamicClasses."
        private val TAG = "Adapter"
    }
}
