package com.expion.expion.ApplicationComponents.ViewPortComponents
import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.webkit.*
import android.widget.RelativeLayout
import android.widget.Toast
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.R
import com.expion.expion.Services.OnTaskCompleted
import com.expion.expion.Services.StackLoadedListener
import com.expion.expion.Utilities.*
import com.expion.expion.Utilities.PhotoUploadingUtils.deleteAndScanFile
import com.expion.expion.Utilities.StringParsingUtilities.parseBase64Text
import kotlinx.android.synthetic.main.mlstackviewframe.*
import kotlinx.android.synthetic.main.mlstackviewframe.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Request
import okhttp3.Response
import okio.Okio
import org.jetbrains.anko.design.longSnackbar
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.net.URLEncoder
import java.util.*

class WebViewFragment : Fragment() {

    internal var rootView: RelativeLayout? = null
    private lateinit var handler: Handler
    private var debugMode: Boolean = false

    internal lateinit var loadUrl: String
    internal lateinit var company_id: String
    private lateinit var cookieValue: String
    private lateinit var instagramImgInfo: String

    var injectionStr: String? = null
    var position: Int = 0
    private var columnCount: Int = 0
    private var webViewUrl: String? = null

    internal var isLoaded = false
    internal var isShown = false
    internal var loadFinished = false

    private var stackRefreshing: Boolean = false
    internal var errorToastDisplayed = false
    private var loadExpion = true

    var initialScriptInjected = false

    /** File Uploading **/
    private var mCapturedImageURI: Uri? = null
    private var mUploadMessage: ValueCallback<Uri>? = null
    var uploadMessage: ValueCallback<Array<Uri>>? = null
    private var fileUploadPath: String? = null
    private var uploadFile: File? = null
    private var uploadFileIntent: Intent? = null
    private var acceptType = ""

    /** WebView Clients **/
    private val webViewClient = EmbeddedWebViewClient()
    private val webChromeClient = EmbeddedWebChromeClient()

    /** Native Bridge **/
    private var selectedLocationIds = ArrayList<String>()

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (view != null) {
            if (!isLoaded)
                populateWebView()
            else {
                if (!isShown) {
                    isShown = true
                }
            }
            handler.postDelayed({
                if (activity is StackLoadedListener)
                    (activity as StackLoadedListener).stackLoaded(position + 1)

            }, 500)
            if (debugMode)
                LoggingUtils.logTimestamp(activity as AppCompatActivity?, "#$position - isLoaded: $isLoaded", debugMode)

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adjustFontScale(context, resources.configuration)

        if(savedInstanceState == null) {

            position = arguments?.getInt(KEY_POSITION, 0)!!
            columnCount = arguments?.getInt(COLUMN_COUNT, 0)!!
            webViewUrl = arguments?.getString(WV_URL, "")
            stackRefreshing = arguments?.getBoolean("stackRefreshing", false)!!
            debugMode = arguments?.getBoolean("debugMode", false)!!
            loadExpion = arguments?.getBoolean("loadExpion", true)!!
            loadUrl = arguments?.getString("loadUrl", "")!!
            injectionStr = "javascript:" + arguments?.getString(WV_INJECTIONSTR, "")!!
            company_id = arguments?.getString("company_id", "0")!!
            cookieValue = arguments?.getString("cookieValue", "")!!
            handler = Handler()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            view.loadingView!!.bringToFront()
            view.webViewLoadingProgressBar!!.bringToFront()

            setupWebView()

            populateWebView()

            if (position == 0 || stackRefreshing) {
                populateWebView()
                if (!isShown) {
                    isShown = true
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(savedInstanceState == null) {
            setHasOptionsMenu(debugMode)
            isLoaded = false
            rootView = inflater.inflate(R.layout.mlstackviewframe, container, false) as RelativeLayout

        }
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)

        return rootView
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        val webCookieManager = CookieManager.getInstance()
        webCookieManager.setAcceptCookie(true)
        webCookieManager.setCookie(webViewUrl, cookieValue)

        WebView.setWebContentsDebuggingEnabled(true)

        if (!loadFinished) {
            val webSettings = view?.webView!!.settings

            val userAgentString = activity?.getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)?.getString(resources.getString(R.string.android_user_agent), "")

            webSettings.userAgentString = userAgentString
            webView!!.position = position

            webSettings.javaScriptEnabled = true
            webSettings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK

            webSettings.domStorageEnabled = true
            webSettings.setAppCacheEnabled(true)
            if (!stackRefreshing) {
                webSettings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                webSettings.setAppCacheEnabled(true)
                val appCachePath = context?.cacheDir?.absolutePath
                webSettings.setAppCachePath(appCachePath)
            } else {
                webSettings.cacheMode = WebSettings.LOAD_NO_CACHE
                webSettings.setAppCacheEnabled(false)
            }
            webSettings.useWideViewPort = true
            webSettings.builtInZoomControls = true

            webView!!.isScrollContainer = true
            webView!!.isHorizontalScrollBarEnabled = true
            webView!!.setInitialScale(120)

            WebView.setWebContentsDebuggingEnabled(true)
            webView!!.setLayerType(View.LAYER_TYPE_HARDWARE, null)

            webView!!.webViewClient = webViewClient

            webView!!.webChromeClient = webChromeClient
        }
    }

    fun populateWebView() {
        if (webView != null && !isLoaded) {
            loadWebView(loadUrl)
        }
    }

    fun loadWebView(stackUrl: String?) {
        if (loadExpion) {
            if (position == 0 && !errorToastDisplayed) {
                handler!!.postAtFrontOfQueue {
                    isLoaded = true
                    if(view != null && view?.webView != null) {
                        Log.d("Stackss", "Loading URL #$position - $stackUrl")
                        view?.webView!!.loadUrl(stackUrl, customHeaders) //Expion
                        if (debugMode)
                        {
                            launch(UI)
                            {
                                LoggingUtils.logTimestamp(activity as AppCompatActivity?, "#$position: Loading ComponentLoader", debugMode)
                            }
                        }
                    }
                }
            } else {
                handler!!.post {
                    isLoaded = true
                    if(view != null && view?.webView != null) {
                        Log.d("Stackss", "Loading URL #$position - $stackUrl")
                        view?.webView!!.loadUrl(stackUrl, customHeaders) //Expion
                        if (debugMode)
                        {
                            launch(UI)
                            {
                                LoggingUtils.logTimestamp(activity as AppCompatActivity?, "#$position: Loading ComponentLoader", debugMode)
                            }
                        }
                    }
                }
            }
        } else {
            if (debugMode)
            {
                launch(UI)
                {
                    LoggingUtils.logTimestamp(activity as AppCompatActivity?, "#$position: Loading Google", debugMode)
                }
            }


            handler!!.post {
                view?.webView!!.loadUrl("http://www.google.com")
            }
        }
    }

    fun getWebView(): WebView? {
        return webView
    }

    fun injectScript(injectionStr: String?) {

        if (position == 0) {
            handler!!.post {
                if(webView != null) {
                    webView!!.loadUrl("javascript:$injectionStr",customHeaders)
                    //.evaluateJavascript(injectionStr, null)
                }
            }
        } else {
            if(webView != null) {
                handler!!.post {
                    //webView!!.evaluateJavascript(injectionStr, null)
                    webView!!.loadUrl("javascript:$injectionStr",customHeaders)
                }
            }
        }

        if(view != null && view?.webView != null) {

            view?.webView!!.postDelayed({
                if(view?.loadingView != null) {
                    view?.loadingView!!.visibility = View.GONE
                    Log.d("Stacks","Hiding from OnPageFinished #$position")
                }},150)

            if(stackRefreshing) {
                if(view != null && webViewLoadingProgressBar != null) {
                    view?.webViewLoadingProgressBar!!.visibility = View.GONE
                    view?.webViewLoadingProgressBar!!.progress = 5
                }
            }
        }

        if (debugMode)
        {
            launch(UI)
            {
                LoggingUtils.logTimestamp(activity as AppCompatActivity?, "#$position: Injecting script", debugMode)
            }
        }
    }

    private val customHeaders: Map<String, String>
        get() {
            val accessTokenString = activity?.getSharedPreferences(getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)?.getString("access_token", "")
            val headers = HashMap<String, String>()
            accessTokenString?.let { headers.put("Authorization", it) }
            headers.put("Cookie", cookieValue)
            return headers
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGEUPLOADER_RESULTCODE) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //If data is null, we're uploading a photo
                    if(data != null) {
                        if (uploadMessage == null) return
                        else {
                            uploadMessage?.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data))
                            uploadMessage = null
                            if (uploadFile != null) {
                                deleteAndScanFile(context, uploadFile!!.absolutePath, uploadFile!!)
                            }
                        }
                    }
                    else {
                        //If there is not data, then we may have taken a photo
                        if (fileUploadPath != null) {
                            val results = arrayOf(Uri.parse(fileUploadPath))
                            uploadMessage!!.onReceiveValue(results)

                            handler?.postDelayed({
                                if (uploadFile != null) {
                                    deleteAndScanFile(context, uploadFile!!.absolutePath, uploadFile!!)
                                }

                            }, 500)
                        }
                    }
                }
            }
            else if (requestCode == DIALOG_FRAGMENT) {
                selectedLocationIds = data!!.getStringArrayListExtra("selectedLocationIds")
                val locationsText = data.getStringExtra("locationsText")
                val guid = data.getStringExtra("guid")
                val jsonObject = JSONObject()
                try {
                    val intIds = ArrayList<Int>()
                    for (selectedLocationId in selectedLocationIds)
                        intIds.add(Integer.valueOf(selectedLocationId))

                    val array = JSONArray(intIds)
                    jsonObject.put("locationIds", array)
                    jsonObject.put("locationsTxt", locationsText)
                } catch (exc: JSONException) {
                    Log.e("Pixie Error", "JSONExc")
                }

                val injectionScript = "completeNativeRequest('" + guid + "'," + jsonObject.toString() + ")"

                view?.webView!!.evaluateJavascript(injectionScript
                ) { String -> webView!!.clearCache(true) }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            uploadMessage!!.onReceiveValue(null)
        }
    }

    companion object {
        val DIALOG_FRAGMENT = 1

        private val perimeterThreshhold = .35

        private val KEY_POSITION = "position"
        private val WV_INJECTIONSTR = "wvInjectionStr"
        private val COLUMN_COUNT = "columnCount"
        private val WV_URL = "webViewUrl"

        internal var debugMode = false
        val FILEUPLOADER_RESULTCODE = 100

        fun getInstance(position: Int, stackUrl: String, injectionStr: String, myUrl: String, columnCount: Int): WebViewFragment {
            val frag = WebViewFragment()

            val args = Bundle()
            args.putInt(KEY_POSITION, position)
            args.putString("loadUrl", stackUrl)
            args.putString(WV_INJECTIONSTR, injectionStr)
            args.putInt(COLUMN_COUNT, columnCount)
            args.putString(WV_URL, myUrl)
            frag.arguments = args
            return frag
        }
    }

    inner class EmbeddedWebChromeClient: WebChromeClient() {

        var lastNewProgress = 0

        private fun determineIntentType() {
            //TODO: Need to add conditions for documents and audio here.
            if (acceptType.contains("image")) {
                uploadFileIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            } else if (acceptType.contains("video")) {
                uploadFileIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            }
        }

        fun uploadImageStuff(takePictureIntent: Intent?) {

            if (activity is OnTaskCompleted)
                (activity as OnTaskCompleted).updateResumingStacksFromFragment(true)

            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "image/*"
            val intentArray: Array<Intent?>
            if (takePictureIntent != null) {
                intentArray = arrayOf(takePictureIntent)
            } else {
                intentArray = arrayOfNulls(0)
            }
            val chooserIntent = Intent(Intent.ACTION_CHOOSER)
            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
            startActivityForResult(chooserIntent, IMAGEUPLOADER_RESULTCODE)
        }

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if(webViewLoadingProgressBar != null)
                    webViewLoadingProgressBar.setProgress(newProgress,true)
            }
            else {
                if(webViewLoadingProgressBar != null)
                    webViewLoadingProgressBar.progress = newProgress
            }

            if(newProgress == 100 && !stackRefreshing) {
                if(webViewLoadingProgressBar != null && lastNewProgress != 10 && lastNewProgress != 100) {
                    webViewLoadingProgressBar.postDelayed({ webViewLoadingProgressBar!!.visibility = View.GONE }, 100)
                    webViewLoadingProgressBar.progress = 5
                }
            }

            lastNewProgress = newProgress

            Log.d("Stackss","OnProgressChanged: #$position - Progress: $newProgress")
        }
        // openFileChooser for Android 3.0+
        fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String = "") {
            mUploadMessage = uploadMsg

            // Create camera captured image file path and name
            try {
                uploadFile = PhotoUploadingUtils.createUploadFile(acceptType)
            } catch (ex: IOException) {
                // Error occurred while creating the File
                Log.e("File Upload", "Unable to create File", ex)
            }


            // Continue only if the File was successfully created
            if (uploadFile != null) {

                mCapturedImageURI = context?.let { FileProvider.getUriForFile(it, "com.expion.expion.fileProvider", uploadFile!!) }!!

                determineIntentType()

                uploadFileIntent!!.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI)
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.type = acceptType

                // Create file chooser intent
                val chooserIntent = Intent.createChooser(i, "Image Chooser")
                // Set camera intent to file chooser
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf<Parcelable>(uploadFileIntent!!))
                // On select image call onActivityResult method of activity
                startActivityForResult(chooserIntent, FILEUPLOADER_RESULTCODE)
            }

        }

        //Android 4.4 (19) to 5.1 (23)
        fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String, capture: String) {
            openFileChooser(uploadMsg, acceptType)
        }

        override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: WebChromeClient.FileChooserParams): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                acceptType = fileChooserParams.acceptTypes[0]
                if (acceptType.isEmpty()) {
                    acceptType = "image"
                }

                determineIntentType()

                uploadMessage = filePathCallback

                if (activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.READ_EXTERNAL_STORAGE) } != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
                } else {
                    uploadImageStuff(uploadFileIntent)
                }
            }
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> {
                run {
                    if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        EmbeddedWebChromeClient().uploadImageStuff(uploadFileIntent)
                    } else {
                        Toast.makeText(activity, "You can't upload a picture without this permission.", Toast.LENGTH_LONG).show()
                    }
                }
            }
            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EmbeddedWebViewClient().postToInstagram()
                } else {
                    Toast.makeText(activity, "You can't upload to Instagram without this permission.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    inner class EmbeddedWebViewClient : WebViewClient() {
        private var stackModificationType = -1

        fun loadNext() {

            handler!!.postDelayed({
                if(!isDetached) {
                    if (activity is StackLoadedListener)
                        (activity as StackLoadedListener).stackLoaded(position + 1)
                    //Log.d("Stacks","Populating next stack in 100ms")
                }}, 100)
        }


        fun jsCallToNativeBridge(jsCallString: String) {
            val callStringArray = jsCallString.split("::".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val callMessage = callStringArray[1]

            Log.d("Stacks","CallMessage: $callMessage")

            when (callMessage) {
                "is_loaded" -> {
                    Log.d("Stacks","Is Loaded #$position")
                    val getGuidJSON = parseBase64Text(callStringArray[2])
                    val guid = getGuidJSON!!.optString("guid")

                    if (guid != null && !guid.isEmpty()) {
                        val completeNativeScript =  "window.completeNativeRequest(\"$guid\", true)"

                        Log.d("Stacks","Injecting Initial Script #$position")
                        injectScript(completeNativeScript)
                        initialScriptInjected = true
                    }
                    handler!!.post{
                        Log.d("Stacks","Injecting Script #$position - $injectionStr")
                        injectScript(injectionStr)
                    }
                }
                "stack_load_completed" -> {

                    loadNext()
                    errorToastDisplayed = false
                    loadFinished = true
                    if(view != null) {

                        view?.loadingView!!.visibility = View.GONE
                        Log.d("Stacks","Hiding from StackLoadCompleted #$position")
                        if(stackRefreshing) {
                            view?.webView!!.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                        }
                        if (debugMode) {
                            launch(UI)
                            {
                                LoggingUtils.logTimestamp(activity as AppCompatActivity?, "#$position: stackLoadCompleted", debugMode)
                            }
                        }
                    }
                }
                "FlatTree" -> popupLocationDialog(jsCallString.split("::".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2])
                "IGMobile" -> {
                    instagramImgInfo = jsCallString.split("::".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2]
                    if (activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE) } != PackageManager.PERMISSION_GRANTED) {
                        if (activity is OnTaskCompleted)
                            (activity as OnTaskCompleted).updateResumingStacksFromFragment(true)
                        requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
                    } else {
                        postToInstagram()

                        handler!!.postDelayed({
                            if (uploadFile != null)
                                deleteAndScanFile(context, uploadFile!!.absolutePath, uploadFile!!)
                        }, 500)
                    }
                }
                "stack_added" -> {
                    stackModificationType = StackUtils.STACK_ADDED
                }
                "stack_id_inserted" -> {
                    val stackInsertInfoJson = parseBase64Text(callStringArray[2])
                    if(stackInsertInfoJson != null) {
                        val stackId = parseStackId(stackInsertInfoJson)
                        if(stackId != -1) {
                            view?.loadingView!!.visibility = View.VISIBLE
                            stackModificationType = StackUtils.STACK_INSERTED
                            if (activity is StackLoadedListener)
                                (activity as StackLoadedListener).insertStack(position + 1, stackId!!)
                        }
                    }
                }
                "stack_deleted" -> {
                    stackModificationType = StackUtils.STACK_DELETED

                    if (activity is StackLoadedListener)
                        (activity as StackLoadedListener).deleteStack(position)
                }
            }
        }

        private fun popupLocationDialog(locationsDataString: String) {
            var source = ""
            var guid = ""
            try {
                try {
                    val jsonLocations = parseBase64Text(locationsDataString)
                    val locationsData = JSONObject(jsonLocations!!.getString("data"))
                    source = locationsData.optString("sources")
                    val selectedLocsJsonArray = locationsData.optJSONArray("selected_locs")
                    for (i in 0 until selectedLocsJsonArray.length())
                        selectedLocationIds.add(selectedLocsJsonArray.getString(i))

                    guid = jsonLocations.optString("guid")
                } catch (exc: JSONException) {
                    Log.e("PixieJsonExc", exc.message)
                }

            } catch (exc: UnsupportedEncodingException) {
                exc.printStackTrace()
            }

            val locationsPickerFragment = LocationsPickerFragment()
            val bundle = Bundle()
            bundle.putString("baseUrl", webViewUrl)
            bundle.putString("company_id", company_id)
            bundle.putString("source", source)
            bundle.putString("guid", guid)
            bundle.putStringArrayList("selected_locs", selectedLocationIds)

            locationsPickerFragment.arguments = bundle
            locationsPickerFragment.isCancelable = false

            locationsPickerFragment.setTargetFragment(this@WebViewFragment, DIALOG_FRAGMENT)
            locationsPickerFragment.show(fragmentManager, "locationsDialog")

            fragmentManager?.executePendingTransactions()
            //locationsPickerFragment.getDialog().getWindow().setDimAmount(.9f);
        }

        fun postToInstagram() {

            try {
                val jsonInstagramInfo = JSONObject(instagramImgInfo)
                val jsonInstagramData = JSONObject(jsonInstagramInfo.getString("data"))
                var imageUrl = jsonInstagramData.optString("image_url")
                if (!imageUrl.startsWith("http")) {
                    imageUrl = webViewUrl!! + imageUrl
                }
                var msgText = jsonInstagramData.optString("msg_txt")
                Log.i("msgTxt","From server: $msgText")
                if(msgText != null && msgText.isNotEmpty()) {
                    msgText = replacer(msgText)
                    Log.i("msgTxt","After replacing: $msgText")
                    msgText = URLEncoder.encode(msgText, "UTF-8")
                    Log.i("msgTxt","After encoding: $msgText")
                    msgText = URLDecoder.decode(msgText,"UTF-8")
                    Log.i("msgTxt","After decoding: $msgText")
                }
                val attachType = if (jsonInstagramData.optString("attach_type").isEmpty()) "image" else jsonInstagramData.optString("attach_type")
                val videoUrl = jsonInstagramData.optString("video_url")

                var fileSharingType = "image/*"
                var connectUrl = imageUrl
                if (attachType == "video") {
                    fileSharingType = "video/*"
                    connectUrl = videoUrl
                }

                val fileType = fileSharingType

                if (attachType == "image") {
                    val request = Request.Builder()
                            .url(connectUrl)
                            .get()
                            .build()

                    (activity as ContainerActivity).client.newCall(request).enqueue(object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            e.printStackTrace()
                        }

                        @Throws(IOException::class)
                        override fun onResponse(call: Call, response: Response) {
                            response.body()!!.use { _ ->
                                if (!response.isSuccessful) {
                                    longSnackbar(rootView!!, "Failed to get IG image " + response.message())
                                } else {
                                    var uri: Uri? = null
                                    if (fileType.contains("image")) {
                                        val inputStream = response.body()!!.byteStream()
                                        val bitmap = BitmapFactory.decodeStream(inputStream)
                                        uri = getImageUri(bitmap)
                                    } else {
                                        val sink = Okio.buffer(Okio.sink(uploadFile!!))
                                        sink.writeAll(response.body()!!.source())
                                        sink.close()

                                        val media = File(uploadFile!!.absolutePath)
                                        uri = context?.let { FileProvider.getUriForFile(it, "com.expion.expion.fileProvider", media) }
                                    }

                                    if (uri != null) {

                                        val shareIntent = CreateInstagramPost.createInstagramIntent(uri, fileType)

                                        if (activity is OnTaskCompleted)
                                            (activity as OnTaskCompleted).updateResumingStacksFromFragment(true)

                                        activity?.runOnUiThread {
                                            if (msgText != null && !msgText.isEmpty()) {
                                                val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                                                val clip = ClipData.newPlainText("label", msgText)
                                                clipboard.primaryClip = clip
                                            }

                                            val chooser = Intent.createChooser(shareIntent, "Share to")

                                            if (shareIntent.resolveActivity(context?.packageManager) != null) {
                                                try {
                                                    startActivity(chooser)
                                                    //startActivity(shareIntent);
                                                } catch (exc: ActivityNotFoundException) {
                                                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.instagram.android")))
                                                } catch (exc: Exception) {
                                                    //Potential exception: user not logged in and/or user not having an account
                                                    startActivity(chooser)
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        longSnackbar(rootView!!, "Error sharing IG post: null URI")
                                    }
                                }
                            }
                        }
                    })
                }
            } catch (jsonExc: JSONException) {
                jsonExc.printStackTrace()
            }

        }

        fun getImageUri(inImage: Bitmap): Uri {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path = MediaStore.Images.Media.insertImage(context?.contentResolver, inImage, "Title", null)
            return Uri.parse(path)
        }

        private fun replacer(outBuffer: String): String {
            var data = outBuffer
            try {
                data = data.replace("%(?![0-9a-fA-F]{2})".toRegex(), "%25")
                data = data.replace("\\+".toRegex(), "%2B")
                data = URLDecoder.decode(data, "utf-8")
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return data
        }

        private fun parseStackId(stackIdJson: JSONObject): Int? {
            val dataJson = stackIdJson.optString("data")
            var stackId = ""
            if(dataJson != null) {
                val layoutDataJson = JSONObject(dataJson).optString("layoutData")
                if(layoutDataJson != null)
                    stackId = JSONObject(layoutDataJson).optString("stack_id")
            }
            return stackId?.toInt() ?: -1
        }

        //TODO: Delegate these calls to either a listener or Architecture Components

        /**STACKS**/
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            /**STACKS**/
            webViewLoadingProgressBar?.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            /**PIXIE**/
            /*if (!loadFinished) {
                webView!!.postDelayed({ loadingView.visibility = View.GONE }, 1000)
            }*/
        }

        override fun onLoadResource(view: WebView?, url: String?) {
            super.onLoadResource(view, url)
            //Log.d("Stackss","$url")
        }

        override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {
            when {
                url.startsWith("js-call::") -> jsCallToNativeBridge(url)
                url.contains(webViewUrl!!) -> webView.loadUrl(url, customHeaders)
                else -> openExternalLink(url)
            }
            return true
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            shouldOverrideUrlLoading(view,request.url.toString())
            return true
        }

        fun openExternalLink(link: String) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            openNativeLink(link, intent)
        }

        private fun getPackageName(link: String): String {
            when {
                link.contains("facebook") -> return "com.facebook.katana"
                link.contains("twitter") -> return "com.twitter.android"
                link.contains("instagram") -> return "com.instagram.android"
            }
            return link

        }

        private fun getAppUrl(link: String): String {

            when {
                link.contains("facebook") -> return "fb://facewebmodal/f?href=$link"
                link.contains("twitter") -> return if (link.contains("https")) link.replace("https://twitter.com/", "twitter://")
                else link.replace("http://twitter.com/", "twitter://")
                link.contains("instagram") -> return if (link.contains("https")) link.replace("https://instagram.com/", "instagram://")
                else link.replace("http://instagram.com/", "instagram://")
            }
            return link
        }

        private fun openNativeLink(link: String, intent: Intent) {
            if (activity is OnTaskCompleted)
                (activity as OnTaskCompleted).updateResumingStacksFromFragment(true)
            try {
                val info = context?.packageManager?.getPackageInfo(getPackageName(link),0)
                if(info?.applicationInfo!!.enabled) {
                    val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getAppUrl(link)))
                    startActivity(appIntent)
                }
                else {
                    startActivity(intent)
                }
            }
            catch(exc: Exception) {
                startActivity(intent)
            }
        }

        override fun onReceivedError(thisWebView: WebView, errorCode: Int, description: String, failingUrl: String) {

            /**STACKS**/
            if (!errorToastDisplayed) {
                try {
                    Log.d("Error", "Stack #$position : $description")

                    if (description.contains("ERR_INSUFFICIENT_RESOURCES") || description.contains("SPDY_PROTOCOL_ERROR")) {
                        longSnackbar(rootView!!, "Stack Loading Error. Reloading Now.")
                        errorToastDisplayed = true
                        loadingView!!.visibility = View.VISIBLE
                        isLoaded = false
                        loadFinished = false
                        isShown = false
                        initialScriptInjected = false
                        view?.webView!!.clearCache(false)
                        handler!!.postDelayed({ loadWebView(loadUrl) }, 1000)
                    }
                } catch (exc: Exception) {
                    Log.e("Exc", exc.message)
                }

            }
        }

        @TargetApi(Build.VERSION_CODES.M)
        override fun onReceivedError(view: WebView, req: WebResourceRequest, rerr: WebResourceError) {
            // Redirect to deprecated method, so you can use it in all SDK versions
            Log.e("OnReceivedError", rerr.description.toString() + " - " + req.url.toString())
            onReceivedError(view, rerr.errorCode, rerr.description.toString(), req.url.toString())
        }

        override fun onReceivedHttpError(view: WebView?, request: WebResourceRequest?, errorResponse: WebResourceResponse?) {
            super.onReceivedHttpError(view, request, errorResponse)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.e("StacksHttpErr", "URL: ${request?.url} - Reason: ${errorResponse?.reasonPhrase} - Code: ${errorResponse?.statusCode}")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item!!.itemId == R.id.logout) {
            activity?.supportFragmentManager?.popBackStack()
            val loginIntent = Intent(activity, LoginFragment::class.java)
            startActivity(loginIntent)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDetach() {
        super.onDetach()
        if(webView != null)
            webView.stopLoading()
    }
}