package com.expion.expion.DynamicClasses

/**
 * Created by mbagwell on 10/20/15.
 */
open class ExpionMLBaseClass internal constructor(val settings: ExpionMLBaseClassSettings) {

    fun newInstance(settingsOverrride: ExpionMLBaseClassSettings): ExpionMLBaseClass {
        return ExpionMLBaseClass(settingsOverrride)
    }

}

