package com.expion.expion.DynamicClasses

import android.support.v4.app.FragmentManager
import com.expion.expion.Models.MLItem
import java.util.*

/**
 * Created by mbagwell on 10/20/15.
 */
class ExpionMLBaseClassSettings(var supportFragmentManager: FragmentManager, var myUrl: String, var company_id: String, loadUrl: String, internal var tabletMode: Boolean,
                                var cookieValue: String, internal var debugMode: Boolean) {
    //var myUrl: String
    //var company_id: String
    lateinit var settingsMlItem: MLItem

    //Stacks Settings
    lateinit var stackIdArray: ArrayList<String>
    lateinit var columnCounts: ArrayList<Int>
    internal var loadUrl: String
    internal var resumingFromStacksFragment: Boolean = false
    internal var delayedInjection: Boolean = false
    lateinit var workspaceType: String
    internal var stacks: Boolean = false

    //Publish Settings
    /*internal var publishOption: String? = null
    internal var actionUrl: String? = null*/

    //Photos Settings
    lateinit var displayMode: String
    internal var loadExpion: Boolean = false



    init {
        this.myUrl = myUrl
        this.company_id = company_id
        this.loadUrl = loadUrl
        if (tabletMode) {
            this.loadUrl += "&xTablet=True"
        }
    }

    fun setMlItem(mlItem: MLItem) {
        this.settingsMlItem = mlItem
    }

    fun setStacksSettings(stackIdArray: ArrayList<String>, columnCounts: ArrayList<Int>, resumingFromStacksFragment: Boolean, loadExpion: Boolean, workspaceType: String, delayedInjection: Boolean) //HorizontalGridView gridView,ViewPager vPager,
    {
        this.stackIdArray = stackIdArray
        this.columnCounts = columnCounts
        this.resumingFromStacksFragment = resumingFromStacksFragment
        this.loadExpion = loadExpion
        this.workspaceType = workspaceType
        this.delayedInjection = delayedInjection
        this.stacks = true
    }

    /*public void setPublishSettings(String actionUrl, String publishOption)
    {
        this.actionUrl = actionUrl;
        this.publishOption = publishOption;
    }*/

    fun setPhotosSettings(displayMode: String) {
        this.displayMode = displayMode
    }
}
