package com.expion.expion.DynamicClasses


import android.os.Bundle
import com.expion.expion.ApplicationComponents.ViewPortComponents.StacksFragment
import com.expion.expion.R
import com.expion.expion.Utilities.StackLayoutUtils
import java.util.*

class MLStackClass(settingsOverrride: ExpionMLBaseClassSettings) : ExpionMLBaseClass(settingsOverrride) {

    fun Execute() {
        val injectionStrings = StackLayoutUtils.buildInjectionStrings(settings.settingsMlItem.id, settings.settingsMlItem.layout_id, settings.stackIdArray)

        setupAdapter(injectionStrings)
    }

    private fun setupAdapter(injectionStrings: ArrayList<String>) {
        val stacksFragment = StacksFragment()

        val bundle = Bundle()
        bundle.putStringArrayList("injectionStrings", injectionStrings)
        bundle.putStringArrayList("stackIdArray",settings.stackIdArray)
        bundle.putBoolean("tabletMode", settings.tabletMode)
        bundle.putBoolean("resumingFromStacksFragment", settings.resumingFromStacksFragment)
        bundle.putBoolean("loadExpion", settings.loadExpion)
        bundle.putBoolean("delayedInjection",settings.delayedInjection)
        bundle.putString("myUrl", settings.myUrl)
        bundle.putString("loadUrl", settings.loadUrl)
        bundle.putString("company_id", settings.company_id)
        bundle.putInt("layout_id", settings.settingsMlItem.layout_id)
        bundle.putInt("mlItemId", settings.settingsMlItem.id)
        bundle.putIntegerArrayList("columnCounts", settings.columnCounts)
        bundle.putString("cookieValue", settings.cookieValue)
        bundle.putBoolean("debugMode",settings.debugMode)

        stacksFragment.arguments = bundle

        val transaction = settings.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, stacksFragment, settings.workspaceType)
        transaction.addToBackStack(settings.workspaceType)
        transaction.commit()
    }
}
