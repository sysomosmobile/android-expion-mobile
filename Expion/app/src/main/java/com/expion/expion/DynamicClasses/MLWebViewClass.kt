package com.expion.expion.DynamicClasses

import android.os.Bundle
import com.expion.expion.ApplicationComponents.ViewPortComponents.WebViewFragment
import com.expion.expion.R

/**
 * Created by arowe on 3/3/2016.
 */
class MLWebViewClass(baseClassSettings: ExpionMLBaseClassSettings) : ExpionMLBaseClass(baseClassSettings) {

    fun Execute() {

        val bundle = Bundle()

        val injectionString = "loadXPComponent('${settings.settingsMlItem.settings}');"

        bundle.putString("wvInjectionStr", injectionString)
        bundle.putString("loadUrl", settings.loadUrl)
        bundle.putString("webViewUrl", settings.myUrl)
        bundle.putString("company_id", settings.company_id)
        bundle.putString("cookieValue", settings.cookieValue)
        bundle.putBoolean("stacks", settings.stacks)

        //val mlWebViewFragment = MLWebViewFragment()
        val mlWebViewFragment = WebViewFragment()
        mlWebViewFragment.arguments = bundle

        val transaction = settings.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, mlWebViewFragment, "Pixie")
        transaction.addToBackStack("Pixie")

        transaction.commit()
    }

}
