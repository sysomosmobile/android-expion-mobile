package com.expion.expion.DynamicClasses


import android.os.Bundle
import com.expion.expion.ApplicationComponents.GalleryView.PhotoApprovalFragment
import com.expion.expion.R

/**
 * Created by arowe on 10/20/15.
 */
class MobileApprovalClass(settingsOverride: ExpionMLBaseClassSettings) : ExpionMLBaseClass(settingsOverride) {

    fun Execute() {
        launchGallery()
    }

    private fun launchGallery() {

        val photoApprovalFragment = PhotoApprovalFragment()

        val args = Bundle()
        args.putString("companyId", settings.company_id)
        args.putString("baseUrl", settings.myUrl)
        args.putString("displayMode", settings.displayMode)
        args.putBoolean("canSchedule", true)
        args.putBoolean("canExpionImage", true)

        photoApprovalFragment.arguments = args

        val transaction = settings.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, photoApprovalFragment, "Photo Approval")
        transaction.addToBackStack("Photo Approval")

        transaction.commit()
    }

}
