package com.expion.expion.DynamicClasses


import android.os.Bundle
import com.expion.expion.ApplicationComponents.GalleryView.AlbumsFragment
import com.expion.expion.R

/**
 * Created by arowe on 10/20/15.
 */
class MobileUploaderClass(settingsOverride: ExpionMLBaseClassSettings) : ExpionMLBaseClass(settingsOverride) {

    fun Execute() {
        launchGalleryFragment()
    }

    private fun launchGalleryFragment() {

        val albumsFragment = AlbumsFragment()

        val args = Bundle()
        args.putString("company_id", settings.company_id)
        args.putString("baseUrl", settings.myUrl)
        args.putString("displayMode", settings.displayMode)
        args.putBoolean("canSchedule", true)
        args.putBoolean("canExpionImage", true)

        albumsFragment.arguments = args

        val transaction = settings.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, albumsFragment, "Albums")
        transaction.addToBackStack("Albums")
        transaction.commit()
    }

}
