package com.expion.expion.Models

import com.google.gson.annotations.SerializedName

/**
 * Created by arowe on 12/2/2015.
 */
class Channel {
    @SerializedName("name")
    var name: String? = null

    @SerializedName("val")
    var `val`: String? = null
}
