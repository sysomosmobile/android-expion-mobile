package com.expion.expion.Models

import com.google.gson.annotations.SerializedName

/**
 * Created by arowe on 5/12/2016.
 */
class EImageAction {
    @SerializedName("link")
    var link: String? = null
    @SerializedName("name")
    var name: String? = null
}
