package com.expion.expion.Models

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.SerializedName

class EImageAlbum : Parcelable {
    @SerializedName("image")
    lateinit var image: Bitmap
    @SerializedName("title")
    var title: String
    @SerializedName("id")
    var id: String
    @SerializedName("image_url")
    var coverUrl: String
    @SerializedName("cred")
    var albumCred: String
    @SerializedName("create_date")
    var createDate: String
    @SerializedName("locationName")
    var locationName: String
    @SerializedName("Location")
    lateinit var location: Location

    private constructor(`in`: Parcel) {
        title = `in`.readString()
        id = `in`.readString()
        coverUrl = `in`.readString()
        albumCred = `in`.readString()
        createDate = `in`.readString()
        locationName = `in`.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(title)
        dest.writeString(id)
        dest.writeString(coverUrl)
        dest.writeString(albumCred)
        dest.writeString(createDate)
        dest.writeString(locationName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<EImageAlbum> = object : Parcelable.Creator<EImageAlbum> {
            override fun createFromParcel(`in`: Parcel): EImageAlbum {
                return EImageAlbum(`in`)
            }

            override fun newArray(size: Int): Array<EImageAlbum?> {
                return arrayOfNulls(size)
            }
        }
    }
}
