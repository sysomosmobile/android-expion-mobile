package com.expion.expion.Models

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import org.json.JSONArray
import java.io.Serializable
import java.util.*

class EImageAlbumImage : Serializable, Parcelable {
    @SerializedName("image")
    var image: Bitmap? = null
    @SerializedName("name")
    var name: String? = ""
    @SerializedName("image_id")
    var imageId: String
    @SerializedName("image_url")
    var imageUrl: String
    @SerializedName("location_name")
    var locName: String? = null
    @SerializedName("uploaded_by")
    var uploadedBy: String
    @SerializedName("upload_date")
    var uploadDate: String
    @SerializedName("actions")
    lateinit var actions: ArrayList<EImageAction>
    @SerializedName("actionsJsonArray")
    lateinit var actionsData: JSONArray
    @SerializedName("approveUrl")
    var approveUrl: String
    @SerializedName("rejectUrl")
    var rejectUrl: String
    @SerializedName("albumName")
    var albumName: String? = null
    @SerializedName("status")
    var status: String? = ""

    private constructor(`in`: Parcel) {
        name = `in`.readString()
        imageId = `in`.readString()
        imageUrl = `in`.readString()
        locName = `in`.readString()
        uploadedBy = `in`.readString()
        uploadDate = `in`.readString()
        approveUrl = `in`.readString()
        rejectUrl = `in`.readString()
        albumName = `in`.readString()
        status = `in`.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(name)
        dest.writeString(imageId)
        dest.writeString(imageUrl)
        dest.writeString(locName)
        dest.writeString(uploadedBy)
        dest.writeString(uploadDate)
        dest.writeString(approveUrl)
        dest.writeString(rejectUrl)
        dest.writeString(albumName)
        dest.writeString(status)
    }

    fun setId(image_id: String) {
        this.imageId = image_id
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<EImageAlbumImage> = object : Parcelable.Creator<EImageAlbumImage> {
            override fun createFromParcel(`in`: Parcel): EImageAlbumImage {
                return EImageAlbumImage(`in`)
            }

            override fun newArray(size: Int): Array<EImageAlbumImage?> {
                return arrayOfNulls(size)
            }
        }
    }
}
