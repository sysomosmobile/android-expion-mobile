package com.expion.expion.Models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by arowe on 5/16/2016.
 */
@Parcelize
class Location(var name: String, var id: String, var street_address: String) : Parcelable {

    val isSelected: Boolean
        get() = street_address == "1"

    fun setSelected(street_address: String) {
        this.street_address = street_address
    }
}
