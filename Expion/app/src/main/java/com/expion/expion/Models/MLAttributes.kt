package com.expion.expion.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by arowe on 10/18/17.
 */
class MLAttributes {
    @SerializedName("layout_id")
    @Expose
    var layoutId: Int? = null
}