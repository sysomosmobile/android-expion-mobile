package com.expion.expion.Models

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by mbagwell on 10/15/15.
 */
class MLItem {

    var folder_ind = false
    var id = -1

    @SerializedName("layout_id")
    var layout_id = -1

    @SerializedName("children")
    var children: ArrayList<MLItem>? = null
    var name = ""
    var code_class_name = ""
    var css_class_name = ""
    var settings = ""

    internal var company_id = -1
    internal var order_ind = -1
    internal var suggested_ind = false
    internal var shd_v = false
    internal var shd_e = false
    internal var shd_d = false
    internal var shd_c = false
    internal var set_hw_c = false
    internal var set_hw_v = false

    var deployed_status: String? = null

    private constructor(builder: Builder) {
        this.id = builder.id
        this.company_id = builder.company_id
        this.name = builder.name
        this.settings = builder.settings
        this.code_class_name = builder.code_class_name
        this.css_class_name = builder.css_class_name
        folder_ind = builder.folder_ind
        order_ind = builder.order_ind
        children = builder.children
        layout_id = builder.layout_id
        deployed_status = builder.deployed_status
    }

    constructor() {
        children = ArrayList()
    }

    constructor(folder_ind: Boolean, id: Int, company_id: Int, name: String, order_ind: Int, settings: String, suggested_ind: Boolean,
                shd_v: Boolean, shd_e: Boolean, shd_d: Boolean, shd_c: Boolean, set_hw_c: Boolean, set_hw_v: Boolean, code_class_name: String, css_class_name: String, layout_id: Int, children: ArrayList<MLItem>) {

        this.folder_ind = folder_ind
        this.id = id
        this.company_id = company_id
        this.name = name
        this.order_ind = order_ind
        this.settings = settings
        this.suggested_ind = suggested_ind
        this.shd_v = shd_v
        this.shd_e = shd_e
        this.shd_d = shd_d
        this.shd_c = shd_c
        this.set_hw_c = set_hw_c
        this.set_hw_v = set_hw_v
        this.code_class_name = code_class_name
        this.css_class_name = css_class_name

        this.layout_id = layout_id

        this.children = children
    }

    class Builder(id: Int, company_id: Int, name: String, settings: String, code_class_name: String, css_class_name: String) {

        var folder_ind = false
        var id = -1
        var layout_id = -1
        //public MLStack mlStack;
        var children: ArrayList<MLItem>? = null
        var name = ""
        var code_class_name = ""
        var css_class_name = ""
        var settings = ""

        var company_id = -1
        var order_ind = -1
        var suggested_ind = false
        var shd_v = false
        var shd_e = false
        var shd_d = false
        var shd_c = false
        var set_hw_c = false
        var set_hw_v = false

        var deployed_status: String? = null

        init {
            this.id = id
            this.company_id = company_id
            this.name = name
            this.settings = settings
            this.code_class_name = code_class_name
            this.css_class_name = css_class_name
        }

        fun folder_ind(folder_ind: Boolean): Builder {
            this.folder_ind = folder_ind
            return this
        }

        fun order_ind(order_ind: Int): Builder {
            this.order_ind = order_ind
            return this
        }

        fun children(children: ArrayList<MLItem>): Builder {
            this.children = children
            return this
        }

        fun deployed_status(deployed_status: String): Builder {
            this.deployed_status = deployed_status
            return this
        }

        fun layout_id(layout_id: Int): Builder {
            this.layout_id = layout_id
            return this
        }

        fun set_hw_c(set_hw_c: Boolean): Builder {
            this.set_hw_c = set_hw_c
            return this
        }

        fun set_hw_v(set_hw_v: Boolean): Builder {
            this.set_hw_v = set_hw_v
            return this
        }

        fun shd_c(shd_c: Boolean): Builder {
            this.shd_c = shd_c
            return this
        }

        fun shd_e(shd_e: Boolean): Builder {
            this.shd_e = shd_e
            return this
        }

        fun shd_d(shd_d: Boolean): Builder {
            this.shd_d = shd_d
            return this
        }

        fun shd_v(shd_v: Boolean): Builder {
            this.shd_v = shd_v
            return this
        }

        fun build(): MLItem {
            return MLItem(this)
        }
    }
}

