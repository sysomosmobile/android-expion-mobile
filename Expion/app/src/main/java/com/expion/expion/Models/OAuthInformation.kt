package com.expion.expion.Models

/**
 * Created by arowe on 4/26/2016.
 */
class OAuthInformation() {
    var accessToken: String? = null
    var accessSecret: String? = null
    var authorizationError: String? = null
    var authenticationError: String? = null
    var errorReason: String? = null
}
