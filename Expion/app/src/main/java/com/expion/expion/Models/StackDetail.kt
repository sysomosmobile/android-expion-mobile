package com.expion.expion.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by arowe on 10/18/17.
 */
class StackDetail {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("column_count")
    @Expose
    var columnCount: Int? = null
}
