package com.expion.expion.Services

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * Created by arowe on 8/31/17.
 */

class AddLSAccessCookieInterceptor(private val lsAccessCookie: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val cookieHeaders = originalRequest.headers("Cookie")
        cookieHeaders
                .filter { it != null && it.contains("ls.access") }
                .forEach { Log.d("CookieInLSAccessCookie", it) }

        var cookieHeader = originalRequest.header("Cookie")
        return if(cookieHeader != null && !cookieHeader.contains("ls.access")) {
            cookieHeader = "$cookieHeader;$lsAccessCookie"

            val requestWithUserAgent = originalRequest.newBuilder()
                    .removeHeader("Cookie")
                    .addHeader("Cookie",cookieHeader)
                    .build()

            if (!lsAccessCookie.isEmpty())
                chain.proceed(requestWithUserAgent)
            else
                chain.proceed(originalRequest)
        }
        else
            chain.proceed(originalRequest)
    }
}
