package com.expion.expion.Services

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.expion.expion.Models.EImageAlbum
import com.expion.expion.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arowe on 11/9/2015.
 */
class AlbumsArrayAdapter(context: Context?, albums: ArrayList<EImageAlbum>) : ArrayAdapter<EImageAlbum>(context, R.layout.album_item) {
    private val albums: List<EImageAlbum>
    private val format: SimpleDateFormat
    private val inputFormat: SimpleDateFormat

    init {
        this.albums = albums
        format = SimpleDateFormat("MM/dd/yyyy", context?.resources?.configuration?.locale)
        inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", context?.resources?.configuration?.locale)
        inputFormat.timeZone = TimeZone.getTimeZone("GMT")
    }

    private inner class ViewHolder {
        internal var imageView: ImageView? = null
        internal var albumTitle: TextView? = null
        internal var albumDetails: TextView? = null
        internal var albumLocation: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val retView: View
        val holder: ViewHolder?

        if (convertView == null) {
            retView = LayoutInflater.from(parent.context).inflate(R.layout.album_item, parent, false)

            holder = ViewHolder()
            holder.albumTitle = retView.findViewById<View>(R.id.albumTitle) as TextView
            holder.albumDetails = retView.findViewById<View>(R.id.albumDetails) as TextView
            holder.albumLocation = retView.findViewById<View>(R.id.albumLocation) as TextView

            holder.imageView = retView.findViewById<View>(R.id.albumIcon) as ImageView

            retView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }

        val album = albums[position]
        val albumTitle = album.title
        holder.albumTitle!!.text = albumTitle
        var albumDetailsString = album.createDate

        if (albumDetailsString != null) {
            val convertedDate: Date
            try {
                convertedDate = inputFormat.parse(albumDetailsString)
                albumDetailsString = "Created: " + format.format(convertedDate)
                holder.albumDetails!!.text = albumDetailsString
            } catch (e: ParseException) {
                holder.albumDetails!!.text = albumDetailsString
                e.printStackTrace()
            }

        } else {
            holder.albumDetails!!.visibility = View.GONE
        }

        val locationName = "For: " + album.location?.name ?: album.locationName
        holder.albumLocation!!.text = locationName

        return retView
    }

    override fun getCount(): Int {
        return albums.size
    }
}
