package com.expion.expion.Services

import android.content.Context
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.expion.expion.Models.EImageAlbumImage
import com.expion.expion.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arowe on 11/9/2015.
 */
class ImagesArrayAdapter(private val context: Context?, albumImages: ArrayList<EImageAlbumImage>,
                         private val listener: RecyclerViewClickListener) : RecyclerView.Adapter<ImagesArrayAdapter.ImagesViewHolder>() {
    private var albumImages: MutableList<EImageAlbumImage>
    private val format: SimpleDateFormat
    private val inputFormat: SimpleDateFormat

    init {
        this.albumImages = albumImages
        format = SimpleDateFormat("MM/dd/yyyy", context?.resources?.configuration?.locale)
        inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", context?.resources?.configuration?.locale)
        inputFormat.timeZone = TimeZone.getTimeZone("GMT")
    }

    inner class ImagesViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        override fun onClick(v: View?) {
            listener.onClick(v,adapterPosition)
        }

        private var image: EImageAlbumImage? = null

        private val imageView: ImageView = view.findViewById<View>(R.id.albumImage) as ImageView
        private val imageStatus: ImageView = view.findViewById<View>(R.id.imageStatusIcon) as ImageView
        private val imageTitle: TextView = view.findViewById<View>(R.id.imageTitle) as TextView
        private val imageUploadDetails: TextView = view.findViewById<View>(R.id.imageUploadDetails) as TextView
        private val imageLocationDetails: TextView = view.findViewById<View>(R.id.imageLocationDetails) as TextView
        private val imageAlbumDetails: TextView = view.findViewById<View>(R.id.imageAlbumDetails) as TextView

        init {
            view.setOnClickListener(this)
        }

        fun bindImage(image: EImageAlbumImage) {
            this.image = image
            imageTitle.text = image.name

            if (image.locName != null) {
                val imageLocationDetailsString = "Location: " + image.locName!!
                imageLocationDetails.text = imageLocationDetailsString
            }

            var albumName: String? = if (image.albumName != null) image.albumName else ""
            if (albumName!!.isNotEmpty()) {
                albumName = "Album: " + albumName
                imageAlbumDetails.text = albumName
            }

            imageView.setImageBitmap(image.image)

            var imageDateString = image.uploadDate

            val convertedDate: Date
            try {
                convertedDate = inputFormat.parse(imageDateString)
                imageDateString = format.format(convertedDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val imageUploadDetailsString = "By: " + image.uploadedBy + ", " + imageDateString
            imageUploadDetails.text = imageUploadDetailsString

            var imageStatusString = image.status
            if(imageStatusString != null) {
                Log.i("Imagess","Setting ${image.name} to $imageStatusString")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(imageStatusString.toLowerCase().contains("approve")) {
                        imageStatus.setImageDrawable(context?.getDrawable(R.drawable.approved_photo))
                    }
                    else if(imageStatusString.toLowerCase().contains("reject")) {
                        imageStatus.setImageDrawable(context?.getDrawable(R.drawable.rejected_photo))
                    }
                }
                else {
                    if(imageStatusString.toLowerCase().contains("approve")) imageStatus.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.approved_photo) })
                    else if(imageStatusString.toLowerCase().contains("reject")) imageStatus.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.rejected_photo) })
                }
            } else {
                imageStatusString = "Submitted"
                Log.i("Imagess","Setting ${image.name} to $imageStatusString")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesViewHolder {
        val inflatedView = parent.inflate(R.layout.image_item, false)
        return ImagesViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ImagesViewHolder, position: Int) {
        val itemImage = albumImages[position]
        holder.bindImage(itemImage)
    }

    /*override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var retView = convertView
        val holder: ImagesViewHolder?

        if (retView == null) {
            retView = LayoutInflater.from(parent.context).inflate(viewResourceId, parent, false)

            holder = ImagesViewHolder(retView)

            retView.tag = holder
        } else {
            holder = retView.tag as ImagesViewHolder
        }

        val image = albumImages[position]

        val imageTitle = image.name
        holder.imageTitle.text = imageTitle

        if (image.locName != null) {
            val imageLocationDetailsString = "Location: " + image.locName!!
            holder.imageLocationDetails.text = imageLocationDetailsString
        }

        var albumName: String? = if (image.albumName != null) image.albumName else ""
        if (albumName!!.isNotEmpty()) {
            albumName = "Album: " + albumName
            holder.imageAlbumDetails.text = albumName
        }

        holder.imageView.setImageBitmap(image.image)
        var imageDateString = image.uploadDate

        val convertedDate: Date
        try {
            convertedDate = inputFormat.parse(imageDateString)
            imageDateString = format.format(convertedDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val imageUploadDetails = "By: " + image.uploadedBy + ", " + imageDateString
        holder.imageUploadDetails.text = imageUploadDetails

        var imageStatusString = image.status
        if(imageStatusString != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.i("Imagess","Setting ${image.name} to $imageStatusString")
                if(imageStatusString == "Approve") {
                    holder.imageStatus.setImageDrawable(context.getDrawable(R.drawable.approved_photo))
                }
                else if(imageStatusString == "Reject") {
                    holder.imageStatus.setImageDrawable(context.getDrawable(R.drawable.rejected_photo))
                }
            }
            else {
                Log.i("Imagess","Setting ${image.name} to $imageStatusString")
                if(imageStatusString == "Approve") holder.imageStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.approved_photo))
                else if(imageStatusString == "Reject") holder.imageStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.rejected_photo))
            }
        } else {
            imageStatusString = "Submitted"
        }
        Log.i("Images","Status: $imageStatusString | Name: ${image.name} | Position: $position")

        return retView
    }

    private fun setStatusDrawable(imageStatus: ImageView, status: String) =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                when (status) {
                    "approve" -> imageStatus.setImageDrawable(context.getDrawable(R.drawable.approved_photo))
                    "reject" -> imageStatus.setImageDrawable(context.getDrawable(R.drawable.rejected_photo))
                    else -> imageStatus.setImageDrawable(context.getDrawable(R.drawable.submitted_photo))
                }
            }
            else {
                when (status) {
                    "approve" -> imageStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.approved_photo))
                    "reject" -> imageStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.rejected_photo))
                    else -> imageStatus.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.submitted_photo))
                }
            }*/

    override fun getItemCount(): Int = albumImages.size

    public fun setAlbumImages(albumImages: MutableList<EImageAlbumImage>) {
        this.albumImages = albumImages
    }

    public fun addAlbumImages(albumImages: List<EImageAlbumImage>) {
        this.albumImages.addAll(albumImages)
    }
}
