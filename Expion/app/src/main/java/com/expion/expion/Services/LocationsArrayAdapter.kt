package com.expion.expion.Services

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckedTextView
import com.expion.expion.Models.Location
import com.expion.expion.R
import java.util.*

/**
 * Created by arowe on 5/16/2016.
 */
class LocationsArrayAdapter(context: Context?, private val locations: ArrayList<Location>) : ArrayAdapter<Location>(context, R.layout.rowlayout) {


    private inner class ViewHolder {
        internal var textView: CheckedTextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val retView: View
        val holder: ViewHolder?

        if (convertView == null) {
            //val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            retView = LayoutInflater.from(parent.context).inflate(R.layout.rowlayout, parent, false) // inflater.inflate(R.layout.rowlayout, parent)

            holder = ViewHolder()
            holder.textView = retView.findViewById<CheckedTextView>(R.id.checkedtxtview)

            retView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }

        val location = locations[position]

        holder.textView!!.text = location.name

        return retView
    }

    override fun getCount(): Int {
        return locations.size
    }

    override fun getItem(position: Int): Location? {
        return locations[position]
    }

}
