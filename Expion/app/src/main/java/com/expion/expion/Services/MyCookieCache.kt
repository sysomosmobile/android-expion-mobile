package com.expion.expion.Services

import android.os.Build
import android.util.Log
import com.franmontiel.persistentcookiejar.cache.CookieCache
import okhttp3.Cookie
import java.util.*

/**
 * Created by arowe on 9/5/17.
 */

class MyCookieCache : CookieCache {

    override fun addAll(newCookies: Collection<Cookie>) {
        for (cookie in newCookies) {
            cookies!!.remove(cookie)
            cookies!!.add(cookie)
            Log.d("Cookie","${cookie.domain()} - ${cookie.name()} - ${cookie.value()}")
        }
    }

    override fun clear() {
        if (cookies != null)
            cookies!!.clear()
    }

    override fun iterator(): MutableIterator<Cookie> {
        return cookies!!.iterator()
    }

    override fun spliterator(): Spliterator<Cookie> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            cookies!!.spliterator()
        } else {
            TODO("VERSION.SDK_INT < N")
        }
    }

    companion object {
        internal var cookies: MutableCollection<Cookie>? = object : ArrayList<Cookie>() {}

        fun getCookies(): Collection<Cookie>? {
            return cookies
        }

        fun createNonPersistentCookie(cookieName: String, cookieValue: String, cookieDomain: String): Cookie {
            return Cookie.Builder()
                    .domain(cookieDomain)
                    .path("/")
                    .name(cookieName)
                    .value(cookieValue)
                    .build()
        }
    }
}
