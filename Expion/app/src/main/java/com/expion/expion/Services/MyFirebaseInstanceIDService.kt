/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.expion.expion.Services

import android.app.Activity
import android.util.Log
import com.expion.expion.R
import com.expion.expion.R.id.username
import com.expion.expion.R.string.company_id
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import org.json.JSONObject

class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {
    //private static final String baseNotificationsUrl = "http://34.199.227.10";

    /*
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */

    // [START refresh_token]
    override fun onTokenRefresh() {
        // Get updated InstanceID token.
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(TAG, "Refreshed token: " + refreshedToken!!)

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken);


        val preferences = applicationContext.getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)

        val prefEditor = preferences.edit()
        prefEditor.putString(resources.getString(R.string.firebase_token_needs_updating), "true")
        prefEditor.apply()
    }
    // [END refresh_token]

    /*
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param firebaseToken The new token.
     */

    private fun sendRegistrationToServer(firebaseToken: String) {
        val preferences = applicationContext.getSharedPreferences(resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE)
        val myUrl = preferences.getString(resources.getString(R.string.base_url), "")

        if (!myUrl!!.isEmpty()) {

            val connectUrl = myUrl + "/api/notificationproxy"
            val notificationEndpoint = "/api/user_devices"

            /*SyncHttpClient asyncHttpClient = new SyncHttpClient();
            asyncHttpClient.setEnableRedirects(true);
            asyncHttpClient.setCookieStore(new PersistentCookieStore(getApplicationContext()));
            asyncHttpClient.addHeader("Content-type","application/json");*/

            //asyncHttpClient.addHeader("api-key","7cf1b775e8674716b56efa82ade8b7a9");

            val jsonParams = JSONObject()
            //StringEntity entity = null;

            try {
                val dataJson = JSONObject()
                dataJson.put("company_id", company_id)
                //dataJson.put("user_id", 0);
                dataJson.put("sso_user_nm", username)
                dataJson.put("server_nm", "t1")
                dataJson.put("device_type", "android")
                dataJson.put("device_token", firebaseToken)

                Log.d("FirebaseToken", firebaseToken)

                //dataJson.put("badges_ind",true);
                //dataJson.put("sound_ind", true);
                //dataJson.put("active_ind", true);

                jsonParams.put("data", dataJson)
                jsonParams.put("method", "post")
                jsonParams.put("endpoint", notificationEndpoint)

                /*entity = new StringEntity(jsonParams.toString());
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));*/
            } catch (exception: Exception) {

            }

            /*asyncHttpClient.post(getApplicationContext(), connectUrl, entity, "application/json", new JsonHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject)
                {
                    Log.d(TAG,"Token Registered");
                }

                @Override
                public void onFailure(int errorCode, Header[] headers, Throwable exc, JSONObject jsonObject)
                {
                    Log.e("Failed token refresh_icon", exc.getMessage());
                }
            });*/
        }
    }

    companion object {

        private val TAG = "MyFirebaseIIDService"
    }
}
