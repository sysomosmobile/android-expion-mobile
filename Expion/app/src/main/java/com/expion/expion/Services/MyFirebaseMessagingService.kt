package com.expion.expion.Services

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import android.util.Log
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = "MyFirebaseMsgService"

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
    // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage!!.from)

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty())
        {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null)
        {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body!!)
            val messageBody = if (remoteMessage.notification!!.body != null) remoteMessage.notification!!.body else ""
            val messageTitle = if (remoteMessage.notification!!.title != null) remoteMessage.notification!!.title else ""
            val notificationId = Integer.valueOf(if (remoteMessage.data["n_id"] != null) remoteMessage.data["n_id"] else "0")!!
            sendNotification(messageBody, messageTitle, notificationId)

            //updateMyActivity(getApplicationContext(), messageBody)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody:String?, messageTitle:String?, notificationId:Int) {
        val intent = Intent(this, ContainerActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("message", messageBody)
        intent.putExtra("stack_type", "insights")
        val pendingIntent = PendingIntent.getActivity(this, notificationId, intent,
        PendingIntent.FLAG_ONE_SHOT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.chat_bubbles)
        .setContentTitle(messageTitle)
        .setTicker(messageTitle)
        .setContentText(messageBody)
        .setAutoCancel(true)
        .setSound(defaultSoundUri)
        /*.setStyle(NotificationCompat.BigPictureStyle().bigPicture(
        BitmapFactory.decodeResource(getResources(),
        R.drawable.chat_bubbles)))*/
        .setContentIntent(pendingIntent)

        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addParentStack(ContainerActivity::class.java)
        stackBuilder.addNextIntent(intent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

        notificationManager!!.notify(notificationId, notificationBuilder.build())
    }

    private fun updateMyActivity(context:Context, message:String?) {

    val intent = Intent("update_notifications")

     //put whatever data you want to send, if any
            intent.putExtra("message", message)

     //send broadcast
            context.sendBroadcast(intent)
    }
}
