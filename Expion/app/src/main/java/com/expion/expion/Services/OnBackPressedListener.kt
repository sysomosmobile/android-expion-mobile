package com.expion.expion.Services

/**
 * Created by arowe on 12/21/2015.
 */
interface OnBackPressedListener {
    fun onBackPressed()
}
