package com.expion.expion.Services

/**
 * Created by arowe on 12/9/2015.
 */
interface OnTaskCompleted {
    fun loginCompleted(isLoggedIn: Boolean, debugMode: Boolean, overrideCookies: Boolean)
    fun dialogClosed()
    fun updateResumingStacksFromFragment(resumingStacksFromFragment: Boolean)
}
