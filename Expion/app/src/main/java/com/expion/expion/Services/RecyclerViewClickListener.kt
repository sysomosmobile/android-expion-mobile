package com.expion.expion.Services

import android.view.View

/**
 * Created by arowe on 11/20/17.
 */
interface RecyclerViewClickListener {

    fun onClick(view: View?, position: Int)
}