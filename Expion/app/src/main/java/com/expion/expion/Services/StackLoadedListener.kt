package com.expion.expion.Services

/**
 * Created by arowe on 10/6/2016.
 */

interface StackLoadedListener {
    fun stackLoaded(stackPositionLoaded: Int)
    fun refreshWorkspace(position: Int, refreshType: Int)
    fun deleteStack(position: Int)
    fun insertStack(position: Int, stackId: Int)
}
