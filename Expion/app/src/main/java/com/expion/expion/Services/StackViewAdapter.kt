package com.expion.expion.Services

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import com.expion.expion.ApplicationComponents.RootComponents.ContainerActivity
import com.expion.expion.ApplicationComponents.ViewPortComponents.HorizontalScrollWebView
import com.expion.expion.R
import com.expion.expion.Utilities.LoggingUtils
import com.expion.expion.Utilities.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
import com.expion.expion.Utilities.MathUtils
import com.expion.expion.Utilities.StringParsingUtilities.parseBase64Text
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arowe on 1/11/2016.
 */
class StackViewAdapter(private val containerActivity: ContainerActivity?, private val injectionStrings: List<String>, internal var wvMyUrl: String, private val loadUrl: String?, internal var columnCounts: List<Int>, private val cookieValue: String) //boolean loadExpion,
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val handler: Handler
    val IMAGEUPLOADER_RESULTCODE = 100
    private var debugMode: Boolean

    var uploadMessage: ValueCallback<Array<Uri>>? = null
    private var fileUploadPath: String? = null
    private var uploadFileName: String? = null

    private var uploadFileIntent: Intent? = null
    private var acceptType = ""

    init {
        debugMode = containerActivity!!.debugMode

        val webCookieManager = CookieManager.getInstance()
        webCookieManager.setAcceptCookie(true)

        webCookieManager.setCookie(wvMyUrl, cookieValue)
        handler = Handler()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): StackViewTestViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.mlstackviewframe, viewGroup, false)

        return StackViewTestViewHolder(itemView)
    }


    override fun onBindViewHolder(baseHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = baseHolder as StackViewTestViewHolder
        if(columnCounts.size > baseHolder.adapterPosition) {
            val columnCount = columnCounts[baseHolder.adapterPosition]
            val dpWidth = columnCount * MathUtils.convertDpToPixel(390f, containerActivity!!)
            holder.fragView.layoutParams = FrameLayout.LayoutParams(dpWidth.toInt(), FrameLayout.LayoutParams.MATCH_PARENT)

            val injectionString = injectionStrings[position]

            if (position != -1 && loadUrl != null) {

                if (!holder.loadFinished) {
                    holder.loadingView.visibility = View.VISIBLE
                    holder.loadingView.bringToFront()

                    val webSettings = holder.webView.settings
                    webSettings.javaScriptEnabled = true

                    webSettings.userAgentString = containerActivity.getSharedPreferences(containerActivity.resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE).getString(containerActivity.resources.getString(R.string.android_user_agent), "")

                    webSettings.useWideViewPort = true
                    webSettings.loadWithOverviewMode = true
                    webSettings.domStorageEnabled = true

                    webSettings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                    webSettings.setAppCacheEnabled(true)
                    val appCachePath = containerActivity.cacheDir.absolutePath
                    webSettings.setAppCachePath(appCachePath)

                    holder.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)

                    WebView.setWebContentsDebuggingEnabled(true)

                    holder.webView.webViewClient = object : WebViewClient() {


                        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                            if (url.startsWith("js-call::")) {
                                jsCallToNativeBridge(holder, injectionString, url)
                            } else
                                view.loadUrl(url, customHeaders)
                            return true
                        }

                        @TargetApi(Build.VERSION_CODES.N)
                        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                            val url = request.url.toString()
                            if (url.startsWith("js-call::")) {
                                jsCallToNativeBridge(holder, injectionString, url)
                            } else
                                view.loadUrl(url, customHeaders)
                            return true
                        }

                        override fun onPageFinished(view: WebView, url: String) {
                            super.onPageFinished(view, url)
                            if (debugMode) {
                                LoggingUtils.logTimestamp(containerActivity, "#$position: onLoadFinished", containerActivity.logToTextView)
                            }
                        }
                    }

                    holder.webView.webChromeClient = object : WebChromeClient() {

                        override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: WebChromeClient.FileChooserParams): Boolean {

                            determineIntentType()
                            uploadMessage = filePathCallback

                            uploadFileIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                            if (holder.webView.context is OnTaskCompleted)
                                (holder.webView.context as OnTaskCompleted).updateResumingStacksFromFragment(true)

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                acceptType = fileChooserParams.acceptTypes[0]
                                if (acceptType.isEmpty()) {
                                    acceptType = "image/*"
                                }

                                if (ContextCompat.checkSelfPermission(containerActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    requestPermission()
                                } else {
                                    if (uploadFileIntent!!.resolveActivity(containerActivity.packageManager) != null) {
                                        // Create the File where the photo should go
                                        var photoFile: File? = null
                                        try {
                                            photoFile = createUploadFile()
                                            uploadFileIntent!!.putExtra("PhotoPath", fileUploadPath)
                                        } catch (ex: IOException) {
                                            // Error occurred while creating the File
                                            Log.e("Photo", "Unable to create Image File", ex)
                                        }

                                        // Continue only if the File was successfully created
                                        if (photoFile != null) {
                                            fileUploadPath = "file:" + photoFile.absolutePath
                                            uploadFileIntent!!.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(containerActivity, "com.expion.expion.fileProvider", photoFile))
                                        } else {
                                            uploadFileIntent = null
                                        }
                                    }

                                    uploadImageStuff(holder.webView.context, uploadFileIntent)
                                }
                            } else {
                                if (uploadFileIntent!!.resolveActivity(containerActivity.packageManager) != null) {
                                    // Create the File where the photo should go
                                    var photoFile: File? = null
                                    try {
                                        photoFile = createUploadFile()
                                        uploadFileIntent!!.putExtra("PhotoPath", fileUploadPath)
                                    } catch (ex: IOException) {
                                        // Error occurred while creating the File
                                        Log.e("Photo", "Unable to create Image File", ex)
                                    }

                                    // Continue only if the File was successfully created
                                    if (photoFile != null) {
                                        fileUploadPath = "file:" + photoFile.absolutePath
                                        uploadFileIntent!!.putExtra(MediaStore.EXTRA_OUTPUT,
                                                FileProvider.getUriForFile(containerActivity, "com.expion.expion.fileProvider", photoFile))
                                    } else {
                                        uploadFileIntent = null
                                    }
                                }

                                uploadImageStuff(holder.webView.context, uploadFileIntent)
                            }
                            return true
                        }
                    }

                    loadWebView(loadUrl, holder.adapterPosition, holder)
                }
            } else {
                val progBar = holder.fragView.findViewById<View>(R.id.stacksProgressBar) as ProgressBar
                progBar.visibility = View.GONE
                holder.webView.visibility = View.GONE
            }
        }
        else {
            holder.loadingView.visibility = View.VISIBLE
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(containerActivity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
    }

    private fun loadWebView(stackUrl: String, position: Int, holder: StackViewTestViewHolder) {

        /*if(loadExpion)
        {*/
        if (position == 0) {
            handler.postAtFrontOfQueue {
                /*if(debugMode)
                        mainActivity.runOnUiThread(() -> LoggingUtils.logTimestamp(mainActivity, "#" + position + ": Loading ComponentLoader", mainActivity.logToTextView));*/
                holder.webView.loadUrl(stackUrl, customHeaders) //Expion
                //holder.loadFinished = true;
            }
        } else {
            handler.post {
                /*if(debugMode)
                        mainActivity.runOnUiThread(() -> LoggingUtils.logTimestamp(mainActivity, "#" + position + ": Loading ComponentLoader", mainActivity.logToTextView));*/

                holder.webView.loadUrl(stackUrl, customHeaders) //Expion
                //holder.loadFinished = true;
            }
        }
        /*}
        else
        {
            if(debugMode)
                LoggingUtils.logTimestamp(mainActivity, "#" + position + ": Loading Google", mainActivity.logToTextView);

            webView.load("http://www.google.com", null);
        }*/
    }

    override fun getItemCount(): Int {
        return injectionStrings.size
    }

    private fun determineIntentType() {
        //TODO: Need to add conditions for documents and audio here.
        if (acceptType.contains("image")) {
            uploadFileIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        } else if (acceptType.contains("video")) {
            uploadFileIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        }
    }

    @Throws(IOException::class)
    private fun createUploadFile(): File {
        val storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES)

        return File.createTempFile(
                createFileName(), /* prefix */
                createFileExtension(), /* suffix */
                storageDir      /* directory */
        )
    }

    private fun createFileName(): String {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())

        if (acceptType.contains("image")) {
            uploadFileName = "JPEG_" + timeStamp + "_"
        } else
        // if(acceptType.contains("video"))
        {
            uploadFileName = "MPEG_" + timeStamp + "_"
        }

        return uploadFileName!!
    }

    private fun createFileExtension(): String {
        var fileExtension = ""

        if (acceptType.contains("image")) {
            fileExtension = ".jpg"
        } else
        // if(acceptType.contains("video"))
        {
            fileExtension = ".mp4"
        }

        return fileExtension
    }

    private fun uploadImageStuff(context: Context, takePictureIntent: Intent?) {

        if (context is OnTaskCompleted)
            (context as OnTaskCompleted).updateResumingStacksFromFragment(true)

        val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
        contentSelectionIntent.type = "image/*"
        val intentArray: Array<Intent?>
        if (takePictureIntent != null) {
            intentArray = arrayOf(takePictureIntent)
        } else {
            intentArray = arrayOfNulls(0)
        }
        val chooserIntent = Intent(Intent.ACTION_CHOOSER)
        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        //TODO: Implement this start activity for result somehow for tablet asset upload
        //startActivityForResult(chooserIntent, IMAGEUPLOADER_RESULTCODE);
    }

    class StackViewTestViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var fragView: RelativeLayout = itemView as RelativeLayout
        internal var loadingView: RelativeLayout
        internal var webView: HorizontalScrollWebView
        internal var loadFinished = false

        init {
            val dpWidth = MathUtils.convertDpToPixel(390f, itemView.context)
            fragView.layoutParams = FrameLayout.LayoutParams(dpWidth.toInt(), FrameLayout.LayoutParams.MATCH_PARENT)
            loadingView = fragView.findViewById<View>(R.id.loadingView) as RelativeLayout
            webView = fragView.findViewById<View>(R.id.webView) as HorizontalScrollWebView
        }
    }

    private val customHeaders: Map<String, String>
        get() {
            val headers = HashMap<String, String>()
            if (containerActivity != null) {
                val accessTokenString = containerActivity.getSharedPreferences(containerActivity.resources.getString(R.string.stored_prefs_name), Activity.MODE_PRIVATE).getString("access_token", "")
                headers.put("Authorization", accessTokenString)
                headers.put("Cookie", cookieValue)
            }
            return headers
        }

    companion object {


        private fun jsCallToNativeBridge(holder: StackViewTestViewHolder, injectionStr: String?, jsCallString: String) {
            val callStringArray = jsCallString.split("::".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val callMessage = callStringArray[1]

            val urlToLoad = "javascript:" + injectionStr!!

            when (callMessage) {
                "is_loaded" -> if (injectionStr.length > 0) {
                    val getGuidJSON = parseBase64Text(callStringArray[2])
                    val guid = getGuidJSON!!.optString("guid")

                    if (guid != null && !guid.isEmpty()) {
                        val completeNativeScript =  "window.completeNativeRequest(\"$guid\", true)"

                        holder.webView.evaluateJavascript(completeNativeScript, null)
                    }
                    holder.webView.evaluateJavascript(injectionStr, null)

                    holder.webView.postDelayed({
                        holder.loadingView.visibility = View.GONE
                        },1500)

                }
                "stack_load_completed" -> {
                    holder.loadFinished = true
                    holder.loadingView.visibility = View.GONE
                }
            }//Log.e("NoActionJSCallToNative","js--jsCallToNativeBridge no action for message=>" + callMessage);
        }
    }
}
