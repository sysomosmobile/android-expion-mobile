package com.expion.expion.Services

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * Created by arowe on 8/30/17.
 */

/* This interceptor adds a custom User-Agent. */
class UserAgentInterceptor(private val userAgent: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestWithUserAgent = originalRequest.newBuilder()
                .header("User-Agent", userAgent)
                .build()
        return chain.proceed(requestWithUserAgent)
    }
}
