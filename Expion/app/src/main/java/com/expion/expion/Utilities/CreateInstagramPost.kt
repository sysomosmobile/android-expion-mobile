package com.expion.expion.Utilities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.support.v4.content.FileProvider

import java.io.File

/**
 * Created by arowe on 2/10/17.
 */

object CreateInstagramPost {
    private var mediaPath: String? = null

    fun createInstagramIntent(context: Context, filename: String, fileType: String): Intent {
        mediaPath = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString() + "/" + filename

        // Create the new Intent using the 'Send' action.
        val shareIntent = Intent(Intent.ACTION_SEND)

        // Set the MIME type
        shareIntent.type = fileType

        // Create the URI from the media
        val media = File(mediaPath!!)

        val uri = FileProvider.getUriForFile(context, "com.expion.expion.fileProvider", media)

        // Add the URI to the Intent.
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)

        return shareIntent
    }

    fun createInstagramIntent(uri: Uri, fileType: String): Intent {

        // Create the new Intent using the 'Send' action.
        val shareIntent = Intent(Intent.ACTION_SEND)

        // Set the MIME type
        shareIntent.type = fileType

        // Add the URI to the Intent.
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        return shareIntent
    }
}
