package com.expion.expion.Utilities

import android.content.Context
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager

/**
 * Created by arowe on 11/3/17.
 */
fun adjustFontScale(context: Context?, configuration: Configuration) {
    if (configuration.fontScale > 1f)
    {
        configuration.fontScale = 1f
        val metrics = context?.resources?.displayMetrics
        val wm = context?.getSystemService(AppCompatActivity.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(metrics)
        metrics?.scaledDensity = configuration.fontScale * metrics?.density!!
        context.resources.updateConfiguration(configuration, metrics)
    }
}