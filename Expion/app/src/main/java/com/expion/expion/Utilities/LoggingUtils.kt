package com.expion.expion.Utilities

import android.support.v7.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arowe on 11/20/2015.
 */
object LoggingUtils {
    private val formatter = SimpleDateFormat("HH:mm:ss.SSS", Locale.US)

    fun logTimestamp(activity: AppCompatActivity?, loggingInfo: String, logToTextView: Boolean) {
        if (!logToTextView)
            println(loggingInfo + " - TIME: " + formatter.format(Date()))
        else {
            val loggingCallback = activity as LoggingCallback
            loggingCallback.logOutput("\n" + loggingInfo + " - TIME: " + formatter.format(Date()))
        }
    }

    interface LoggingCallback {
        fun logOutput(outputText: String)
    }
}
