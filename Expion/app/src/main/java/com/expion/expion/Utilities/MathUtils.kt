package com.expion.expion.Utilities

import android.content.Context

/**
 * Created by arowe on 1/4/2016.
 */
object MathUtils {
    fun convertDpToPixel(dp: Float, context: Context?): Float {
        val metrics = context?.resources?.displayMetrics
        return Math.ceil((dp * metrics?.density!!).toDouble()).toInt().toFloat()
    }

    fun convertPixelsToDp(px: Float, context: Context): Float {
        val metrics = context?.resources?.displayMetrics
        return Math.ceil((px / metrics?.density!!).toDouble()).toInt().toFloat()
    }
}
