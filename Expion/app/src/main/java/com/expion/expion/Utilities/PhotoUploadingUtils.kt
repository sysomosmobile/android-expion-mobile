package com.expion.expion.Utilities

import android.content.Context
import android.media.MediaScannerConnection
import android.os.Environment
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arowe on 2/10/17.
 */

object PhotoUploadingUtils {
    @Throws(IOException::class)
    fun createUploadFile(acceptType: String): File {
        val storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES)

        return File.createTempFile(
                createFileName(acceptType), /* prefix */
                createFileExtension(acceptType), /* suffix */
                storageDir      /* directory */
        )
    }

    fun createFileName(acceptType: String): String {
        var uploadFileName = ""

        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())

        if (acceptType.contains("image")) {
            uploadFileName = "JPEG_" + timeStamp + "_"
        } else
        // if(acceptType.contains("video"))
        {
            uploadFileName = "MPEG_" + timeStamp + "_"
        }

        return uploadFileName
    }

    fun createFileExtension(acceptType: String): String {
        var fileExtension = ""

        if (acceptType.contains("image")) {
            fileExtension = ".jpg"
        } else
        // if(acceptType.contains("video"))
        {
            fileExtension = ".mp4"
        }

        return fileExtension
    }

    fun deleteAndScanFile(context: Context?, path: String,
                          fi: File) {
        val fpath = path.substring(path.lastIndexOf("/") + 1)
        try {
            MediaScannerConnection.scanFile(context, arrayOf(Environment
                    .getExternalStorageDirectory().toString()
                    + "/images/"
                    + fpath), null
            ) { _, uri ->
                if (uri != null) {
                    context?.contentResolver?.delete(uri, null, null)
                }
                fi.delete()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
