package com.expion.expion.Utilities

import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by arowe on 9/6/17.
 */

object StackLayoutUtils {
    private val INJECTION_STRING_ANTECEDENT = "loadXPComponent('"
    private val INJECTION_STRING_POSTCEDENT = "');"
    private val injectionStrings = ArrayList<String>()
    private val attributeValues = JSONObject()
    private val mlStack = JSONObject()
    private val mlAttr = JSONObject()
    private val mlDir = JSONObject()

    fun initializeJsonObject() {
        attributeValues.put("class", "xMobile mobilize")
        attributeValues.put("add-new-stack", "False")
        mlAttr.put("attributeValues", attributeValues)

        mlDir.put("directive_name", "stacks-layout")
        mlDir.put("attributes", mlAttr)

        mlStack.put("directive", mlDir)

    }

    fun buildInjectionStrings(mlItemId: Int, layoutId: Int, stackIdArray: ArrayList<String>): ArrayList<String> {

        try {
            attributeValues.put("layout_id", layoutId)
            attributeValues.put("module_id", mlItemId)

            mlAttr.put("layout_id", layoutId)

            injectionStrings.clear()

            (0 until stackIdArray.size).mapTo(injectionStrings) { loadMLStackView(stackIdArray, it) }
        } catch (exc: JSONException) {
            Log.e("JSONExc", exc.message)
        }

        return injectionStrings
    }

    private fun loadMLStackView(stackIdArray: List<String>, position: Int): String {
        var injectionStr = ""
        try {
            attributeValues.put("data-single-stack-id", stackIdArray[position])

            injectionStr = "$INJECTION_STRING_ANTECEDENT${mlStack.toString()}$INJECTION_STRING_POSTCEDENT"
            return injectionStr
        } catch (e: JSONException) {
            Log.e("loadMLStackView", e.message)
        }

        return injectionStr
    }

}
