package com.expion.expion.Utilities

/**
 * Created by arowe on 5/31/17.
 */

interface StackUtils {
    companion object {

        val STACK_INSERTED = 0
        val STACK_ADDED = 1
        val STACK_DELETED = 2
        val STACK_REFRESHED = 3

        val INCREMENT_STACK_POSITION = 1
        val DECREMENT_STACK_POSITION = -1

        //Networking
        val SOCKET_TIMEOUT = 40000
    }
}
