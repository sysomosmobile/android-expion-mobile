package com.expion.expion.Utilities

import android.util.Base64
import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URI
import java.net.URISyntaxException

/**
 * Created by arowe on 12/11/2015.
 */
object StringParsingUtilities {

    fun convertCodeClassNameForDCI(code_class_name: String): String {
        when (code_class_name) {
            "MLStack" -> return code_class_name
            "homeworkspace" -> return "HomeWorkspace"
            "quick-publish", "wechat-broadcast", "" -> return "MLWebView"
            "mobile-uploader" -> return "MobileUploader"
            "mobile-approval" -> return "MobileApproval"
            else -> return code_class_name
        }
    }

    @Throws(URISyntaxException::class)
    fun getDomainName(url: String): String {
        val uri = URI(url)
        val domain = uri.host
        return if (domain.startsWith("www.")) domain.substring(4) else domain
    }



    fun parseBase64Text(base64Text: String?): JSONObject? {
        var jsonObject: JSONObject? = null

        val data = Base64.decode(base64Text, Base64.DEFAULT)
        try {
            val utf8Text = String(data, Charsets.UTF_8)
            try {
                jsonObject = JSONObject(utf8Text)
            } catch (exc: JSONException) {
                Log.e("JsonExc", exc.message)
            }

        } catch (exc: UnsupportedEncodingException) {
            exc.printStackTrace()
        }

        return jsonObject
    }
}
